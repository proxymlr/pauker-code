<?php

error_reporting(E_ALL & ~E_NOTICE);
ini_set("display_errors", 1);

##
# Pages for guestbook
$page_size = 10;
$master = '81.217.14.229';

# detect users language
# inspired by http://aktuell.de.selfhtml.org/tippstricks/php/httpsprache/
function lang_getfrombrowser ($allowed_languages, $default_language, $linfo = null, $strict_mode = true) {
        if ($linfo === null) {
                $linfo = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
        }
        // if no language information available return default
        if (empty($linfo)) {
                return $default_language;
        }
        // split header
        $languages = preg_split('/,\s*/', $linfo);
        $current_lang = $default_language;
        $current_q = 0;
        foreach ($languages as $language) {                // get information about each language.
                $res = preg_match ('/^([a-z]{1,8}(?:-[a-z]{1,8})*)'.
                                   '(?:;\s*q=(0(?:\.[0-9]{1,3})?|1(?:\.0{1,3})?))?$/i', $language, $matches);
                if (!$res) {                               // ignore invalid syntax.
                        continue;
                }
                $code = explode ('-', $matches[1]);
                // get language 'quality'
                if (isset($matches[2])) {
                        $quality = (float)$matches[2];
                } else {
                        $quality = 1.0;
                }
                while (count ($code)) {
                        if (in_array (strtolower (join ('-', $code)), $allowed_languages)) {
                                if ($lang_quality > $current_q) {
                                        // use this language
                                        $current_lang = strtolower (join ('-', $code));
                                        $current_q = $lang_quality;
                                        break;
                                }
                        }
                        if ($strict_mode) {                // do not mimize language if in strict mode
                                break;
                        }
                        array_pop ($code);
                }
        }
        return $current_lang;
}


function guestbook_connect() {
    global $db, $connected;
    if (!$connected) {
        if (!($db = mysql_connect("mysql4-p.sourceforge.net", "p40334rw", "rawhide"))) {
            die("Cannot connect to database server.");
        }

        if (!(mysql_select_db("p40334_guestbook",$db))) {
            die("Aye, database does not exist on server.");
        }
    }
}

function guestbook_query() {
   global $db, $result, $pages, $page_size;

   guestbook_connect();
   $result = mysql_query("SELECT * FROM data ORDER BY id ASC", $db);
   $num_rows = mysql_num_rows($result);
   $pages = ceil($num_rows / $page_size);
}

function guestbook_pagesel() {
   global $db, $result, $pages, $page_size;

   guestbook_connect();
   $page = $_GET["pn"];
   if ($page=="") {
       $page = $pages;
   }

   $asw = '<pagesel>';
   if ($page > 1) {
      $asw .= '<left>';
      for ($i = 1; $i < $page; $i+=1) {
        $asw .= '<gpage pn="'.$i.'"/>';
      }
      $asw .= '</left>';
   }
   $asw .= '<center pn="'.$page.'"/>';
   if ($page < $pages) {
      $asw .= '<right>';
      for ($i = $page + 1; $i <= $pages; $i+=1) {
        $asw .= '<gpage pn="'.$i.'"/>';
      }
      $asw .= '</right>';
   }
   $asw .= '</pagesel>';
   return DOMDocument::loadXML($asw);
}

function guestbook_err($node) {
   global $guestbook_err;
   return $node[0]->getAttribute($guestbook_err);
}

function guestbook_name() {
   global $name;
   return $name;
}

function guestbook_text() {
   global $text;
   return $text;
}

function guestbook_read() {
   global $db, $result, $pages, $page_size, $master;
   $asw = '';
   # How many posts per page?
   $page = $_GET["pn"];
   if ($page=="") {
       $page = $pages;
   }
   $postc = 0;
   $asw .= "<entries>";
   while ($myrow = mysql_fetch_array($result)) {
       $postc += 1;

       if ($postc < ($page - 1) * $page_size) {
           continue;
       }

       if ($postc > ($page) * $page_size) {
           continue;
       }

       $id = $myrow["id"];
       $name = $myrow["name"];
       $name = htmlspecialchars($name);
       $name = utf8_encode($name);

       $date = $myrow["date"];
       $date = gmdate("Y-m-d, H:i:s", $date);

       $text = $myrow["text"];
       $text = htmlspecialchars($text);
       $text = nl2br($text);
       if (mb_detect_encoding($text)!='UTF-8' || !mb_check_encoding($text, 'UTF-8')) {
           $text = utf8_encode($text);
       }

       $asw .= "<entry>";
       $asw .= "<name>$name</name>";
       $asw .= "<date>$date</date>";
       $asw .= "<text>$text</text>\n";
       if ($_SERVER["HTTP_X_REMOTE_ADDR"] == $master) {
          $asw .= '<delete id="'.$id.'"/>';
       }
       $asw .= "</entry>";
   }
   if ($page == $pages) {
       $asw .= "<entry class=\"new\">";
       $asw .= "<name>Free</name>\n";
       $asw .= "<date></date>";
       $asw .= "<text>Free for your post!</text>";
       $asw .= "</entry>";
   }
   $asw .= "</entries>";
   return DOMDocument::loadXML($asw);
}

function prepare_news($text) {
   $text = preg_replace("/\(<a href=.*\)/", "", $text);
   return DOMDocument::loadXML("<text>$text</text>");
}

##
# guestbook submit
if ($_POST['submit']) {
    guestbook_connect();
    // process form
    $name = $_POST["name"];
    $ip = $_SERVER["HTTP_X_REMOTE_ADDR"];
    $date = time();
    $text = $_POST["text"];
    $name = trim($name);
    $text = trim($text);
    $captcha= $_POST["keyx"];

    if ($name == "") {
        $guestbook_err="err_name";
    } else if ($text == "") {
        $guestbook_err="err_notext";
    } else if ($captcha != "pauker" && $captcha != "Pauker") {
        $guestbook_err="err_captcha";
    } else {
        $sql = "INSERT INTO data (name,email,ip,date,text) VALUES ('$name','$email','$ip','$date', '$text')";
        $result = mysql_query($sql);
        if (!$result) {
            die('SQL-Error: ' . mysql_error());
        }
        $guestbook_err="success";
    }
}

######
# deletes a guestbook entry
if ($_GET["action"]=="delete") {
   guestbook_connect();
   if ($_SERVER["HTTP_X_REMOTE_ADDR"] != $master) {
      $guestbook_err="err_notallow";
   } else {
      $id = $_GET["id"];
      $result = mysql_query("SELECT * FROM data WHERE id='$id'", $db);
      if (!($myrow = mysql_fetch_array($result))) {
         echo "$guestbook_items[err_notallow]";
      } else {
          $sql = "DELETE FROM data WHERE id='$id'";
          $result = mysql_query($sql);
          $guestbook_err='msg_delete';
      }
   }
}


$project=$_GET['project'];
if ($project=="") {
    $project="pauker";
}

$page=$_GET['page'];
if ($page=="") {
    $page=$_GET['target'];     // backward URL compability
}
if ($page=="") {
    $page="home";
}

$screenshot=$_GET['screenshot'];

$lang=$_GET['lang'];
if ($lang=="") {
   # start the dection process if no language specified
   $lang = lang_getfrombrowser(array("en","de"), "en", null, false);
}

$xml_doc = new DomDocument;
$xml_doc->load('paukerweb.xml');

$xsl = new DomDocument;
$xsl->load('webtransform.xsl');

$xp = new XsltProcessor();
$xp->importStylesheet($xsl);

$xp->setParameter('', 'project', $project);
$xp->setParameter('', 'page', $page);
$xp->setParameter('', 'lang', $lang);
if ($screenshot!="") {
  $xp->setParameter('', 'screenshot', $screenshot);
}
$xp->registerPHPFunctions(array('guestbook_query', 'guestbook_pagesel', 'guestbook_err', 'guestbook_read',
                                'guestbook_name', 'guestbook_text', 'prepare_news'));

// transform the XML into HTML using the XSL file
if ($html = $xp->transformToXML($xml_doc)) {
    echo $html;
} else {
    trigger_error('XSL transformation failed.', E_USER_ERROR);
}
  
?>
