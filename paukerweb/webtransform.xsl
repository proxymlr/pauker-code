<?xml version='1.0'?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:php="http://php.net/xsl"
		xmlns="http://www.w3.org/1999/xhtml">
                

<xsl:output method='xml' 
   encoding='utf-8' 
   doctype-public="-//W3C//DTD XHTML 1.0 Transistional//EN"
   doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"/>
<xsl:param name="project"/>
<xsl:param name="page"/>
<xsl:param name="lang"/>
<xsl:param name="screenshot"/>

<xsl:template match="/">
<html>
<head>
 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
 <meta name="keywords" content="pauker, flash card, learning, vocables, leitner, java, linux, software"/>
 <link rel="stylesheet" type="text/css" href="main.css"/>
 <link rel="icon" href="images/pauker-web-icon.png" type="image/png"/>
 <title><xsl:apply-templates select="paukerweb/project" mode="title"/></title>
</head>
<body>

<!-- Top -->
<div class="top">
  <table class="top"><tr>
    <td class="topleft"></td>
    <xsl:choose>
      <xsl:when test="$project='pauker'">
         <td>
	   <table class="topsel"><tr><td>
             <img style="height: 1.5em;" src="images/repeat.png"/></td><td><b><sub>^</sub>Pauker<sub>^</sub></b>
	   </td></tr></table>
	 </td>
      </xsl:when>
      <xsl:otherwise>
         <td><a href="pauker.php?target=home&amp;lang={$lang}">
	   <table class="topitem"><tr><td>
	    <img style="height: 1.5em;" src="images/repeat.png"/></td><td>&#160;<a href="pauker.php?target=home&amp;lang={$lang}">Pauker</a>
	   </td></tr></table>
	 </a></td>
      </xsl:otherwise>
    </xsl:choose>
    <xsl:choose>
      <xsl:when test="$project='minipauker'">
         <td>
	   <table class="topsel"><tr><td>
	     <img style="height: 1.5em;" src="images/minipauker3.gif"/></td><td><b><sub>^</sub>MiniPauker<sub>^</sub></b>
	   </td></tr></table>
	 </td>
      </xsl:when>
      <xsl:otherwise>
         <td><a href="pauker.php?target=home&amp;lang={$lang}&amp;project=minipauker">
  	   <table class="topitem"><tr><td>
	     <img style="height: 1.5em;" src="images/minipauker3.gif"/></td><td>&#160;<a href="pauker.php?target=home&amp;lang={$lang}&amp;project=minipauker">MiniPauker</a>
	   </td></tr></table>
	 </a></td>
      </xsl:otherwise>
    </xsl:choose>
  <td class="sflogo"><a href="http://sourceforge.net/projects/pauker"><img src="http://sflogo.sourceforge.net/sflogo.php?group_id=40334&amp;type=12" width="120" height="30" alt="Get Pauker at SourceForge.net. Fast, secure and Free Open Source software downloads"/></a></td>
  </tr></table>
</div>

<!-- Left -->
<table><tr><td>
  <table class="menu"><tbody>
    <xsl:apply-templates select="paukerweb/project" mode="pagelist"/>  
  </tbody></table>
  <div class="langmenu">
    <xsl:apply-templates select="paukerweb/languages"/>  
  </div>


 
  <xsl:choose>
    <xsl:when test="$project='pauker'">
      <div class="paypal">
        <xsl:choose>
          <xsl:when test="$lang='en'">
            <span>Pauker Development</span><br/>
          </xsl:when>
          <xsl:when test="$lang='de'">
            <span>Pauker Entwicklung</span><br/>
          </xsl:when>
        </xsl:choose>
  
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
          <input type="hidden" name="cmd" value="_xclick"/>
          <input type="hidden" name="business" value="Ronny.Standtke@gmx.de"/>
          <input type="hidden" name="item_name" value="Pauker"/>
          <input type="hidden" name="currency_code" value="EUR"/>
          <input type="hidden" name="image_url" value="http://pauker.sourceforge.net/de/pics/pauker_paypal.png"/>
          <input type="hidden" name="no_shipping" value="1"/>
          <input type="hidden" name="return" value="http://pauker.sourceforge.net/de/danke.html"/>
          <input type="hidden" name="cancel_return" value="http://pauker.sourceforge.net/en/ohh.html"/>
          <input type="hidden" name="cn" value="Erzähl mir was :-)"/>
          <input type="image" src="https://www.paypal.com/images/x-click-but04.gif" name="submit"
          alt="Donate for Pauker" />
        </form>
      </div>
    </xsl:when>
    <xsl:when test="$project='minipauker'">
      <div class="paypal">
        <xsl:choose>
          <xsl:when test="$lang='en'">
            <span>MiniPauker Development</span><br/>
          </xsl:when>
          <xsl:when test="$lang='de'">
            <span>MiniPauker Entwicklung</span><br/>
          </xsl:when>
        </xsl:choose>
<a href="http://sourceforge.net/donate/index.php?group_id=159194"><img src="http://images.sourceforge.net/images/x-click-but7.gif" width="88" height="32" border="0" alt="Support This Project" /> </a>
      </div>
    </xsl:when>
  </xsl:choose>
 
</td>

<!-- Content -->
<td class="text">
<xsl:apply-templates select="paukerweb/project" mode="content"/>  
</td>
</tr></table></body>
</html>
</xsl:template>

<xsl:template match="language">
<span>
  <xsl:choose>
    <xsl:when test=".=$lang">
      <xsl:attribute name="class">langsel</xsl:attribute>
      <img><xsl:attribute name="src">images/flag-<xsl:value-of select="."/>.png</xsl:attribute><xsl:attribute name="alt"><xsl:value-of select="."/></xsl:attribute></img>
    </xsl:when>
    <xsl:otherwise>
      <a><xsl:attribute name="href">pauker.php?page=<xsl:value-of select="$page"/>&amp;lang=<xsl:value-of select="."/><xsl:choose><xsl:when test="$project='minipauker'">&amp;project=minipauker</xsl:when></xsl:choose></xsl:attribute>
      <img><xsl:attribute name="src">images/flag-<xsl:value-of select="."/>.png</xsl:attribute><xsl:attribute name="alt"><xsl:value-of select="."/></xsl:attribute></img>
      </a>
    </xsl:otherwise>
  </xsl:choose>
</span>
</xsl:template>

<xsl:template match="project" mode="title">
  <xsl:if test="@id=$project">
    <xsl:apply-templates select="pages" mode="title"/>
  </xsl:if>
</xsl:template>

<xsl:template match="pages" mode="title">
  <xsl:choose>
    <xsl:when test="$project='minipauker'">MiniPauker</xsl:when>
    <xsl:otherwise>Pauker</xsl:otherwise>
  </xsl:choose>
  <xsl:for-each select="*">
    <xsl:if test="@id=$page">
      <xsl:for-each select="name">
        <xsl:for-each select="*">
          <xsl:if test="name()=$lang">::<xsl:value-of select="."/></xsl:if>
        </xsl:for-each>
      </xsl:for-each>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="project" mode="pagelist">
  <xsl:if test="@id=$project">
    <xsl:apply-templates select="pages" mode="pagelist"/>
  </xsl:if>
</xsl:template>

<xsl:template match="pages" mode="pagelist">
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="@id=$page">
        <tr><th><b>&gt;<xsl:apply-templates select="name"/></b></th></tr>
      </xsl:when>
      <xsl:otherwise>
        <tr><td><xsl:apply-templates select="name"/></td></tr>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:for-each>
</xsl:template>

<xsl:template match="name">
  <xsl:for-each select="*">
    <xsl:if test="name()=$lang">
       <a>
       <xsl:attribute name="href">pauker.php?page=<xsl:value-of select="../../@id"/>&amp;lang=<xsl:value-of select="$lang"/><xsl:choose><xsl:when test="$project='minipauker'">&amp;project=minipauker</xsl:when></xsl:choose></xsl:attribute>
       <xsl:value-of select="."/>
       </a>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="project" mode="content">
  <xsl:if test="@id=$project">
    <xsl:apply-templates mode="content"/>
  </xsl:if>
</xsl:template>

<xsl:template match="pages" mode="content">
  <xsl:for-each select="*">
    <xsl:if test="@id=$page">
       <xsl:apply-templates select="content"/>  
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="content">
  <xsl:for-each select="*">
    <xsl:if test="name()=$lang">
       <xsl:apply-templates/>  
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="list">
<ul><xsl:apply-templates/></ul>
</xsl:template>

<xsl:template match="numberlist">
<ol><xsl:apply-templates/></ol>
</xsl:template>

<xsl:template match="item">
<li><xsl:apply-templates/></li>
</xsl:template>

<xsl:template match="external">
<span class="exlink">&#171;</span><xsl:if test="@img">
  <img><xsl:attribute name="src"><xsl:value-of select="@img"/></xsl:attribute></img><xsl:text> </xsl:text> 
</xsl:if>
<a href="{@link}"><xsl:value-of select="."/></a><span class="exlink">&#187;</span>
</xsl:template>

<!-- internal link -->
<xsl:template match="internal">
<a><xsl:attribute name="href">pauker.php?page=<xsl:value-of select="@link"/>&amp;lang=<xsl:choose><xsl:when test="@lang"><xsl:value-of select="@lang"/></xsl:when><xsl:otherwise><xsl:value-of select="$lang"/></xsl:otherwise></xsl:choose><xsl:choose><xsl:when test="@project">&amp;project=<xsl:value-of select="@project"/></xsl:when><xsl:otherwise><xsl:if test="not($project='pauker')">&amp;project=<xsl:value-of select="$project"/></xsl:if></xsl:otherwise></xsl:choose></xsl:attribute>
<xsl:choose>
  <xsl:when test="@img">
    <table class="link"><tr><td>
    <img class="link"><xsl:attribute name="src"><xsl:value-of select="@img"/></xsl:attribute></img>
    </td></tr>
    <tr><td>
       <xsl:apply-templates/>
    </td></tr></table>
  </xsl:when>
  <xsl:otherwise>
    <xsl:apply-templates/>
  </xsl:otherwise>
</xsl:choose>
</a>
</xsl:template>

<xsl:template match="margin">
<div class="margin"><xsl:apply-templates/></div>
</xsl:template>

<xsl:template match="p">
<p><xsl:apply-templates/></p>
</xsl:template>

<xsl:template match="em">
<span class="em"><xsl:apply-templates/></span>
</xsl:template>

<xsl:template match="h1">
<h1><xsl:apply-templates/></h1>
</xsl:template>

<xsl:template match="h2">
<h2><xsl:apply-templates/></h2>
</xsl:template>

<xsl:template match="email">
<a href="mailto:{.}"><xsl:value-of select="."/></a>
</xsl:template>

<xsl:template match="image">
<div class="image">
  <img>
    <xsl:attribute name="alt"><xsl:value-of select="@caption"/></xsl:attribute>
    <xsl:attribute name="src"><xsl:value-of select="@src"/></xsl:attribute>
  </img><br/>
  <xsl:choose>
    <xsl:when test="$lang='en'">
       <span>Figure: <xsl:value-of select="@caption"/></span>
    </xsl:when>
    <xsl:when test="$lang='de'">
       <span>Abbildung: <xsl:value-of select="@caption"/></span>
    </xsl:when>
  </xsl:choose>
</div>
</xsl:template>

<xsl:template match="imglist">
<table class="imglist">
   <xsl:apply-templates select="item" mode="imglist"/>
</table>
</xsl:template>

<xsl:template match="item" mode="imglist">
<tr><td class="imglistimg"><img><xsl:attribute name="src"><xsl:value-of select="@img"/></xsl:attribute></img></td><td><xsl:apply-templates/></td></tr>
</xsl:template>

<xsl:template match="paukerwebstart">
<p><div style="float:left; margin-right: 1em;"><a href="http://pauker.sourceforge.net/webstart/pauker.jnlp"><img alt="Pauker Java WebStart" src="images/webstart.png" align="middle"/></a></div><table><tr><td><xsl:value-of select="prompt"/><br/><div style="margin-left: 2em; margin-top: 0.5em;"><small><xsl:value-of select="alternative"/><br/><code>javaws http://pauker.sourceforge.net/webstart/pauker.jnlp</code></small></div></td></tr></table></p>
</xsl:template>

<xsl:template match="sourceforge-box">
<xsl:choose>
<xsl:when test="@id='minipauker'">
<table class="sourceforge-box">
  <tr><td>
     <xsl:value-of select="titel"/>
  </td></tr>
  <tr><td>
     <span class="exlink">&#171;</span>
     <a href="http://sourceforge.net/projects/minipauker"><xsl:value-of select="project"/></a><span class="exlink">&#187;</span>
     </td></tr>
  <tr><td>
    <span class="exlink">&#171;</span>
    <a href="http://sourceforge.net/tracker/?group_id=159194&amp;atid=810862"><xsl:value-of select="bugs"/></a>
    <span class="exlink">&#187;</span>
  </td></tr>
  <tr><td>
    <span class="exlink">&#171;</span>
    <a href="http://sourceforge.net/tracker/?group_id=159194&amp;atid=810865"><xsl:value-of select="features"/></a>
    <span class="exlink">&#187;</span>
  </td></tr>
  <tr><td>
    <span class="exlink">&#171;</span>
    <a href="http://sourceforge.net/svn/?group_id=159194"><xsl:value-of select="sourcecode"/></a>
    <span class="exlink">&#187;</span>
  </td></tr>
</table>
</xsl:when>
<xsl:otherwise>
<table class="sourceforge-box">
  <tr><td>
     <xsl:value-of select="titel"/>
  </td></tr>
  <tr><td>
     <span class="exlink">&#171;</span>
     <a href="http://sourceforge.net/projects/pauker"><xsl:value-of select="project"/></a><span class="exlink">&#187;</span>
     </td></tr>
  <tr><td>
    <span class="exlink">&#171;</span>
    <a href="http://sourceforge.net/tracker/?atid=427725&amp;group_id=40334&amp;func=browse"><xsl:value-of select="bugs"/></a>
    <span class="exlink">&#187;</span>
  </td></tr>
  <tr><td>
    <span class="exlink">&#171;</span>
    <a href="http://sourceforge.net/tracker/?atid=427728&amp;group_id=40334&amp;func=browse"><xsl:value-of select="features"/></a>
    <span class="exlink">&#187;</span>
  </td></tr>
  <tr><td>
    <span class="exlink">&#171;</span>
    <a href="http://sourceforge.net/cvs/?group_id=40334"><xsl:value-of select="sourcecode"/></a>
    <span class="exlink">&#187;</span>
  </td></tr>
</table>
</xsl:otherwise>
</xsl:choose>
</xsl:template>

<xsl:template match="screenshots">
<table><tr><td class="text">
<table class="thumbnails">
  <xsl:apply-templates mode="thumbnails"/>
</table>
</td>
<td class="text">
<xsl:choose>
  <xsl:when test="$screenshot">
     <xsl:apply-templates mode="largeshot"/>
  </xsl:when>
  <xsl:otherwise>
    <span class="em"><xsl:value-of select="@prompt"/></span>
  </xsl:otherwise>
</xsl:choose>
</td>
</tr></table>
</xsl:template>


<xsl:template match="screenshot" mode="thumbnails">
<tr><td>
<a>
<xsl:attribute name="href">pauker.php?target=screenshots&amp;lang=<xsl:value-of select="$lang"/>&amp;screenshot=<xsl:value-of select="@name"/></xsl:attribute>
<img><xsl:attribute name="src"><xsl:value-of select="@thumbnail"/></xsl:attribute></img>
</a>
<br/><xsl:apply-templates/>
</td></tr>
</xsl:template>

<xsl:template match="screenshot" mode="largeshot">
  <xsl:if test="$screenshot=@name">
    <img><xsl:attribute name="src"><xsl:value-of select="@img"/></xsl:attribute></img>
  </xsl:if>
</xsl:template>

<xsl:template match="lessons">
  <table class="lessons">
    <xsl:apply-templates selecte="lesson"/>
  </table>
</xsl:template>

<xsl:template match="lesson">
  <tr><td>
    <xsl:apply-templates select="name" mode="lesson"/>
  </td><td>
    <xsl:apply-templates select="file"/>
  </td></tr>
</xsl:template>

<xsl:template match="name" mode="lesson">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="file">
   <a><xsl:attribute name="href">lessons/<xsl:value-of select="@name"/></xsl:attribute><xsl:value-of select="@name"/></a>
   <xsl:choose>
     <xsl:when test="not(../@nobreak)"> 
       <br/>
     </xsl:when>
     <xsl:otherwise>
       <xsl:text> </xsl:text>
     </xsl:otherwise>
   </xsl:choose>
</xsl:template>

<xsl:template match="credits">
  <xsl:apply-templates/>
</xsl:template>
  
<xsl:template match="credit">
  <b><xsl:value-of select="name"/></b>
    <xsl:if test="occasion">
    - <xsl:value-of select="occasion"/>
    </xsl:if>  
  <br/>
</xsl:template>


<xsl:template match="guestbook">
<xsl:if test="not(php:function('guestbook_err',.)='')">
<h2> <xsl:value-of select="php:function('guestbook_err',.)"/> </h2>
</xsl:if>
<form method="post" action="pauker.php?page={$page}&amp;lang={$lang}">
 <table class="journalise">
  <tr>
   <td>Name</td>
   <td><input type="text" name="name" size="60"><xsl:attribute name="value"><xsl:value-of select="php:function('guestbook_name')"/></xsl:attribute></input></td>
  </tr>
  <tr>
   <td>Text</td>
   <td><textarea  name="text" cols="60" rows="15"><xsl:value-of select="php:function('guestbook_text')"/></textarea></td>
  </tr>
  <tr>
   <td colspan="2"><xsl:value-of select="@captcha_1"/><img src="images/key.png"/><xsl:value-of select="@captcha_2"/> 
      <xsl:text> </xsl:text><input type="text" name="keyx" size="40"/></td>
  </tr>
  <tr>
   <td></td>
   <td><input type="submit" name="submit" value="Journalise">
     <xsl:attribute name="value"><xsl:value-of select="@journalise"/></xsl:attribute>
   </input></td>
  </tr>
 </table>
</form>
<xsl:value-of select="php:function('guestbook_query')"/>
<table class="pagesel"><tr>
<xsl:for-each select="php:function('guestbook_pagesel')">
  <xsl:apply-templates mode="pagesel"/>
</xsl:for-each>
</tr></table>
<table class="guestbook">
<xsl:for-each select="php:function('guestbook_read')">
  <xsl:apply-templates mode="guestbook"/>
</xsl:for-each>
</table>
</xsl:template>

<xsl:template match="left|right" mode="pagesel">
  <td><table><tr>
  <xsl:apply-templates mode="side"/>
  </tr></table></td>
</xsl:template>

<xsl:template match="center" mode="pagesel">
  <td><table class="pagesel-center"><tr>
<td><a href="pauker.php?page={$page}&amp;lang={$lang}&amp;pn={@pn}"><xsl:value-of select="@pn"/></a></td>
  </tr></table></td>
</xsl:template>

<xsl:template match="gpage" mode="side">
<td><a href="pauker.php?page={$page}&amp;lang={$lang}&amp;pn={@pn}"><xsl:value-of select="@pn"/></a></td>
</xsl:template>

<xsl:template match="entry" mode="guestbook">
<tr>
<td><xsl:value-of select="name"/><br/><xsl:value-of select="date"/></td>
<td>
  <table><tr><td>
  <xsl:apply-templates select="text" mode="guestbook"/>
  </td></tr></table>
</td>
<xsl:if test="delete">
  <td><a href="pauker.php?page={$page}&amp;lang={$lang}&amp;action=delete&amp;id={delete/@id}"><img src="images/delete.jpg"/></a></td>
</xsl:if>
</tr>
</xsl:template>

<xsl:template match="br" mode="guestbook">
<br/>
</xsl:template>

<xsl:template match="news">
  <div style="float:right;">
    <a href="{@rss}"><img src="https://s.fsdn.com/sf/images/phoneix/icons/silk/feed.png"/></a>
  </div>
  <xsl:for-each select="document(@rss)">
    <xsl:apply-templates mode="news"/>
  </xsl:for-each>
</xsl:template>

<xsl:template match="rss" mode="news">
  <xsl:apply-templates mode="news"/>
</xsl:template>

<xsl:template match="channel" mode="news">
  <xsl:apply-templates select="item" mode="news"/>
</xsl:template>


<xsl:template match="item" mode="news">
<table class="news">
  <xsl:variable name="text"><xsl:value-of select="description"/></xsl:variable>
  <tr><td><b><xsl:value-of select="title"/></b> - <xsl:value-of select="pubDate"/></td></tr>
  <tr><td><table><tr><td><xsl:copy-of select="php:function('prepare_news', $text)"/></td></tr></table></td></tr>
</table>
</xsl:template>


</xsl:stylesheet>
