/*
 * PaukerFileView.java
 *
 * Created on 28. Juli 2002, 13:31
 */
package pauker.program.gui.swing;

import java.io.File;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.filechooser.FileView;

/**
 * a FileView for Pauker lessons (just provides a custom lesson icon)
 * @author Ronny.Standtke@gmx.net
 */
public class PaukerFileView extends FileView {

    private final Icon paukerIcon =
            new ImageIcon(getClass().getResource("/pauker/icons/16x16/pauker.png"));

    @Override
    public Icon getIcon(File file) {
        String path = file.getPath();
        if (path.endsWith(".xml.gz") || path.endsWith(".pau.gz") ||
                path.endsWith(".pau")) {
            return paukerIcon;
        } else {
            return null;
        }
    }
}
