package pauker.program.gui.swing;

import java.beans.*;

public class StatisticPanelBeanInfo extends SimpleBeanInfo {
    



    // Beschreibung der Bean//GEN-FIRST:BeanDescriptor
    /*lazy BeanDescriptor*/
    private static BeanDescriptor getBdescriptor(){
        BeanDescriptor beanDescriptor = new BeanDescriptor  ( pauker.program.gui.swing.StatisticPanel.class , null ); // NOI18N//GEN-HEADEREND:BeanDescriptor
        
        // Here you can add code for customizing the BeanDescriptor.
        
        return beanDescriptor;     }//GEN-LAST:BeanDescriptor
    
    
    // Eigenschaftsbezeichner//GEN-FIRST:Properties
    private static final int PROPERTY_accessibleContext = 0;
    private static final int PROPERTY_actionMap = 1;
    private static final int PROPERTY_alignmentX = 2;
    private static final int PROPERTY_alignmentY = 3;
    private static final int PROPERTY_ancestorListeners = 4;
    private static final int PROPERTY_autoscrolls = 5;
    private static final int PROPERTY_background = 6;
    private static final int PROPERTY_backgroundSet = 7;
    private static final int PROPERTY_border = 8;
    private static final int PROPERTY_bounds = 9;
    private static final int PROPERTY_colorModel = 10;
    private static final int PROPERTY_component = 11;
    private static final int PROPERTY_componentCount = 12;
    private static final int PROPERTY_componentListeners = 13;
    private static final int PROPERTY_componentOrientation = 14;
    private static final int PROPERTY_components = 15;
    private static final int PROPERTY_containerListeners = 16;
    private static final int PROPERTY_cursor = 17;
    private static final int PROPERTY_cursorSet = 18;
    private static final int PROPERTY_debugGraphicsOptions = 19;
    private static final int PROPERTY_displayable = 20;
    private static final int PROPERTY_doubleBuffered = 21;
    private static final int PROPERTY_dropTarget = 22;
    private static final int PROPERTY_enabled = 23;
    private static final int PROPERTY_focusable = 24;
    private static final int PROPERTY_focusCycleRoot = 25;
    private static final int PROPERTY_focusCycleRootAncestor = 26;
    private static final int PROPERTY_focusListeners = 27;
    private static final int PROPERTY_focusOwner = 28;
    private static final int PROPERTY_focusTraversable = 29;
    private static final int PROPERTY_focusTraversalKeys = 30;
    private static final int PROPERTY_focusTraversalKeysEnabled = 31;
    private static final int PROPERTY_focusTraversalPolicy = 32;
    private static final int PROPERTY_focusTraversalPolicySet = 33;
    private static final int PROPERTY_font = 34;
    private static final int PROPERTY_fontSet = 35;
    private static final int PROPERTY_foreground = 36;
    private static final int PROPERTY_foregroundSet = 37;
    private static final int PROPERTY_graphics = 38;
    private static final int PROPERTY_graphicsConfiguration = 39;
    private static final int PROPERTY_height = 40;
    private static final int PROPERTY_hierarchyBoundsListeners = 41;
    private static final int PROPERTY_hierarchyListeners = 42;
    private static final int PROPERTY_ignoreRepaint = 43;
    private static final int PROPERTY_inputContext = 44;
    private static final int PROPERTY_inputMethodListeners = 45;
    private static final int PROPERTY_inputMethodRequests = 46;
    private static final int PROPERTY_inputVerifier = 47;
    private static final int PROPERTY_insets = 48;
    private static final int PROPERTY_keyListeners = 49;
    private static final int PROPERTY_layout = 50;
    private static final int PROPERTY_lesson = 51;
    private static final int PROPERTY_lightweight = 52;
    private static final int PROPERTY_locale = 53;
    private static final int PROPERTY_locationOnScreen = 54;
    private static final int PROPERTY_managingFocus = 55;
    private static final int PROPERTY_maximumSize = 56;
    private static final int PROPERTY_maximumSizeSet = 57;
    private static final int PROPERTY_minimumSize = 58;
    private static final int PROPERTY_minimumSizeSet = 59;
    private static final int PROPERTY_mouseListeners = 60;
    private static final int PROPERTY_mouseMotionListeners = 61;
    private static final int PROPERTY_mouseWheelListeners = 62;
    private static final int PROPERTY_name = 63;
    private static final int PROPERTY_nextFocusableComponent = 64;
    private static final int PROPERTY_opaque = 65;
    private static final int PROPERTY_optimizedDrawingEnabled = 66;
    private static final int PROPERTY_paintingTile = 67;
    private static final int PROPERTY_parent = 68;
    private static final int PROPERTY_peer = 69;
    private static final int PROPERTY_preferredSize = 70;
    private static final int PROPERTY_preferredSizeSet = 71;
    private static final int PROPERTY_propertyChangeListeners = 72;
    private static final int PROPERTY_registeredKeyStrokes = 73;
    private static final int PROPERTY_requestFocusEnabled = 74;
    private static final int PROPERTY_rootPane = 75;
    private static final int PROPERTY_showing = 76;
    private static final int PROPERTY_toolkit = 77;
    private static final int PROPERTY_toolTipText = 78;
    private static final int PROPERTY_topLevelAncestor = 79;
    private static final int PROPERTY_transferHandler = 80;
    private static final int PROPERTY_treeLock = 81;
    private static final int PROPERTY_UI = 82;
    private static final int PROPERTY_UIClassID = 83;
    private static final int PROPERTY_valid = 84;
    private static final int PROPERTY_validateRoot = 85;
    private static final int PROPERTY_verifyInputWhenFocusTarget = 86;
    private static final int PROPERTY_vetoableChangeListeners = 87;
    private static final int PROPERTY_visible = 88;
    private static final int PROPERTY_visibleRect = 89;
    private static final int PROPERTY_width = 90;
    private static final int PROPERTY_x = 91;
    private static final int PROPERTY_y = 92;

    // Eigenschaftsfolgen 
    /*lazy PropertyDescriptor*/
    private static PropertyDescriptor[] getPdescriptor(){
        PropertyDescriptor[] properties = new PropertyDescriptor[93];
    
        try {
            properties[PROPERTY_accessibleContext] = new PropertyDescriptor ( "accessibleContext", pauker.program.gui.swing.StatisticPanel.class, "getAccessibleContext", null ); // NOI18N
            properties[PROPERTY_actionMap] = new PropertyDescriptor ( "actionMap", pauker.program.gui.swing.StatisticPanel.class, "getActionMap", "setActionMap" ); // NOI18N
            properties[PROPERTY_alignmentX] = new PropertyDescriptor ( "alignmentX", pauker.program.gui.swing.StatisticPanel.class, "getAlignmentX", "setAlignmentX" ); // NOI18N
            properties[PROPERTY_alignmentY] = new PropertyDescriptor ( "alignmentY", pauker.program.gui.swing.StatisticPanel.class, "getAlignmentY", "setAlignmentY" ); // NOI18N
            properties[PROPERTY_ancestorListeners] = new PropertyDescriptor ( "ancestorListeners", pauker.program.gui.swing.StatisticPanel.class, "getAncestorListeners", null ); // NOI18N
            properties[PROPERTY_autoscrolls] = new PropertyDescriptor ( "autoscrolls", pauker.program.gui.swing.StatisticPanel.class, "getAutoscrolls", "setAutoscrolls" ); // NOI18N
            properties[PROPERTY_background] = new PropertyDescriptor ( "background", pauker.program.gui.swing.StatisticPanel.class, "getBackground", "setBackground" ); // NOI18N
            properties[PROPERTY_backgroundSet] = new PropertyDescriptor ( "backgroundSet", pauker.program.gui.swing.StatisticPanel.class, "isBackgroundSet", null ); // NOI18N
            properties[PROPERTY_border] = new PropertyDescriptor ( "border", pauker.program.gui.swing.StatisticPanel.class, "getBorder", "setBorder" ); // NOI18N
            properties[PROPERTY_bounds] = new PropertyDescriptor ( "bounds", pauker.program.gui.swing.StatisticPanel.class, "getBounds", "setBounds" ); // NOI18N
            properties[PROPERTY_colorModel] = new PropertyDescriptor ( "colorModel", pauker.program.gui.swing.StatisticPanel.class, "getColorModel", null ); // NOI18N
            properties[PROPERTY_component] = new IndexedPropertyDescriptor ( "component", pauker.program.gui.swing.StatisticPanel.class, null, null, "getComponent", null ); // NOI18N
            properties[PROPERTY_componentCount] = new PropertyDescriptor ( "componentCount", pauker.program.gui.swing.StatisticPanel.class, "getComponentCount", null ); // NOI18N
            properties[PROPERTY_componentListeners] = new PropertyDescriptor ( "componentListeners", pauker.program.gui.swing.StatisticPanel.class, "getComponentListeners", null ); // NOI18N
            properties[PROPERTY_componentOrientation] = new PropertyDescriptor ( "componentOrientation", pauker.program.gui.swing.StatisticPanel.class, "getComponentOrientation", "setComponentOrientation" ); // NOI18N
            properties[PROPERTY_components] = new PropertyDescriptor ( "components", pauker.program.gui.swing.StatisticPanel.class, "getComponents", null ); // NOI18N
            properties[PROPERTY_containerListeners] = new PropertyDescriptor ( "containerListeners", pauker.program.gui.swing.StatisticPanel.class, "getContainerListeners", null ); // NOI18N
            properties[PROPERTY_cursor] = new PropertyDescriptor ( "cursor", pauker.program.gui.swing.StatisticPanel.class, "getCursor", "setCursor" ); // NOI18N
            properties[PROPERTY_cursorSet] = new PropertyDescriptor ( "cursorSet", pauker.program.gui.swing.StatisticPanel.class, "isCursorSet", null ); // NOI18N
            properties[PROPERTY_debugGraphicsOptions] = new PropertyDescriptor ( "debugGraphicsOptions", pauker.program.gui.swing.StatisticPanel.class, "getDebugGraphicsOptions", "setDebugGraphicsOptions" ); // NOI18N
            properties[PROPERTY_displayable] = new PropertyDescriptor ( "displayable", pauker.program.gui.swing.StatisticPanel.class, "isDisplayable", null ); // NOI18N
            properties[PROPERTY_doubleBuffered] = new PropertyDescriptor ( "doubleBuffered", pauker.program.gui.swing.StatisticPanel.class, "isDoubleBuffered", "setDoubleBuffered" ); // NOI18N
            properties[PROPERTY_dropTarget] = new PropertyDescriptor ( "dropTarget", pauker.program.gui.swing.StatisticPanel.class, "getDropTarget", "setDropTarget" ); // NOI18N
            properties[PROPERTY_enabled] = new PropertyDescriptor ( "enabled", pauker.program.gui.swing.StatisticPanel.class, "isEnabled", "setEnabled" ); // NOI18N
            properties[PROPERTY_focusable] = new PropertyDescriptor ( "focusable", pauker.program.gui.swing.StatisticPanel.class, "isFocusable", "setFocusable" ); // NOI18N
            properties[PROPERTY_focusCycleRoot] = new PropertyDescriptor ( "focusCycleRoot", pauker.program.gui.swing.StatisticPanel.class, "isFocusCycleRoot", "setFocusCycleRoot" ); // NOI18N
            properties[PROPERTY_focusCycleRootAncestor] = new PropertyDescriptor ( "focusCycleRootAncestor", pauker.program.gui.swing.StatisticPanel.class, "getFocusCycleRootAncestor", null ); // NOI18N
            properties[PROPERTY_focusListeners] = new PropertyDescriptor ( "focusListeners", pauker.program.gui.swing.StatisticPanel.class, "getFocusListeners", null ); // NOI18N
            properties[PROPERTY_focusOwner] = new PropertyDescriptor ( "focusOwner", pauker.program.gui.swing.StatisticPanel.class, "isFocusOwner", null ); // NOI18N
            properties[PROPERTY_focusTraversable] = new PropertyDescriptor ( "focusTraversable", pauker.program.gui.swing.StatisticPanel.class, "isFocusTraversable", null ); // NOI18N
            properties[PROPERTY_focusTraversalKeys] = new IndexedPropertyDescriptor ( "focusTraversalKeys", pauker.program.gui.swing.StatisticPanel.class, null, null, "getFocusTraversalKeys", "setFocusTraversalKeys" ); // NOI18N
            properties[PROPERTY_focusTraversalKeysEnabled] = new PropertyDescriptor ( "focusTraversalKeysEnabled", pauker.program.gui.swing.StatisticPanel.class, "getFocusTraversalKeysEnabled", "setFocusTraversalKeysEnabled" ); // NOI18N
            properties[PROPERTY_focusTraversalPolicy] = new PropertyDescriptor ( "focusTraversalPolicy", pauker.program.gui.swing.StatisticPanel.class, "getFocusTraversalPolicy", "setFocusTraversalPolicy" ); // NOI18N
            properties[PROPERTY_focusTraversalPolicySet] = new PropertyDescriptor ( "focusTraversalPolicySet", pauker.program.gui.swing.StatisticPanel.class, "isFocusTraversalPolicySet", null ); // NOI18N
            properties[PROPERTY_font] = new PropertyDescriptor ( "font", pauker.program.gui.swing.StatisticPanel.class, "getFont", "setFont" ); // NOI18N
            properties[PROPERTY_fontSet] = new PropertyDescriptor ( "fontSet", pauker.program.gui.swing.StatisticPanel.class, "isFontSet", null ); // NOI18N
            properties[PROPERTY_foreground] = new PropertyDescriptor ( "foreground", pauker.program.gui.swing.StatisticPanel.class, "getForeground", "setForeground" ); // NOI18N
            properties[PROPERTY_foregroundSet] = new PropertyDescriptor ( "foregroundSet", pauker.program.gui.swing.StatisticPanel.class, "isForegroundSet", null ); // NOI18N
            properties[PROPERTY_graphics] = new PropertyDescriptor ( "graphics", pauker.program.gui.swing.StatisticPanel.class, "getGraphics", null ); // NOI18N
            properties[PROPERTY_graphicsConfiguration] = new PropertyDescriptor ( "graphicsConfiguration", pauker.program.gui.swing.StatisticPanel.class, "getGraphicsConfiguration", null ); // NOI18N
            properties[PROPERTY_height] = new PropertyDescriptor ( "height", pauker.program.gui.swing.StatisticPanel.class, "getHeight", null ); // NOI18N
            properties[PROPERTY_hierarchyBoundsListeners] = new PropertyDescriptor ( "hierarchyBoundsListeners", pauker.program.gui.swing.StatisticPanel.class, "getHierarchyBoundsListeners", null ); // NOI18N
            properties[PROPERTY_hierarchyListeners] = new PropertyDescriptor ( "hierarchyListeners", pauker.program.gui.swing.StatisticPanel.class, "getHierarchyListeners", null ); // NOI18N
            properties[PROPERTY_ignoreRepaint] = new PropertyDescriptor ( "ignoreRepaint", pauker.program.gui.swing.StatisticPanel.class, "getIgnoreRepaint", "setIgnoreRepaint" ); // NOI18N
            properties[PROPERTY_inputContext] = new PropertyDescriptor ( "inputContext", pauker.program.gui.swing.StatisticPanel.class, "getInputContext", null ); // NOI18N
            properties[PROPERTY_inputMethodListeners] = new PropertyDescriptor ( "inputMethodListeners", pauker.program.gui.swing.StatisticPanel.class, "getInputMethodListeners", null ); // NOI18N
            properties[PROPERTY_inputMethodRequests] = new PropertyDescriptor ( "inputMethodRequests", pauker.program.gui.swing.StatisticPanel.class, "getInputMethodRequests", null ); // NOI18N
            properties[PROPERTY_inputVerifier] = new PropertyDescriptor ( "inputVerifier", pauker.program.gui.swing.StatisticPanel.class, "getInputVerifier", "setInputVerifier" ); // NOI18N
            properties[PROPERTY_insets] = new PropertyDescriptor ( "insets", pauker.program.gui.swing.StatisticPanel.class, "getInsets", null ); // NOI18N
            properties[PROPERTY_keyListeners] = new PropertyDescriptor ( "keyListeners", pauker.program.gui.swing.StatisticPanel.class, "getKeyListeners", null ); // NOI18N
            properties[PROPERTY_layout] = new PropertyDescriptor ( "layout", pauker.program.gui.swing.StatisticPanel.class, "getLayout", "setLayout" ); // NOI18N
            properties[PROPERTY_lesson] = new PropertyDescriptor ( "lesson", pauker.program.gui.swing.StatisticPanel.class, null, "setLesson" ); // NOI18N
            properties[PROPERTY_lightweight] = new PropertyDescriptor ( "lightweight", pauker.program.gui.swing.StatisticPanel.class, "isLightweight", null ); // NOI18N
            properties[PROPERTY_locale] = new PropertyDescriptor ( "locale", pauker.program.gui.swing.StatisticPanel.class, "getLocale", "setLocale" ); // NOI18N
            properties[PROPERTY_locationOnScreen] = new PropertyDescriptor ( "locationOnScreen", pauker.program.gui.swing.StatisticPanel.class, "getLocationOnScreen", null ); // NOI18N
            properties[PROPERTY_managingFocus] = new PropertyDescriptor ( "managingFocus", pauker.program.gui.swing.StatisticPanel.class, "isManagingFocus", null ); // NOI18N
            properties[PROPERTY_maximumSize] = new PropertyDescriptor ( "maximumSize", pauker.program.gui.swing.StatisticPanel.class, "getMaximumSize", "setMaximumSize" ); // NOI18N
            properties[PROPERTY_maximumSizeSet] = new PropertyDescriptor ( "maximumSizeSet", pauker.program.gui.swing.StatisticPanel.class, "isMaximumSizeSet", null ); // NOI18N
            properties[PROPERTY_minimumSize] = new PropertyDescriptor ( "minimumSize", pauker.program.gui.swing.StatisticPanel.class, "getMinimumSize", "setMinimumSize" ); // NOI18N
            properties[PROPERTY_minimumSizeSet] = new PropertyDescriptor ( "minimumSizeSet", pauker.program.gui.swing.StatisticPanel.class, "isMinimumSizeSet", null ); // NOI18N
            properties[PROPERTY_mouseListeners] = new PropertyDescriptor ( "mouseListeners", pauker.program.gui.swing.StatisticPanel.class, "getMouseListeners", null ); // NOI18N
            properties[PROPERTY_mouseMotionListeners] = new PropertyDescriptor ( "mouseMotionListeners", pauker.program.gui.swing.StatisticPanel.class, "getMouseMotionListeners", null ); // NOI18N
            properties[PROPERTY_mouseWheelListeners] = new PropertyDescriptor ( "mouseWheelListeners", pauker.program.gui.swing.StatisticPanel.class, "getMouseWheelListeners", null ); // NOI18N
            properties[PROPERTY_name] = new PropertyDescriptor ( "name", pauker.program.gui.swing.StatisticPanel.class, "getName", "setName" ); // NOI18N
            properties[PROPERTY_nextFocusableComponent] = new PropertyDescriptor ( "nextFocusableComponent", pauker.program.gui.swing.StatisticPanel.class, "getNextFocusableComponent", "setNextFocusableComponent" ); // NOI18N
            properties[PROPERTY_opaque] = new PropertyDescriptor ( "opaque", pauker.program.gui.swing.StatisticPanel.class, "isOpaque", "setOpaque" ); // NOI18N
            properties[PROPERTY_optimizedDrawingEnabled] = new PropertyDescriptor ( "optimizedDrawingEnabled", pauker.program.gui.swing.StatisticPanel.class, "isOptimizedDrawingEnabled", null ); // NOI18N
            properties[PROPERTY_paintingTile] = new PropertyDescriptor ( "paintingTile", pauker.program.gui.swing.StatisticPanel.class, "isPaintingTile", null ); // NOI18N
            properties[PROPERTY_parent] = new PropertyDescriptor ( "parent", pauker.program.gui.swing.StatisticPanel.class, "getParent", null ); // NOI18N
            properties[PROPERTY_peer] = new PropertyDescriptor ( "peer", pauker.program.gui.swing.StatisticPanel.class, "getPeer", null ); // NOI18N
            properties[PROPERTY_preferredSize] = new PropertyDescriptor ( "preferredSize", pauker.program.gui.swing.StatisticPanel.class, "getPreferredSize", "setPreferredSize" ); // NOI18N
            properties[PROPERTY_preferredSizeSet] = new PropertyDescriptor ( "preferredSizeSet", pauker.program.gui.swing.StatisticPanel.class, "isPreferredSizeSet", null ); // NOI18N
            properties[PROPERTY_propertyChangeListeners] = new PropertyDescriptor ( "propertyChangeListeners", pauker.program.gui.swing.StatisticPanel.class, "getPropertyChangeListeners", null ); // NOI18N
            properties[PROPERTY_registeredKeyStrokes] = new PropertyDescriptor ( "registeredKeyStrokes", pauker.program.gui.swing.StatisticPanel.class, "getRegisteredKeyStrokes", null ); // NOI18N
            properties[PROPERTY_requestFocusEnabled] = new PropertyDescriptor ( "requestFocusEnabled", pauker.program.gui.swing.StatisticPanel.class, "isRequestFocusEnabled", "setRequestFocusEnabled" ); // NOI18N
            properties[PROPERTY_rootPane] = new PropertyDescriptor ( "rootPane", pauker.program.gui.swing.StatisticPanel.class, "getRootPane", null ); // NOI18N
            properties[PROPERTY_showing] = new PropertyDescriptor ( "showing", pauker.program.gui.swing.StatisticPanel.class, "isShowing", null ); // NOI18N
            properties[PROPERTY_toolkit] = new PropertyDescriptor ( "toolkit", pauker.program.gui.swing.StatisticPanel.class, "getToolkit", null ); // NOI18N
            properties[PROPERTY_toolTipText] = new PropertyDescriptor ( "toolTipText", pauker.program.gui.swing.StatisticPanel.class, "getToolTipText", "setToolTipText" ); // NOI18N
            properties[PROPERTY_topLevelAncestor] = new PropertyDescriptor ( "topLevelAncestor", pauker.program.gui.swing.StatisticPanel.class, "getTopLevelAncestor", null ); // NOI18N
            properties[PROPERTY_transferHandler] = new PropertyDescriptor ( "transferHandler", pauker.program.gui.swing.StatisticPanel.class, "getTransferHandler", "setTransferHandler" ); // NOI18N
            properties[PROPERTY_treeLock] = new PropertyDescriptor ( "treeLock", pauker.program.gui.swing.StatisticPanel.class, "getTreeLock", null ); // NOI18N
            properties[PROPERTY_UI] = new PropertyDescriptor ( "UI", pauker.program.gui.swing.StatisticPanel.class, "getUI", "setUI" ); // NOI18N
            properties[PROPERTY_UIClassID] = new PropertyDescriptor ( "UIClassID", pauker.program.gui.swing.StatisticPanel.class, "getUIClassID", null ); // NOI18N
            properties[PROPERTY_valid] = new PropertyDescriptor ( "valid", pauker.program.gui.swing.StatisticPanel.class, "isValid", null ); // NOI18N
            properties[PROPERTY_validateRoot] = new PropertyDescriptor ( "validateRoot", pauker.program.gui.swing.StatisticPanel.class, "isValidateRoot", null ); // NOI18N
            properties[PROPERTY_verifyInputWhenFocusTarget] = new PropertyDescriptor ( "verifyInputWhenFocusTarget", pauker.program.gui.swing.StatisticPanel.class, "getVerifyInputWhenFocusTarget", "setVerifyInputWhenFocusTarget" ); // NOI18N
            properties[PROPERTY_vetoableChangeListeners] = new PropertyDescriptor ( "vetoableChangeListeners", pauker.program.gui.swing.StatisticPanel.class, "getVetoableChangeListeners", null ); // NOI18N
            properties[PROPERTY_visible] = new PropertyDescriptor ( "visible", pauker.program.gui.swing.StatisticPanel.class, "isVisible", "setVisible" ); // NOI18N
            properties[PROPERTY_visibleRect] = new PropertyDescriptor ( "visibleRect", pauker.program.gui.swing.StatisticPanel.class, "getVisibleRect", null ); // NOI18N
            properties[PROPERTY_width] = new PropertyDescriptor ( "width", pauker.program.gui.swing.StatisticPanel.class, "getWidth", null ); // NOI18N
            properties[PROPERTY_x] = new PropertyDescriptor ( "x", pauker.program.gui.swing.StatisticPanel.class, "getX", null ); // NOI18N
            properties[PROPERTY_y] = new PropertyDescriptor ( "y", pauker.program.gui.swing.StatisticPanel.class, "getY", null ); // NOI18N
        }
        catch(IntrospectionException e) {
            e.printStackTrace();
        }//GEN-HEADEREND:Properties
        
        // Here you can add code for customizing the properties array.
        
        return properties;     }//GEN-LAST:Properties
    
    // Ereignismengenbezeichner//GEN-FIRST:Events
    private static final int EVENT_ancestorListener = 0;
    private static final int EVENT_componentListener = 1;
    private static final int EVENT_containerListener = 2;
    private static final int EVENT_focusListener = 3;
    private static final int EVENT_hierarchyBoundsListener = 4;
    private static final int EVENT_hierarchyListener = 5;
    private static final int EVENT_inputMethodListener = 6;
    private static final int EVENT_keyListener = 7;
    private static final int EVENT_mouseListener = 8;
    private static final int EVENT_mouseMotionListener = 9;
    private static final int EVENT_mouseWheelListener = 10;
    private static final int EVENT_propertyChangeListener = 11;
    private static final int EVENT_vetoableChangeListener = 12;

    // Ereignismengenfolgen
    /*lazy EventSetDescriptor*/
    private static EventSetDescriptor[] getEdescriptor(){
        EventSetDescriptor[] eventSets = new EventSetDescriptor[13];
    
        try {
            eventSets[EVENT_ancestorListener] = new EventSetDescriptor ( pauker.program.gui.swing.StatisticPanel.class, "ancestorListener", javax.swing.event.AncestorListener.class, new String[] {"ancestorAdded", "ancestorMoved", "ancestorRemoved"}, "addAncestorListener", "removeAncestorListener" ); // NOI18N
            eventSets[EVENT_componentListener] = new EventSetDescriptor ( pauker.program.gui.swing.StatisticPanel.class, "componentListener", java.awt.event.ComponentListener.class, new String[] {"componentHidden", "componentMoved", "componentResized", "componentShown"}, "addComponentListener", "removeComponentListener" ); // NOI18N
            eventSets[EVENT_containerListener] = new EventSetDescriptor ( pauker.program.gui.swing.StatisticPanel.class, "containerListener", java.awt.event.ContainerListener.class, new String[] {"componentAdded", "componentRemoved"}, "addContainerListener", "removeContainerListener" ); // NOI18N
            eventSets[EVENT_focusListener] = new EventSetDescriptor ( pauker.program.gui.swing.StatisticPanel.class, "focusListener", java.awt.event.FocusListener.class, new String[] {"focusGained", "focusLost"}, "addFocusListener", "removeFocusListener" ); // NOI18N
            eventSets[EVENT_hierarchyBoundsListener] = new EventSetDescriptor ( pauker.program.gui.swing.StatisticPanel.class, "hierarchyBoundsListener", java.awt.event.HierarchyBoundsListener.class, new String[] {"ancestorMoved", "ancestorResized"}, "addHierarchyBoundsListener", "removeHierarchyBoundsListener" ); // NOI18N
            eventSets[EVENT_hierarchyListener] = new EventSetDescriptor ( pauker.program.gui.swing.StatisticPanel.class, "hierarchyListener", java.awt.event.HierarchyListener.class, new String[] {"hierarchyChanged"}, "addHierarchyListener", "removeHierarchyListener" ); // NOI18N
            eventSets[EVENT_inputMethodListener] = new EventSetDescriptor ( pauker.program.gui.swing.StatisticPanel.class, "inputMethodListener", java.awt.event.InputMethodListener.class, new String[] {"caretPositionChanged", "inputMethodTextChanged"}, "addInputMethodListener", "removeInputMethodListener" ); // NOI18N
            eventSets[EVENT_keyListener] = new EventSetDescriptor ( pauker.program.gui.swing.StatisticPanel.class, "keyListener", java.awt.event.KeyListener.class, new String[] {"keyPressed", "keyReleased", "keyTyped"}, "addKeyListener", "removeKeyListener" ); // NOI18N
            eventSets[EVENT_mouseListener] = new EventSetDescriptor ( pauker.program.gui.swing.StatisticPanel.class, "mouseListener", java.awt.event.MouseListener.class, new String[] {"mouseClicked", "mouseEntered", "mouseExited", "mousePressed", "mouseReleased"}, "addMouseListener", "removeMouseListener" ); // NOI18N
            eventSets[EVENT_mouseMotionListener] = new EventSetDescriptor ( pauker.program.gui.swing.StatisticPanel.class, "mouseMotionListener", java.awt.event.MouseMotionListener.class, new String[] {"mouseDragged", "mouseMoved"}, "addMouseMotionListener", "removeMouseMotionListener" ); // NOI18N
            eventSets[EVENT_mouseWheelListener] = new EventSetDescriptor ( pauker.program.gui.swing.StatisticPanel.class, "mouseWheelListener", java.awt.event.MouseWheelListener.class, new String[] {"mouseWheelMoved"}, "addMouseWheelListener", "removeMouseWheelListener" ); // NOI18N
            eventSets[EVENT_propertyChangeListener] = new EventSetDescriptor ( pauker.program.gui.swing.StatisticPanel.class, "propertyChangeListener", java.beans.PropertyChangeListener.class, new String[] {"propertyChange"}, "addPropertyChangeListener", "removePropertyChangeListener" ); // NOI18N
            eventSets[EVENT_vetoableChangeListener] = new EventSetDescriptor ( pauker.program.gui.swing.StatisticPanel.class, "vetoableChangeListener", java.beans.VetoableChangeListener.class, new String[] {"vetoableChange"}, "addVetoableChangeListener", "removeVetoableChangeListener" ); // NOI18N
        }
        catch(IntrospectionException e) {
            e.printStackTrace();
        }//GEN-HEADEREND:Events
        
        // Here you can add code for customizing the event sets array.
        
        return eventSets;     }//GEN-LAST:Events
    
    // Methodenbezeichner//GEN-FIRST:Methods
    private static final int METHOD_action0 = 0;
    private static final int METHOD_add1 = 1;
    private static final int METHOD_addNotify2 = 2;
    private static final int METHOD_addPropertyChangeListener3 = 3;
    private static final int METHOD_applyComponentOrientation4 = 4;
    private static final int METHOD_areFocusTraversalKeysSet5 = 5;
    private static final int METHOD_bounds6 = 6;
    private static final int METHOD_checkImage7 = 7;
    private static final int METHOD_computeVisibleRect8 = 8;
    private static final int METHOD_contains9 = 9;
    private static final int METHOD_countComponents10 = 10;
    private static final int METHOD_createImage11 = 11;
    private static final int METHOD_createToolTip12 = 12;
    private static final int METHOD_createVolatileImage13 = 13;
    private static final int METHOD_deliverEvent14 = 14;
    private static final int METHOD_disable15 = 15;
    private static final int METHOD_dispatchEvent16 = 16;
    private static final int METHOD_doLayout17 = 17;
    private static final int METHOD_enable18 = 18;
    private static final int METHOD_enableInputMethods19 = 19;
    private static final int METHOD_findComponentAt20 = 20;
    private static final int METHOD_firePropertyChange21 = 21;
    private static final int METHOD_getActionForKeyStroke22 = 22;
    private static final int METHOD_getBounds23 = 23;
    private static final int METHOD_getClientProperty24 = 24;
    private static final int METHOD_getComponentAt25 = 25;
    private static final int METHOD_getConditionForKeyStroke26 = 26;
    private static final int METHOD_getDefaultLocale27 = 27;
    private static final int METHOD_getFontMetrics28 = 28;
    private static final int METHOD_getInsets29 = 29;
    private static final int METHOD_getListeners30 = 30;
    private static final int METHOD_getLocation31 = 31;
    private static final int METHOD_getPropertyChangeListeners32 = 32;
    private static final int METHOD_getSize33 = 33;
    private static final int METHOD_getToolTipLocation34 = 34;
    private static final int METHOD_getToolTipText35 = 35;
    private static final int METHOD_gotFocus36 = 36;
    private static final int METHOD_grabFocus37 = 37;
    private static final int METHOD_handleEvent38 = 38;
    private static final int METHOD_hasFocus39 = 39;
    private static final int METHOD_hide40 = 40;
    private static final int METHOD_imageUpdate41 = 41;
    private static final int METHOD_insets42 = 42;
    private static final int METHOD_inside43 = 43;
    private static final int METHOD_invalidate44 = 44;
    private static final int METHOD_isAncestorOf45 = 45;
    private static final int METHOD_isFocusCycleRoot46 = 46;
    private static final int METHOD_isLightweightComponent47 = 47;
    private static final int METHOD_keyDown48 = 48;
    private static final int METHOD_keyUp49 = 49;
    private static final int METHOD_layout50 = 50;
    private static final int METHOD_list51 = 51;
    private static final int METHOD_locate52 = 52;
    private static final int METHOD_location53 = 53;
    private static final int METHOD_lostFocus54 = 54;
    private static final int METHOD_minimumSize55 = 55;
    private static final int METHOD_mouseDown56 = 56;
    private static final int METHOD_mouseDrag57 = 57;
    private static final int METHOD_mouseEnter58 = 58;
    private static final int METHOD_mouseExit59 = 59;
    private static final int METHOD_mouseMove60 = 60;
    private static final int METHOD_mouseUp61 = 61;
    private static final int METHOD_move62 = 62;
    private static final int METHOD_nextFocus63 = 63;
    private static final int METHOD_paint64 = 64;
    private static final int METHOD_paintAll65 = 65;
    private static final int METHOD_paintComponents66 = 66;
    private static final int METHOD_paintImmediately67 = 67;
    private static final int METHOD_postEvent68 = 68;
    private static final int METHOD_preferredSize69 = 69;
    private static final int METHOD_prepareImage70 = 70;
    private static final int METHOD_print71 = 71;
    private static final int METHOD_printAll72 = 72;
    private static final int METHOD_printComponents73 = 73;
    private static final int METHOD_putClientProperty74 = 74;
    private static final int METHOD_registerKeyboardAction75 = 75;
    private static final int METHOD_remove76 = 76;
    private static final int METHOD_removeAll77 = 77;
    private static final int METHOD_removeNotify78 = 78;
    private static final int METHOD_removePropertyChangeListener79 = 79;
    private static final int METHOD_repaint80 = 80;
    private static final int METHOD_requestDefaultFocus81 = 81;
    private static final int METHOD_requestFocus82 = 82;
    private static final int METHOD_requestFocusInWindow83 = 83;
    private static final int METHOD_resetKeyboardActions84 = 84;
    private static final int METHOD_reshape85 = 85;
    private static final int METHOD_resize86 = 86;
    private static final int METHOD_revalidate87 = 87;
    private static final int METHOD_scrollRectToVisible88 = 88;
    private static final int METHOD_setBounds89 = 89;
    private static final int METHOD_setDefaultLocale90 = 90;
    private static final int METHOD_show91 = 91;
    private static final int METHOD_size92 = 92;
    private static final int METHOD_toString93 = 93;
    private static final int METHOD_transferFocus94 = 94;
    private static final int METHOD_transferFocusBackward95 = 95;
    private static final int METHOD_transferFocusDownCycle96 = 96;
    private static final int METHOD_transferFocusUpCycle97 = 97;
    private static final int METHOD_unregisterKeyboardAction98 = 98;
    private static final int METHOD_update99 = 99;
    private static final int METHOD_updateUI100 = 100;
    private static final int METHOD_validate101 = 101;

    // Methodenfolgen 
    /*lazy MethodDescriptor*/
    private static MethodDescriptor[] getMdescriptor(){
        MethodDescriptor[] methods = new MethodDescriptor[102];
    
        try {
            methods[METHOD_action0] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("action", new Class[] {java.awt.Event.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_action0].setDisplayName ( "" );
            methods[METHOD_add1] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("add", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_add1].setDisplayName ( "" );
            methods[METHOD_addNotify2] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("addNotify", new Class[] {})); // NOI18N
            methods[METHOD_addNotify2].setDisplayName ( "" );
            methods[METHOD_addPropertyChangeListener3] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("addPropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class})); // NOI18N
            methods[METHOD_addPropertyChangeListener3].setDisplayName ( "" );
            methods[METHOD_applyComponentOrientation4] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("applyComponentOrientation", new Class[] {java.awt.ComponentOrientation.class})); // NOI18N
            methods[METHOD_applyComponentOrientation4].setDisplayName ( "" );
            methods[METHOD_areFocusTraversalKeysSet5] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("areFocusTraversalKeysSet", new Class[] {Integer.TYPE})); // NOI18N
            methods[METHOD_areFocusTraversalKeysSet5].setDisplayName ( "" );
            methods[METHOD_bounds6] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("bounds", new Class[] {})); // NOI18N
            methods[METHOD_bounds6].setDisplayName ( "" );
            methods[METHOD_checkImage7] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("checkImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class})); // NOI18N
            methods[METHOD_checkImage7].setDisplayName ( "" );
            methods[METHOD_computeVisibleRect8] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("computeVisibleRect", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_computeVisibleRect8].setDisplayName ( "" );
            methods[METHOD_contains9] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("contains", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_contains9].setDisplayName ( "" );
            methods[METHOD_countComponents10] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("countComponents", new Class[] {})); // NOI18N
            methods[METHOD_countComponents10].setDisplayName ( "" );
            methods[METHOD_createImage11] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("createImage", new Class[] {java.awt.image.ImageProducer.class})); // NOI18N
            methods[METHOD_createImage11].setDisplayName ( "" );
            methods[METHOD_createToolTip12] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("createToolTip", new Class[] {})); // NOI18N
            methods[METHOD_createToolTip12].setDisplayName ( "" );
            methods[METHOD_createVolatileImage13] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("createVolatileImage", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_createVolatileImage13].setDisplayName ( "" );
            methods[METHOD_deliverEvent14] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("deliverEvent", new Class[] {java.awt.Event.class})); // NOI18N
            methods[METHOD_deliverEvent14].setDisplayName ( "" );
            methods[METHOD_disable15] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("disable", new Class[] {})); // NOI18N
            methods[METHOD_disable15].setDisplayName ( "" );
            methods[METHOD_dispatchEvent16] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("dispatchEvent", new Class[] {java.awt.AWTEvent.class})); // NOI18N
            methods[METHOD_dispatchEvent16].setDisplayName ( "" );
            methods[METHOD_doLayout17] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("doLayout", new Class[] {})); // NOI18N
            methods[METHOD_doLayout17].setDisplayName ( "" );
            methods[METHOD_enable18] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("enable", new Class[] {})); // NOI18N
            methods[METHOD_enable18].setDisplayName ( "" );
            methods[METHOD_enableInputMethods19] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("enableInputMethods", new Class[] {Boolean.TYPE})); // NOI18N
            methods[METHOD_enableInputMethods19].setDisplayName ( "" );
            methods[METHOD_findComponentAt20] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("findComponentAt", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_findComponentAt20].setDisplayName ( "" );
            methods[METHOD_firePropertyChange21] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("firePropertyChange", new Class[] {java.lang.String.class, Boolean.TYPE, Boolean.TYPE})); // NOI18N
            methods[METHOD_firePropertyChange21].setDisplayName ( "" );
            methods[METHOD_getActionForKeyStroke22] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("getActionForKeyStroke", new Class[] {javax.swing.KeyStroke.class})); // NOI18N
            methods[METHOD_getActionForKeyStroke22].setDisplayName ( "" );
            methods[METHOD_getBounds23] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("getBounds", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_getBounds23].setDisplayName ( "" );
            methods[METHOD_getClientProperty24] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("getClientProperty", new Class[] {java.lang.Object.class})); // NOI18N
            methods[METHOD_getClientProperty24].setDisplayName ( "" );
            methods[METHOD_getComponentAt25] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("getComponentAt", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_getComponentAt25].setDisplayName ( "" );
            methods[METHOD_getConditionForKeyStroke26] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("getConditionForKeyStroke", new Class[] {javax.swing.KeyStroke.class})); // NOI18N
            methods[METHOD_getConditionForKeyStroke26].setDisplayName ( "" );
            methods[METHOD_getDefaultLocale27] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("getDefaultLocale", new Class[] {})); // NOI18N
            methods[METHOD_getDefaultLocale27].setDisplayName ( "" );
            methods[METHOD_getFontMetrics28] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("getFontMetrics", new Class[] {java.awt.Font.class})); // NOI18N
            methods[METHOD_getFontMetrics28].setDisplayName ( "" );
            methods[METHOD_getInsets29] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("getInsets", new Class[] {java.awt.Insets.class})); // NOI18N
            methods[METHOD_getInsets29].setDisplayName ( "" );
            methods[METHOD_getListeners30] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("getListeners", new Class[] {java.lang.Class.class})); // NOI18N
            methods[METHOD_getListeners30].setDisplayName ( "" );
            methods[METHOD_getLocation31] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("getLocation", new Class[] {java.awt.Point.class})); // NOI18N
            methods[METHOD_getLocation31].setDisplayName ( "" );
            methods[METHOD_getPropertyChangeListeners32] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("getPropertyChangeListeners", new Class[] {java.lang.String.class})); // NOI18N
            methods[METHOD_getPropertyChangeListeners32].setDisplayName ( "" );
            methods[METHOD_getSize33] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("getSize", new Class[] {java.awt.Dimension.class})); // NOI18N
            methods[METHOD_getSize33].setDisplayName ( "" );
            methods[METHOD_getToolTipLocation34] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("getToolTipLocation", new Class[] {java.awt.event.MouseEvent.class})); // NOI18N
            methods[METHOD_getToolTipLocation34].setDisplayName ( "" );
            methods[METHOD_getToolTipText35] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("getToolTipText", new Class[] {java.awt.event.MouseEvent.class})); // NOI18N
            methods[METHOD_getToolTipText35].setDisplayName ( "" );
            methods[METHOD_gotFocus36] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("gotFocus", new Class[] {java.awt.Event.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_gotFocus36].setDisplayName ( "" );
            methods[METHOD_grabFocus37] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("grabFocus", new Class[] {})); // NOI18N
            methods[METHOD_grabFocus37].setDisplayName ( "" );
            methods[METHOD_handleEvent38] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("handleEvent", new Class[] {java.awt.Event.class})); // NOI18N
            methods[METHOD_handleEvent38].setDisplayName ( "" );
            methods[METHOD_hasFocus39] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("hasFocus", new Class[] {})); // NOI18N
            methods[METHOD_hasFocus39].setDisplayName ( "" );
            methods[METHOD_hide40] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("hide", new Class[] {})); // NOI18N
            methods[METHOD_hide40].setDisplayName ( "" );
            methods[METHOD_imageUpdate41] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("imageUpdate", new Class[] {java.awt.Image.class, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_imageUpdate41].setDisplayName ( "" );
            methods[METHOD_insets42] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("insets", new Class[] {})); // NOI18N
            methods[METHOD_insets42].setDisplayName ( "" );
            methods[METHOD_inside43] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("inside", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_inside43].setDisplayName ( "" );
            methods[METHOD_invalidate44] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("invalidate", new Class[] {})); // NOI18N
            methods[METHOD_invalidate44].setDisplayName ( "" );
            methods[METHOD_isAncestorOf45] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("isAncestorOf", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_isAncestorOf45].setDisplayName ( "" );
            methods[METHOD_isFocusCycleRoot46] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("isFocusCycleRoot", new Class[] {java.awt.Container.class})); // NOI18N
            methods[METHOD_isFocusCycleRoot46].setDisplayName ( "" );
            methods[METHOD_isLightweightComponent47] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("isLightweightComponent", new Class[] {java.awt.Component.class})); // NOI18N
            methods[METHOD_isLightweightComponent47].setDisplayName ( "" );
            methods[METHOD_keyDown48] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("keyDown", new Class[] {java.awt.Event.class, Integer.TYPE})); // NOI18N
            methods[METHOD_keyDown48].setDisplayName ( "" );
            methods[METHOD_keyUp49] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("keyUp", new Class[] {java.awt.Event.class, Integer.TYPE})); // NOI18N
            methods[METHOD_keyUp49].setDisplayName ( "" );
            methods[METHOD_layout50] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("layout", new Class[] {})); // NOI18N
            methods[METHOD_layout50].setDisplayName ( "" );
            methods[METHOD_list51] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("list", new Class[] {java.io.PrintStream.class, Integer.TYPE})); // NOI18N
            methods[METHOD_list51].setDisplayName ( "" );
            methods[METHOD_locate52] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("locate", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_locate52].setDisplayName ( "" );
            methods[METHOD_location53] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("location", new Class[] {})); // NOI18N
            methods[METHOD_location53].setDisplayName ( "" );
            methods[METHOD_lostFocus54] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("lostFocus", new Class[] {java.awt.Event.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_lostFocus54].setDisplayName ( "" );
            methods[METHOD_minimumSize55] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("minimumSize", new Class[] {})); // NOI18N
            methods[METHOD_minimumSize55].setDisplayName ( "" );
            methods[METHOD_mouseDown56] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("mouseDown", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseDown56].setDisplayName ( "" );
            methods[METHOD_mouseDrag57] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("mouseDrag", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseDrag57].setDisplayName ( "" );
            methods[METHOD_mouseEnter58] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("mouseEnter", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseEnter58].setDisplayName ( "" );
            methods[METHOD_mouseExit59] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("mouseExit", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseExit59].setDisplayName ( "" );
            methods[METHOD_mouseMove60] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("mouseMove", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseMove60].setDisplayName ( "" );
            methods[METHOD_mouseUp61] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("mouseUp", new Class[] {java.awt.Event.class, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_mouseUp61].setDisplayName ( "" );
            methods[METHOD_move62] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("move", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_move62].setDisplayName ( "" );
            methods[METHOD_nextFocus63] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("nextFocus", new Class[] {})); // NOI18N
            methods[METHOD_nextFocus63].setDisplayName ( "" );
            methods[METHOD_paint64] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("paint", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_paint64].setDisplayName ( "" );
            methods[METHOD_paintAll65] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("paintAll", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_paintAll65].setDisplayName ( "" );
            methods[METHOD_paintComponents66] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("paintComponents", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_paintComponents66].setDisplayName ( "" );
            methods[METHOD_paintImmediately67] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("paintImmediately", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_paintImmediately67].setDisplayName ( "" );
            methods[METHOD_postEvent68] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("postEvent", new Class[] {java.awt.Event.class})); // NOI18N
            methods[METHOD_postEvent68].setDisplayName ( "" );
            methods[METHOD_preferredSize69] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("preferredSize", new Class[] {})); // NOI18N
            methods[METHOD_preferredSize69].setDisplayName ( "" );
            methods[METHOD_prepareImage70] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("prepareImage", new Class[] {java.awt.Image.class, java.awt.image.ImageObserver.class})); // NOI18N
            methods[METHOD_prepareImage70].setDisplayName ( "" );
            methods[METHOD_print71] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("print", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_print71].setDisplayName ( "" );
            methods[METHOD_printAll72] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("printAll", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_printAll72].setDisplayName ( "" );
            methods[METHOD_printComponents73] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("printComponents", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_printComponents73].setDisplayName ( "" );
            methods[METHOD_putClientProperty74] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("putClientProperty", new Class[] {java.lang.Object.class, java.lang.Object.class})); // NOI18N
            methods[METHOD_putClientProperty74].setDisplayName ( "" );
            methods[METHOD_registerKeyboardAction75] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("registerKeyboardAction", new Class[] {java.awt.event.ActionListener.class, java.lang.String.class, javax.swing.KeyStroke.class, Integer.TYPE})); // NOI18N
            methods[METHOD_registerKeyboardAction75].setDisplayName ( "" );
            methods[METHOD_remove76] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("remove", new Class[] {Integer.TYPE})); // NOI18N
            methods[METHOD_remove76].setDisplayName ( "" );
            methods[METHOD_removeAll77] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("removeAll", new Class[] {})); // NOI18N
            methods[METHOD_removeAll77].setDisplayName ( "" );
            methods[METHOD_removeNotify78] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("removeNotify", new Class[] {})); // NOI18N
            methods[METHOD_removeNotify78].setDisplayName ( "" );
            methods[METHOD_removePropertyChangeListener79] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("removePropertyChangeListener", new Class[] {java.lang.String.class, java.beans.PropertyChangeListener.class})); // NOI18N
            methods[METHOD_removePropertyChangeListener79].setDisplayName ( "" );
            methods[METHOD_repaint80] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("repaint", new Class[] {Long.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_repaint80].setDisplayName ( "" );
            methods[METHOD_requestDefaultFocus81] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("requestDefaultFocus", new Class[] {})); // NOI18N
            methods[METHOD_requestDefaultFocus81].setDisplayName ( "" );
            methods[METHOD_requestFocus82] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("requestFocus", new Class[] {})); // NOI18N
            methods[METHOD_requestFocus82].setDisplayName ( "" );
            methods[METHOD_requestFocusInWindow83] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("requestFocusInWindow", new Class[] {})); // NOI18N
            methods[METHOD_requestFocusInWindow83].setDisplayName ( "" );
            methods[METHOD_resetKeyboardActions84] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("resetKeyboardActions", new Class[] {})); // NOI18N
            methods[METHOD_resetKeyboardActions84].setDisplayName ( "" );
            methods[METHOD_reshape85] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("reshape", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_reshape85].setDisplayName ( "" );
            methods[METHOD_resize86] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("resize", new Class[] {Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_resize86].setDisplayName ( "" );
            methods[METHOD_revalidate87] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("revalidate", new Class[] {})); // NOI18N
            methods[METHOD_revalidate87].setDisplayName ( "" );
            methods[METHOD_scrollRectToVisible88] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("scrollRectToVisible", new Class[] {java.awt.Rectangle.class})); // NOI18N
            methods[METHOD_scrollRectToVisible88].setDisplayName ( "" );
            methods[METHOD_setBounds89] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("setBounds", new Class[] {Integer.TYPE, Integer.TYPE, Integer.TYPE, Integer.TYPE})); // NOI18N
            methods[METHOD_setBounds89].setDisplayName ( "" );
            methods[METHOD_setDefaultLocale90] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("setDefaultLocale", new Class[] {java.util.Locale.class})); // NOI18N
            methods[METHOD_setDefaultLocale90].setDisplayName ( "" );
            methods[METHOD_show91] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("show", new Class[] {})); // NOI18N
            methods[METHOD_show91].setDisplayName ( "" );
            methods[METHOD_size92] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("size", new Class[] {})); // NOI18N
            methods[METHOD_size92].setDisplayName ( "" );
            methods[METHOD_toString93] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("toString", new Class[] {})); // NOI18N
            methods[METHOD_toString93].setDisplayName ( "" );
            methods[METHOD_transferFocus94] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("transferFocus", new Class[] {})); // NOI18N
            methods[METHOD_transferFocus94].setDisplayName ( "" );
            methods[METHOD_transferFocusBackward95] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("transferFocusBackward", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusBackward95].setDisplayName ( "" );
            methods[METHOD_transferFocusDownCycle96] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("transferFocusDownCycle", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusDownCycle96].setDisplayName ( "" );
            methods[METHOD_transferFocusUpCycle97] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("transferFocusUpCycle", new Class[] {})); // NOI18N
            methods[METHOD_transferFocusUpCycle97].setDisplayName ( "" );
            methods[METHOD_unregisterKeyboardAction98] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("unregisterKeyboardAction", new Class[] {javax.swing.KeyStroke.class})); // NOI18N
            methods[METHOD_unregisterKeyboardAction98].setDisplayName ( "" );
            methods[METHOD_update99] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("update", new Class[] {java.awt.Graphics.class})); // NOI18N
            methods[METHOD_update99].setDisplayName ( "" );
            methods[METHOD_updateUI100] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("updateUI", new Class[] {})); // NOI18N
            methods[METHOD_updateUI100].setDisplayName ( "" );
            methods[METHOD_validate101] = new MethodDescriptor ( pauker.program.gui.swing.StatisticPanel.class.getMethod("validate", new Class[] {})); // NOI18N
            methods[METHOD_validate101].setDisplayName ( "" );
        }
        catch( Exception e) {}//GEN-HEADEREND:Methods
        
        // Here you can add code for customizing the methods array.
        
        return methods;     }//GEN-LAST:Methods
    
    
    private static final int defaultPropertyIndex = -1;//GEN-BEGIN:Idx
    private static final int defaultEventIndex = -1;//GEN-END:Idx
    
    
//GEN-FIRST:Superclass
    
    // Here you can add code for customizing the Superclass BeanInfo.
    
//GEN-LAST:Superclass
    
    /**
     * Gets the bean's <code>BeanDescriptor</code>s.
     *
     * @return BeanDescriptor describing the editable
     * properties of this bean.  May return null if the
     * information should be obtained by automatic analysis.
     */
    public BeanDescriptor getBeanDescriptor() {
        return getBdescriptor();
    }
    
    /**
     * Gets the bean's <code>PropertyDescriptor</code>s.
     *
     * @return An array of PropertyDescriptors describing the editable
     * properties supported by this bean.  May return null if the
     * information should be obtained by automatic analysis.
     * <p>
     * If a property is indexed, then its entry in the result array will
     * belong to the IndexedPropertyDescriptor subclass of PropertyDescriptor.
     * A client of getPropertyDescriptors can use "instanceof" to check
     * if a given PropertyDescriptor is an IndexedPropertyDescriptor.
     */
    public PropertyDescriptor[] getPropertyDescriptors() {
        return getPdescriptor();
    }
    
    /**
     * Gets the bean's <code>EventSetDescriptor</code>s.
     *
     * @return  An array of EventSetDescriptors describing the kinds of
     * events fired by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public EventSetDescriptor[] getEventSetDescriptors() {
        return getEdescriptor();
    }
    
    /**
     * Gets the bean's <code>MethodDescriptor</code>s.
     *
     * @return  An array of MethodDescriptors describing the methods
     * implemented by this bean.  May return null if the information
     * should be obtained by automatic analysis.
     */
    public MethodDescriptor[] getMethodDescriptors() {
        return getMdescriptor();
    }
    
    /**
     * A bean may have a "default" property that is the property that will
     * mostly commonly be initially chosen for update by human's who are
     * customizing the bean.
     * @return  Index of default property in the PropertyDescriptor array
     * 		returned by getPropertyDescriptors.
     * <P>	Returns -1 if there is no default property.
     */
    public int getDefaultPropertyIndex() {
        return defaultPropertyIndex;
    }
    
    /**
     * A bean may have a "default" event that is the event that will
     * mostly commonly be used by human's when using the bean.
     * @return Index of default event in the EventSetDescriptor array
     *		returned by getEventSetDescriptors.
     * <P>	Returns -1 if there is no default event.
     */
    public int getDefaultEventIndex() {
        return defaultEventIndex;
    }
}

