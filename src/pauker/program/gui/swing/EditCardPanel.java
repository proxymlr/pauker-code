/*
 * EditCardPanel.java
 *
 * Created on 10. April 2002, 07:36
 */
package pauker.program.gui.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ActionMap;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.ListCellRenderer;
import javax.swing.event.CaretEvent;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.DefaultEditorKit;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import pauker.program.Card;

/**
 *
 * @author Ronny.Standtke@gmx.net
 */
public class EditCardPanel
        extends JPanel
        implements UndoableEditListener, DocumentListener {

    private final static Logger logger =
            Logger.getLogger(EditCardPanel.class.getName());
    private final MyUndoManager undoManager;
    private final Document frontSideDocument;
    private final Document reverseSideDocument;
    private final Map<Object, Action> editActions;
    private final Action cutAction;
    private final Action copyAction;
    private final Action pasteAction;
    private final Action insertTabAction;
    private static Card.Element lastFocusSide;
    private Point lastMousePoint;
    private int scrollIncrement;
    private Component nextFocusComponent;
    private PaukerFrame paukerFrame;
    private final javax.swing.Timer scrollTimer = new javax.swing.Timer(
            50, new ActionListener() {

        public void actionPerformed(ActionEvent evt) {

            // scroll
            JScrollBar scrollbar = popupScrollPane.getVerticalScrollBar();
            int position = scrollbar.getValue();
            scrollbar.setValue(position + scrollIncrement);

            // update selection & counter
            lastMousePoint.y += scrollIncrement;
            updatePopup();
        }
    });

    private static class MyListCellRenderer implements ListCellRenderer {

        public Component getListCellRendererComponent(JList list, Object value,
                int index, boolean isSelected, boolean cellHasFocus) {
            JLabel label = (JLabel) value;
            if (isSelected) {
                label.setBackground(list.getSelectionBackground());
                label.setForeground(list.getSelectionForeground());
            } else {
                label.setBackground(list.getBackground());
                label.setForeground(list.getForeground());
            }
            return label;
        }
    }

    /** Creates new form BeanForm */
    public EditCardPanel() {

        initComponents();

        // configure all accelerators
        int menuShortcutKeyMask =
                Toolkit.getDefaultToolkit().getMenuShortcutKeyMask();
        undoMenuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_Z, menuShortcutKeyMask));
        redoMenuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_Y, menuShortcutKeyMask));
        cutMenuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_X, menuShortcutKeyMask));
        copyMenuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_C, menuShortcutKeyMask));
        pasteMenuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_V, menuShortcutKeyMask));
        selectAllMenuItem.setAccelerator(KeyStroke.getKeyStroke(
                KeyEvent.VK_A, menuShortcutKeyMask));


        undoManager = new MyUndoManager();
        frontSideDocument = frontSideTextArea.getDocument();
        reverseSideDocument = reverseSideTextArea.getDocument();
        frontSideDocument.addDocumentListener(this);
        reverseSideDocument.addDocumentListener(this);
        frontSideDocument.addUndoableEditListener(this);
        reverseSideDocument.addUndoableEditListener(this);

        editActions = new HashMap<Object, Action>();
        Action[] actions = frontSideTextArea.getActions();
        for (Action action : actions) {
            editActions.put(action.getValue(Action.NAME), action);
        }

        cutAction = editActions.get(DefaultEditorKit.cutAction);
        copyAction = editActions.get(DefaultEditorKit.copyAction);
        pasteAction = editActions.get(DefaultEditorKit.pasteAction);
        insertTabAction = editActions.get(DefaultEditorKit.insertTabAction);


        // change the default TAB-handling to something more comfortable

        Action focusButtonPanelAction = new AbstractAction() {

            public void actionPerformed(ActionEvent event) {
                tabButton.requestFocusInWindow();
            }
        };

        Action focusFrontSideAction = new AbstractAction() {

            public void actionPerformed(ActionEvent event) {
                setFocus(Card.Element.FRONT_SIDE);
            }
        };

        Action focusReverseSideAction = new AbstractAction() {

            public void actionPerformed(ActionEvent event) {
                setFocus(Card.Element.REVERSE_SIDE);
            }
        };

        Action transferFocusAction = new AbstractAction() {

            public void actionPerformed(ActionEvent event) {
                if (nextFocusComponent == null) {
                    repeatingMethodComboBox.requestFocusInWindow();
                } else {
                    nextFocusComponent.requestFocusInWindow();
                }
            }
        };

        KeyStroke tabKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0);
        KeyStroke shiftTabKeyStroke =
                KeyStroke.getKeyStroke(KeyEvent.VK_TAB, InputEvent.SHIFT_MASK);

        InputMap frontSideInputMap = frontSideTextArea.getInputMap();
        frontSideInputMap.put(tabKeyStroke, "tabAction");
        frontSideInputMap.put(shiftTabKeyStroke, "shiftTabAction");
        ActionMap frontSideActionMap = frontSideTextArea.getActionMap();
        frontSideActionMap.put("tabAction", focusReverseSideAction);
        frontSideActionMap.put("shiftTabAction", focusButtonPanelAction);

        InputMap reverseSideInputMap = reverseSideTextArea.getInputMap();
        reverseSideInputMap.put(tabKeyStroke, "tabAction");
        reverseSideInputMap.put(shiftTabKeyStroke, "shiftTabAction");
        ActionMap reverseSideActionMap = reverseSideTextArea.getActionMap();
        reverseSideActionMap.put("tabAction", transferFocusAction);
        reverseSideActionMap.put("shiftTabAction", focusFrontSideAction);

        // fill font combo box
        DefaultComboBoxModel fontComboModel = new DefaultComboBoxModel();
        GraphicsEnvironment graphicsEnvironment =
                GraphicsEnvironment.getLocalGraphicsEnvironment();
        String[] fontFamilyNames =
                graphicsEnvironment.getAvailableFontFamilyNames();
        for (String fontFamilyName : fontFamilyNames) {
            fontComboModel.addElement(fontFamilyName);
        }
        fontComboBox.setModel(fontComboModel);

        // fill font sizes
        DefaultComboBoxModel fontSizeComboModel = new DefaultComboBoxModel();
        for (int i = 6; i < 41; i++) {
            fontSizeComboModel.addElement(Integer.valueOf(i));
        }
        fontSizeComboBox.setModel(fontSizeComboModel);

        // set font values in gui
        Font defaultFont = frontSideTextArea.getFont();
        fontComboBox.setSelectedItem(defaultFont.getFamily());
        fontSizeComboBox.setSelectedItem(
                Integer.valueOf(defaultFont.getSize()));
        boldToggleButton.setSelected(defaultFont.isBold());
        italicToggleButton.setSelected(defaultFont.isItalic());

        repeatingMethodComboBox.addItem(typingLabel);
        repeatingMethodComboBox.addItem(memorizingLabel);

        // set locale specific text orientation (left-to-right or right-to-left)
        ComponentOrientation defaultOrientation =
                ComponentOrientation.getOrientation(Locale.getDefault());
        frontSideTextArea.setComponentOrientation(defaultOrientation);
        reverseSideTextArea.setComponentOrientation(defaultOrientation);
        if (defaultOrientation.isLeftToRight()) {
            leftToRightToggleButton.setSelected(true);
        } else {
            rightToLeftToggleButton.setSelected(true);
        }

        // fix the preferred size
        setMinimumSize(getPreferredSize());
    }

    /**
     * applies the current line wrap settings to the front and reverse side text
     * areas
     */
    public void applyWrapSettings() {
        if (paukerFrame != null) {
            frontSideTextArea.setLineWrap(paukerFrame.isLineWrapping());
            reverseSideTextArea.setLineWrap(paukerFrame.isLineWrapping());
        }
    }

    /**
     * sets the PaukerFrame
     * @param paukerFrame the PaukerFrame
     */
    public void setPaukerFrame(PaukerFrame paukerFrame) {
        this.paukerFrame = paukerFrame;
    }

    /**
     * returns the component orientation of the front side
     * @return the component orientation of the front side
     */
    public ComponentOrientation getFrontSideComponentOrientation() {
        return frontSideTextArea.getComponentOrientation();
    }

    /**
     * returns the component orientation of the reverse side
     * @return the component orientation of the reverse side
     */
    public ComponentOrientation getReverseSideComponentOrientation() {
        return reverseSideTextArea.getComponentOrientation();
    }

    /**
     * returns the orientation of the SplitPane
     * @return the orientation of the SplitPane
     */
    public int getOrientation() {
        return splitPane.getOrientation();
    }

    /**
     * sets the focus (and thereby the caret) to a card side
     * @param cardSide the card side to focus
     */
    public void setFocus(Card.Element cardSide) {
        if (cardSide == Card.Element.FRONT_SIDE) {
            frontSideTextArea.requestFocusInWindow();
        } else {
            reverseSideTextArea.requestFocusInWindow();
        }
    }

    /**
     * returns the last focussed card side
     * @return the last focussed card side
     */
    public static Card.Element getLastFocusSide() {
        return lastFocusSide;
    }

    /**
     * removes the text on both card sides
     */
    public void clear() {
        frontSideTextArea.setText("");
        reverseSideTextArea.setText("");
    }

    /**
     * resets the panel so that a new card can be inserted
     */
    public void reset() {
        clear();
        frontSideTextArea.requestFocusInWindow();

        undoManager.discardAllEdits();
        undoButton.setEnabled(false);
        undoListButton.setEnabled(false);
        redoButton.setEnabled(false);
        redoListButton.setEnabled(false);

        applyWrapSettings();
    }

    /**
     * sets the next component to focus
     * @param nextFocusComponent the next component to focus
     */
    public void setNextFocusComponent(Component nextFocusComponent) {
        this.nextFocusComponent = nextFocusComponent;
    }

    /**
     * returns the font of the front side
     * @return the font of the front side
     */
    public Font getFrontSideFont() {
        return frontSideTextArea.getFont();
    }

    /**
     * returns the font of the reverse side
     * @return the font of the reverse side
     */
    public Font getReverseSideFont() {
        return reverseSideTextArea.getFont();
    }

    /**
     * returns the foreground color of the front side
     * @return the foreground color of the front side
     */
    public Color getFrontForegroundColor() {
        return frontSideTextArea.getForeground();
    }

    /**
     * returns the background color of the front side
     * @return the background color of the front side
     */
    public Color getFrontBackgroundColor() {
        return frontSideTextArea.getBackground();
    }

    /**
     * returns the foreground color of the reverse side
     * @return the foreground color of the reverse side
     */
    public Color getReverseSideForegroundColor() {
        return reverseSideTextArea.getForeground();
    }

    /**
     * returns the background color of the reverse side
     * @return the background color of the reverse side
     */
    public Color getReverseSideBackgroundColor() {
        return reverseSideTextArea.getBackground();
    }

    public void undoableEditHappened(UndoableEditEvent undoableEditEvent) {
        undoManager.addEdit(undoableEditEvent.getEdit());
        updateUndoRedoButtons();
    }

    private void updateUndoRedoButtons() {
        boolean canUndo = undoManager.canUndo();
        undoButton.setEnabled(canUndo);
        undoListButton.setEnabled(canUndo);
        undoMenuItem.setEnabled(canUndo);

        boolean canRedo = undoManager.canRedo();
        redoButton.setEnabled(canRedo);
        redoListButton.setEnabled(canRedo);
        redoMenuItem.setEnabled(canRedo);
    }

    /** Getter for property frontSide.
     * @return Value of property frontSide.
     */
    public String getFrontSide() {
        return frontSideTextArea.getText();
    }

    /** Getter for frontSideTextArea
     * @return frontSideTextArea
     */
    public JTextComponent getFrontSideTextComponent() {
        return frontSideTextArea;
    }

    /**
     * sets the card to edit
     * @param card the card to edit
     */
    public void setCard(Card card) {
        PaukerFrame.fillTextComponentWithCardSide(card.getFrontSide(),
                frontSideTextArea, true);
        PaukerFrame.fillTextComponentWithCardSide(card.getReverseSide(),
                reverseSideTextArea, true);

        // remove initial undo
        undoManager.discardAllEdits();
        undoButton.setEnabled(false);
        undoListButton.setEnabled(false);
    }

    /**
     * Getter for property reverseSide.
     * @return Value of property reverseSide.
     */
    public String getReverseSide() {
        return reverseSideTextArea.getText();
    }

    /** Getter for reverseSideTextArea
     * @return reverseSideTextArea
     */
    public JTextComponent getReverseSideTextComponent() {
        return reverseSideTextArea;
    }

    /** Setter for property frontSideFont.
     * @param frontSideFont New value of property frontSideFont.
     */
    public void setFrontSideFont(Font frontSideFont) {
        frontSideTextArea.setFont(frontSideFont);
    }

    /**
     * Setter for property reverseSideFont.
     * @param reverseSideFont New value of property reverseSideFont.
     */
    public void setReverseSideFont(Font reverseSideFont) {
        reverseSideTextArea.setFont(reverseSideFont);
    }

    /**
     * returns true, if the current card must be repeat by typing,
     * false otherwise
     * @return true, if the current card must be repeat by typing,
     * false otherwise
     */
    public boolean isRepeatByTyping() {
        return repeatingMethodComboBox.getSelectedItem().equals(typingLabel);
    }

    /**
     * determines if the current card must be repeat by typing
     * @param repeatByTyping if true, the current card must be repeat by typing,
     * if false, the current card must be repeat by remembering
     */
    public void setRepeatByTyping(boolean repeatByTyping) {
        if (repeatByTyping) {
            repeatingMethodComboBox.setSelectedItem(typingLabel);
        } else {
            repeatingMethodComboBox.setSelectedItem(memorizingLabel);
        }
    }

    public void changedUpdate(DocumentEvent documentEvent) {
        documentChanged(documentEvent);
    }

    public void insertUpdate(DocumentEvent documentEvent) {
        documentChanged(documentEvent);
    }

    public void removeUpdate(DocumentEvent documentEvent) {
        documentChanged(documentEvent);
    }

    /**
     * sets the split orientation between the card sides
     * @param orientation the split orientation between the card sides
     */
    public void setSplitOrientation(int orientation) {
        // change global setting in case of "internal" call
        if (paukerFrame != null) {
            paukerFrame.setSplitOrientation(orientation);
        }

        // change orientation
        splitPane.setOrientation(orientation);

        // update sizes
        int lines = frontSideTextArea.getLineCount();
        frontSideTextArea.setRows(lines < 5 ? 5 : lines);
        lines = reverseSideTextArea.getLineCount();
        reverseSideTextArea.setRows(lines < 5 ? 5 : lines);
        splitPane.resetToPreferredSizes();

        // update orientation button
        ResourceBundle strings = ResourceBundle.getBundle("pauker/Strings");
        if (orientation == JSplitPane.HORIZONTAL_SPLIT) {
            switchLayoutButton.setIcon(new ImageIcon(getClass().getResource(
                    "/pauker/icons/view_top_bottom.png")));
            switchLayoutButton.setToolTipText(
                    strings.getString("Cardsides_Top_Down"));
        } else {
            switchLayoutButton.setIcon(new ImageIcon(getClass().getResource(
                    "/pauker/icons/view_left_right.png")));
            switchLayoutButton.setToolTipText(
                    strings.getString("Cardsides_Left_Right"));
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        popupPanel = new javax.swing.JPanel();
        popupScrollPane = new javax.swing.JScrollPane();
        popupList = new javax.swing.JList();
        undoCounterLabel = new javax.swing.JLabel();
        popupMenu = new javax.swing.JPopupMenu();
        typingLabel = new javax.swing.JLabel();
        memorizingLabel = new javax.swing.JLabel();
        alignmentButtonGroup = new javax.swing.ButtonGroup();
        textAreaPopupMenu = new javax.swing.JPopupMenu();
        undoMenuItem = new javax.swing.JMenuItem();
        redoMenuItem = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JSeparator();
        cutMenuItem = new javax.swing.JMenuItem();
        copyMenuItem = new javax.swing.JMenuItem();
        pasteMenuItem = new javax.swing.JMenuItem();
        clearMenuItem = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JSeparator();
        selectAllMenuItem = new javax.swing.JMenuItem();
        editButtonPanel = new javax.swing.JPanel();
        undoButtonPanel = new javax.swing.JPanel();
        undoButton = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        undoListButton = new javax.swing.JButton();
        redoButtonPanel = new javax.swing.JPanel();
        redoButton = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        redoListButton = new javax.swing.JButton();
        cutButton = new javax.swing.JButton();
        copyButton = new javax.swing.JButton();
        pasteButton = new javax.swing.JButton();
        leftToRightToggleButton = new javax.swing.JToggleButton();
        rightToLeftToggleButton = new javax.swing.JToggleButton();
        tabButton = new javax.swing.JButton();
        switchLayoutButton = new javax.swing.JButton();
        fontPanel = new javax.swing.JPanel();
        fontComboBox = new javax.swing.JComboBox();
        fontSizeComboBox = new javax.swing.JComboBox();
        boldToggleButton = new javax.swing.JToggleButton();
        italicToggleButton = new javax.swing.JToggleButton();
        foregroundButton = new javax.swing.JButton();
        backgroundButton = new javax.swing.JButton();
        splitPane = new javax.swing.JSplitPane();
        frontSidePanel = new javax.swing.JPanel();
        frontSideLabel = new javax.swing.JLabel();
        frontSideScrollPane = new javax.swing.JScrollPane();
        frontSideTextArea = new javax.swing.JTextArea();
        reverseSidePanel = new javax.swing.JPanel();
        reverseSideLabel = new javax.swing.JLabel();
        reverseSideScrollPane = new javax.swing.JScrollPane();
        reverseSideTextArea = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        repeatingMethodComboBox = new javax.swing.JComboBox();

        popupPanel.setLayout(new java.awt.GridBagLayout());

        popupList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                popupListMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                popupListMouseExited(evt);
            }
        });
        popupList.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                popupListMouseMoved(evt);
            }
        });
        popupScrollPane.setViewportView(popupList);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        popupPanel.add(popupScrollPane, gridBagConstraints);

        undoCounterLabel.setText("number of undos");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        popupPanel.add(undoCounterLabel, gridBagConstraints);

        popupMenu.add(popupPanel);

        typingLabel.setFont(new java.awt.Font("Dialog", 0, 10));
        typingLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/key_enter.png"))); // NOI18N
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("pauker/Strings"); // NOI18N
        typingLabel.setText(bundle.getString("Repeat_By_Typing")); // NOI18N
        typingLabel.setOpaque(true);

        memorizingLabel.setFont(new java.awt.Font("Dialog", 0, 10));
        memorizingLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/smallbrain.png"))); // NOI18N
        memorizingLabel.setText(bundle.getString("Repeat_By_Remembering")); // NOI18N
        memorizingLabel.setOpaque(true);

        undoMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/16x16/undo.png"))); // NOI18N
        undoMenuItem.setText(bundle.getString("Undo")); // NOI18N
        undoMenuItem.setEnabled(false);
        undoMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                undoMenuItemActionPerformed(evt);
            }
        });
        textAreaPopupMenu.add(undoMenuItem);

        redoMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/16x16/redo.png"))); // NOI18N
        redoMenuItem.setText(bundle.getString("Redo")); // NOI18N
        redoMenuItem.setEnabled(false);
        redoMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                redoMenuItemActionPerformed(evt);
            }
        });
        textAreaPopupMenu.add(redoMenuItem);
        textAreaPopupMenu.add(jSeparator3);

        cutMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/16x16/editcut.png"))); // NOI18N
        cutMenuItem.setText(bundle.getString("Cut")); // NOI18N
        cutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cutMenuItemActionPerformed(evt);
            }
        });
        textAreaPopupMenu.add(cutMenuItem);

        copyMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/16x16/editcopy.png"))); // NOI18N
        copyMenuItem.setText(bundle.getString("Copy")); // NOI18N
        copyMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copyMenuItemActionPerformed(evt);
            }
        });
        textAreaPopupMenu.add(copyMenuItem);

        pasteMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/16x16/editpaste.png"))); // NOI18N
        pasteMenuItem.setText(bundle.getString("Paste")); // NOI18N
        pasteMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pasteMenuItemActionPerformed(evt);
            }
        });
        textAreaPopupMenu.add(pasteMenuItem);

        clearMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/16x16/editclear.png"))); // NOI18N
        clearMenuItem.setText(bundle.getString("Clear")); // NOI18N
        clearMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearMenuItemActionPerformed(evt);
            }
        });
        textAreaPopupMenu.add(clearMenuItem);
        textAreaPopupMenu.add(jSeparator4);

        selectAllMenuItem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/16x16/text_block.png"))); // NOI18N
        selectAllMenuItem.setText(bundle.getString("Select_All")); // NOI18N
        selectAllMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selectAllMenuItemActionPerformed(evt);
            }
        });
        textAreaPopupMenu.add(selectAllMenuItem);

        setLayout(new java.awt.GridBagLayout());

        editButtonPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        editButtonPanel.setLayout(new java.awt.GridBagLayout());

        undoButtonPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        undoButtonPanel.setLayout(new java.awt.GridBagLayout());

        undoButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/undo.png"))); // NOI18N
        undoButton.setToolTipText(bundle.getString("Undo")); // NOI18N
        undoButton.setBorder(null);
        undoButton.setEnabled(false);
        undoButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        undoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                undoButtonActionPerformed(evt);
            }
        });
        undoButtonPanel.add(undoButton, new java.awt.GridBagConstraints());

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        undoButtonPanel.add(jSeparator1, gridBagConstraints);

        undoListButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/down_small.png"))); // NOI18N
        undoListButton.setBorder(null);
        undoListButton.setEnabled(false);
        undoListButton.setFocusPainted(false);
        undoListButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        undoListButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                undoListButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        undoButtonPanel.add(undoListButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 0);
        editButtonPanel.add(undoButtonPanel, gridBagConstraints);

        redoButtonPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        redoButtonPanel.setLayout(new java.awt.GridBagLayout());

        redoButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/redo.png"))); // NOI18N
        redoButton.setToolTipText(bundle.getString("Redo")); // NOI18N
        redoButton.setBorder(null);
        redoButton.setEnabled(false);
        redoButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        redoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                redoButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        redoButtonPanel.add(redoButton, gridBagConstraints);

        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        redoButtonPanel.add(jSeparator2, gridBagConstraints);

        redoListButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/down_small.png"))); // NOI18N
        redoListButton.setBorder(null);
        redoListButton.setEnabled(false);
        redoListButton.setFocusPainted(false);
        redoListButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        redoListButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                redoListButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        redoButtonPanel.add(redoListButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 0);
        editButtonPanel.add(redoButtonPanel, gridBagConstraints);

        cutButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/editcut.png"))); // NOI18N
        cutButton.setToolTipText(bundle.getString("Cut")); // NOI18N
        cutButton.setEnabled(false);
        cutButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        cutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cutButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        editButtonPanel.add(cutButton, gridBagConstraints);

        copyButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/editcopy.png"))); // NOI18N
        copyButton.setToolTipText(bundle.getString("Copy")); // NOI18N
        copyButton.setEnabled(false);
        copyButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        copyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copyButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 0);
        editButtonPanel.add(copyButton, gridBagConstraints);

        pasteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/editpaste.png"))); // NOI18N
        pasteButton.setToolTipText(bundle.getString("Paste")); // NOI18N
        pasteButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        pasteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pasteButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 0);
        editButtonPanel.add(pasteButton, gridBagConstraints);

        alignmentButtonGroup.add(leftToRightToggleButton);
        leftToRightToggleButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/leftjust.png"))); // NOI18N
        leftToRightToggleButton.setToolTipText(bundle.getString("LTR_Tooltip")); // NOI18N
        leftToRightToggleButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        leftToRightToggleButton.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                leftToRightToggleButtonItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        editButtonPanel.add(leftToRightToggleButton, gridBagConstraints);

        alignmentButtonGroup.add(rightToLeftToggleButton);
        rightToLeftToggleButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/rightjust.png"))); // NOI18N
        rightToLeftToggleButton.setToolTipText(bundle.getString("RTL_Tooltip")); // NOI18N
        rightToLeftToggleButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        rightToLeftToggleButton.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rightToLeftToggleButtonItemStateChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 2, 0, 0);
        editButtonPanel.add(rightToLeftToggleButton, gridBagConstraints);

        tabButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/indent.png"))); // NOI18N
        tabButton.setToolTipText(bundle.getString("Insert_TAB")); // NOI18N
        tabButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        tabButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tabButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        editButtonPanel.add(tabButton, gridBagConstraints);

        switchLayoutButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/view_top_bottom.png"))); // NOI18N
        switchLayoutButton.setToolTipText(bundle.getString("Cardsides_Top_Down")); // NOI18N
        switchLayoutButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        switchLayoutButton.setName("switchLayoutButton"); // NOI18N
        switchLayoutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                switchLayoutButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 5);
        editButtonPanel.add(switchLayoutButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipady = 5;
        gridBagConstraints.weightx = 1.0;
        add(editButtonPanel, gridBagConstraints);

        fontPanel.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        fontPanel.setLayout(new java.awt.GridBagLayout());

        fontComboBox.setFont(new java.awt.Font("Dialog", 0, 10));
        fontComboBox.setName("fontComboBox"); // NOI18N
        fontComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fontComboBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        fontPanel.add(fontComboBox, gridBagConstraints);

        fontSizeComboBox.setEditable(true);
        fontSizeComboBox.setFont(new java.awt.Font("Dialog", 0, 10));
        fontSizeComboBox.setPreferredSize(new java.awt.Dimension(60, 24));
        fontSizeComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fontSizeComboBoxActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        fontPanel.add(fontSizeComboBox, gridBagConstraints);

        boldToggleButton.setText(bundle.getString("Bold_Mnemonic")); // NOI18N
        boldToggleButton.setToolTipText(bundle.getString("Bold")); // NOI18N
        boldToggleButton.setMargin(new java.awt.Insets(2, 0, 2, 0));
        boldToggleButton.setPreferredSize(new java.awt.Dimension(26, 26));
        boldToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boldToggleButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 0);
        fontPanel.add(boldToggleButton, gridBagConstraints);

        italicToggleButton.setFont(new java.awt.Font("Dialog", 3, 12));
        italicToggleButton.setText(bundle.getString("Italic_Mnemonic")); // NOI18N
        italicToggleButton.setToolTipText(bundle.getString("Italic")); // NOI18N
        italicToggleButton.setMargin(new java.awt.Insets(2, 0, 2, 0));
        italicToggleButton.setPreferredSize(new java.awt.Dimension(26, 26));
        italicToggleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                italicToggleButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        fontPanel.add(italicToggleButton, gridBagConstraints);

        foregroundButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/foreground.png"))); // NOI18N
        foregroundButton.setToolTipText(bundle.getString("Font_Color")); // NOI18N
        foregroundButton.setFocusPainted(false);
        foregroundButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        foregroundButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                foregroundButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        fontPanel.add(foregroundButton, gridBagConstraints);

        backgroundButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pauker/icons/background.png"))); // NOI18N
        backgroundButton.setToolTipText(bundle.getString("Background_Color")); // NOI18N
        backgroundButton.setFocusPainted(false);
        backgroundButton.setMargin(new java.awt.Insets(0, 0, 0, 0));
        backgroundButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backgroundButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        fontPanel.add(backgroundButton, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipady = 5;
        gridBagConstraints.weightx = 1.0;
        add(fontPanel, gridBagConstraints);

        splitPane.setResizeWeight(0.5);
        splitPane.setContinuousLayout(true);
        splitPane.setName("splitPane"); // NOI18N

        frontSidePanel.setLayout(new java.awt.GridBagLayout());

        frontSideLabel.setFont(new java.awt.Font("Dialog", 0, 10));
        frontSideLabel.setText(bundle.getString("Card_Frontside")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        frontSidePanel.add(frontSideLabel, gridBagConstraints);

        frontSideTextArea.setComponentPopupMenu(textAreaPopupMenu);
        frontSideTextArea.setWrapStyleWord(true);
        frontSideTextArea.setName("frontSideTextArea"); // NOI18N
        frontSideTextArea.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                frontSideTextAreaCaretUpdate(evt);
            }
        });
        frontSideTextArea.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                frontSideTextAreaFocusGained(evt);
            }
        });
        frontSideScrollPane.setViewportView(frontSideTextArea);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.RELATIVE;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        frontSidePanel.add(frontSideScrollPane, gridBagConstraints);

        splitPane.setLeftComponent(frontSidePanel);

        reverseSidePanel.setLayout(new java.awt.GridBagLayout());

        reverseSideLabel.setFont(new java.awt.Font("Dialog", 0, 10));
        reverseSideLabel.setText(bundle.getString("Card_ReverseSide")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 5);
        reverseSidePanel.add(reverseSideLabel, gridBagConstraints);

        reverseSideTextArea.setComponentPopupMenu(textAreaPopupMenu);
        reverseSideTextArea.setWrapStyleWord(true);
        reverseSideTextArea.setName("reverseSideTextArea"); // NOI18N
        reverseSideTextArea.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                reverseSideTextAreaCaretUpdate(evt);
            }
        });
        reverseSideTextArea.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                reverseSideTextAreaFocusGained(evt);
            }
        });
        reverseSideScrollPane.setViewportView(reverseSideTextArea);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        reverseSidePanel.add(reverseSideScrollPane, gridBagConstraints);

        splitPane.setRightComponent(reverseSidePanel);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(splitPane, gridBagConstraints);

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 10));
        jLabel1.setText(bundle.getString("Repeating_Method_Colon")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.insets = new java.awt.Insets(0, 5, 0, 0);
        add(jLabel1, gridBagConstraints);

        repeatingMethodComboBox.setFont(new java.awt.Font("Dialog", 0, 10));
        repeatingMethodComboBox.setName("repeatingMethodComboBox"); // NOI18N
        repeatingMethodComboBox.setRenderer(new MyListCellRenderer());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
        add(repeatingMethodComboBox, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents
    private void clearMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearMenuItemActionPerformed
        JTextArea textArea = (JTextArea) textAreaPopupMenu.getInvoker();
        textArea.setText(null);
    }//GEN-LAST:event_clearMenuItemActionPerformed

    private void selectAllMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_selectAllMenuItemActionPerformed
        JTextArea textArea = (JTextArea) textAreaPopupMenu.getInvoker();
        textArea.selectAll();
    }//GEN-LAST:event_selectAllMenuItemActionPerformed

    private void redoMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_redoMenuItemActionPerformed
        redo();
    }//GEN-LAST:event_redoMenuItemActionPerformed

    private void undoMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_undoMenuItemActionPerformed
        undo();
    }//GEN-LAST:event_undoMenuItemActionPerformed

    private void pasteMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pasteMenuItemActionPerformed
        JTextArea textArea = (JTextArea) textAreaPopupMenu.getInvoker();
        textArea.paste();
    }//GEN-LAST:event_pasteMenuItemActionPerformed

    private void copyMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copyMenuItemActionPerformed
        JTextArea textArea = (JTextArea) textAreaPopupMenu.getInvoker();
        textArea.copy();
    }//GEN-LAST:event_copyMenuItemActionPerformed

    private void cutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cutMenuItemActionPerformed
        JTextArea textArea = (JTextArea) textAreaPopupMenu.getInvoker();
        textArea.cut();
    }//GEN-LAST:event_cutMenuItemActionPerformed

    private void caretUpdate(CaretEvent evt) {
        boolean selection = (evt.getDot() != evt.getMark());
        cutButton.setEnabled(selection);
        copyButton.setEnabled(selection);
        cutMenuItem.setEnabled(selection);
        copyMenuItem.setEnabled(selection);
    }

    private void reverseSideTextAreaCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_reverseSideTextAreaCaretUpdate
        caretUpdate(evt);
    }//GEN-LAST:event_reverseSideTextAreaCaretUpdate

    private void reverseSideTextAreaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_reverseSideTextAreaFocusGained
        lastFocusSide = Card.Element.REVERSE_SIDE;
        setFontElements(reverseSideTextArea);
    }//GEN-LAST:event_reverseSideTextAreaFocusGained

    private void frontSideTextAreaCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_frontSideTextAreaCaretUpdate
        caretUpdate(evt);
    }//GEN-LAST:event_frontSideTextAreaCaretUpdate

    private void frontSideTextAreaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_frontSideTextAreaFocusGained
        lastFocusSide = Card.Element.FRONT_SIDE;
        setFontElements(frontSideTextArea);
    }//GEN-LAST:event_frontSideTextAreaFocusGained

    private void leftToRightToggleButtonItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_leftToRightToggleButtonItemStateChanged
        if (leftToRightToggleButton.isSelected()) {
            if (lastFocusSide == Card.Element.FRONT_SIDE) {
                frontSideTextArea.setComponentOrientation(
                        ComponentOrientation.LEFT_TO_RIGHT);
            } else {
                reverseSideTextArea.setComponentOrientation(
                        ComponentOrientation.LEFT_TO_RIGHT);
            }
        }
        restoreFocus();
    }//GEN-LAST:event_leftToRightToggleButtonItemStateChanged

    private void rightToLeftToggleButtonItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rightToLeftToggleButtonItemStateChanged
        if (rightToLeftToggleButton.isSelected()) {
            if (lastFocusSide == Card.Element.FRONT_SIDE) {
                frontSideTextArea.setComponentOrientation(
                        ComponentOrientation.RIGHT_TO_LEFT);
            } else {
                reverseSideTextArea.setComponentOrientation(
                        ComponentOrientation.RIGHT_TO_LEFT);
            }
        }
        restoreFocus();
    }//GEN-LAST:event_rightToLeftToggleButtonItemStateChanged

    private void switchLayoutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_switchLayoutButtonActionPerformed
        if (splitPane.getOrientation() == JSplitPane.HORIZONTAL_SPLIT) {
            setSplitOrientation(JSplitPane.VERTICAL_SPLIT);
        } else {
            setSplitOrientation(JSplitPane.HORIZONTAL_SPLIT);
        }
    }//GEN-LAST:event_switchLayoutButtonActionPerformed

    private void backgroundButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backgroundButtonActionPerformed
        Color background = JColorChooser.showDialog(this,
                PaukerFrame.STRINGS.getString("Choose_Background_Color"),
                Color.white);

        if (background != null) {
            if (lastFocusSide == Card.Element.FRONT_SIDE) {
                frontSideTextArea.setBackground(background);
            } else {
                reverseSideTextArea.setBackground(background);
            }
        }
        restoreFocus();
    }//GEN-LAST:event_backgroundButtonActionPerformed

    private void foregroundButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_foregroundButtonActionPerformed
        Color foreground = JColorChooser.showDialog(this,
                PaukerFrame.STRINGS.getString("Choose_Font_Color"),
                Color.black);

        if (foreground != null) {
            if (lastFocusSide == Card.Element.FRONT_SIDE) {
                frontSideTextArea.setForeground(foreground);
            } else {
                reverseSideTextArea.setForeground(foreground);
            }
        }
        restoreFocus();
    }//GEN-LAST:event_foregroundButtonActionPerformed

    private void italicToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_italicToggleButtonActionPerformed
        if (lastFocusSide == Card.Element.FRONT_SIDE) {
            setFontStyle(Font.ITALIC,
                    italicToggleButton.isSelected(), frontSideTextArea);
        } else {
            setFontStyle(Font.ITALIC,
                    italicToggleButton.isSelected(), reverseSideTextArea);
        }
        restoreFocus();
    }//GEN-LAST:event_italicToggleButtonActionPerformed

    private void boldToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boldToggleButtonActionPerformed
        if (lastFocusSide == Card.Element.FRONT_SIDE) {
            setFontStyle(Font.BOLD,
                    boldToggleButton.isSelected(), frontSideTextArea);
        } else {
            setFontStyle(Font.BOLD,
                    boldToggleButton.isSelected(), reverseSideTextArea);
        }
        restoreFocus();
    }//GEN-LAST:event_boldToggleButtonActionPerformed

    private void fontSizeComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fontSizeComboBoxActionPerformed
        if (lastFocusSide == Card.Element.FRONT_SIDE) {
            changeFontSize(frontSideTextArea);
        } else {
            changeFontSize(reverseSideTextArea);
        }
        restoreFocus();
    }//GEN-LAST:event_fontSizeComboBoxActionPerformed

    private void fontComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fontComboBoxActionPerformed
        if (lastFocusSide == Card.Element.FRONT_SIDE) {
            changeFont(frontSideTextArea);
        } else {
            changeFont(reverseSideTextArea);
        }
        restoreFocus();
    }//GEN-LAST:event_fontComboBoxActionPerformed

    private void popupListMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_popupListMouseExited
        scrollTimer.stop();
    }//GEN-LAST:event_popupListMouseExited

    private void popupListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_popupListMouseClicked
        Point point = evt.getPoint();
        int index = popupList.locationToIndex(point);

        // redo or undo up to index
        Component invoker = popupMenu.getInvoker();
        if (invoker == undoButtonPanel) {
            undoManager.undoTo(index);
        } else {
            undoManager.redoTo(index);
        }
        updateUndoRedoButtons();

        popupMenu.setVisible(false);
        scrollTimer.stop();
        restoreFocus();
    }//GEN-LAST:event_popupListMouseClicked

    private void popupListMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_popupListMouseMoved
        lastMousePoint = evt.getPoint();

        // update selection & counter
        updatePopup();

        // scroll at margins
        Rectangle viewRectangle = popupScrollPane.getViewport().getViewRect();
        int uppderDiff = lastMousePoint.y - viewRectangle.y;
        if (uppderDiff < 15) {
            scrollIncrement = uppderDiff - 15;
            scrollTimer.start();
        } else {
            int lowerDiff =
                    viewRectangle.y + viewRectangle.height - lastMousePoint.y;
            if (lowerDiff < 15) {
                scrollIncrement = 15 - lowerDiff;
                scrollTimer.start();
            } else {
                scrollTimer.stop();
            }
        }
    }//GEN-LAST:event_popupListMouseMoved

    private void redoListButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_redoListButtonActionPerformed
        showPopup(redoButtonPanel, undoManager.getRedoPresentations());
    }//GEN-LAST:event_redoListButtonActionPerformed

    private void undoListButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_undoListButtonActionPerformed
        showPopup(undoButtonPanel, undoManager.getUndoPresentations());
    }//GEN-LAST:event_undoListButtonActionPerformed

    private void tabButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tabButtonActionPerformed
        insertTabAction.actionPerformed(new ActionEvent(
                tabButton, ActionEvent.ACTION_PERFORMED, "TabInsert"));
        restoreFocus();
    }//GEN-LAST:event_tabButtonActionPerformed

    private void cutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cutButtonActionPerformed
        cutAction.actionPerformed(new ActionEvent(
                cutButton, ActionEvent.ACTION_PERFORMED, "Cut"));
        cutButton.setEnabled(false);
        copyButton.setEnabled(false);
        restoreFocus();
    }//GEN-LAST:event_cutButtonActionPerformed

    private void copyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copyButtonActionPerformed
        copyAction.actionPerformed(new ActionEvent(copyButton,
                ActionEvent.ACTION_PERFORMED, "Copy"));
        restoreFocus();
    }//GEN-LAST:event_copyButtonActionPerformed

    private void pasteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pasteButtonActionPerformed
        pasteAction.actionPerformed(new ActionEvent(pasteButton,
                ActionEvent.ACTION_PERFORMED, "Paste"));
        restoreFocus();
    }//GEN-LAST:event_pasteButtonActionPerformed

    private void undoButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_undoButtonActionPerformed
        undo();
    }//GEN-LAST:event_undoButtonActionPerformed

    private void redoButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_redoButtonActionPerformed
        redo();
    }//GEN-LAST:event_redoButtonActionPerformed

    private void redo() {
        try {
            undoManager.redo();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "could not redo", e);
        }
        updateUndoRedoButtons();
        restoreFocus();
    }

    private void undo() {
        try {
            undoManager.undo();
        } catch (Exception e) {
            logger.log(Level.SEVERE, "coudl not undo", e);
        }
        updateUndoRedoButtons();
        restoreFocus();
    }

    private void documentChanged(DocumentEvent documentEvent) {
        if (documentEvent.getDocument() == frontSideDocument) {
            lastFocusSide = Card.Element.FRONT_SIDE;
        } else {
            lastFocusSide = Card.Element.REVERSE_SIDE;
        }
    }

    private void setFontStyle(int style, boolean setStyle,
            JTextComponent textComponent) {
        Font oldFont = textComponent.getFont();
        int newStyle = oldFont.getStyle();
        if (setStyle) {
            newStyle += style;
        } else {
            newStyle -= style;
        }
        Font newFont = oldFont.deriveFont(newStyle);
        textComponent.setFont(newFont);
    }

    private void changeFontSize(JTextComponent textComponent) {
        int fontSize =
                ((Integer) fontSizeComboBox.getSelectedItem()).intValue();
        Font oldFont = textComponent.getFont();
        Font newFont = oldFont.deriveFont((float) fontSize);
        textComponent.setFont(newFont);
    }

    private void changeFont(JTextComponent textComponent) {
        Font oldFont = textComponent.getFont();
        int fontSize = oldFont.getSize();
        int fontStyle = oldFont.getStyle();
        String fontFamilyName = (String) fontComboBox.getSelectedItem();
        Font font = new Font(fontFamilyName, fontStyle, fontSize);
        textComponent.setFont(font);
    }

    private void showPopup(JPanel panel, List presentations) {
        Object[] presentationArray = presentations.toArray();
        // fill list
        popupList.setListData(presentationArray);

        // set counter
        int presentationCounter = presentationArray.length;
        String counterString = null;
        if (presentationCounter == 1) {
            counterString =
                    "1 " + PaukerFrame.STRINGS.getString("Nr_Of_Action_Sng");
        } else {
            counterString = MessageFormat.format(
                    PaukerFrame.STRINGS.getString("Nr_Of_Action_Pl"),
                    Integer.valueOf(presentationCounter));
        }
        undoCounterLabel.setText(counterString);

        // determine popup location
        Dimension panelSize = panel.getSize();
        popupMenu.setPreferredSize(null);
        Dimension popupSize = popupMenu.getPreferredSize();
        popupSize.width += 30;
        popupMenu.setPreferredSize(popupSize);

        Point popupLocation = new Point(
                panelSize.width - popupSize.width,
                panelSize.height);

        // reset scrollbar
        popupScrollPane.getVerticalScrollBar().setValue(0);

        // show popup
        popupMenu.show(panel, popupLocation.x, popupLocation.y);
    }

    private void updatePopup() {
        // update index
        int index = popupList.locationToIndex(lastMousePoint);
        popupList.setSelectionInterval(0, index);

        // update counter text
        String counterString = null;
        int counter = popupList.getModel().getSize();
        if (counter == 1) {
            counterString =
                    "1/1 " + PaukerFrame.STRINGS.getString("Nr_Of_Action_Sng");
        } else {
            counterString = MessageFormat.format(
                    PaukerFrame.STRINGS.getString("Nr_Of_Action_Pl"),
                    (index + 1) + "/" + counter);
        }
        undoCounterLabel.setText(counterString);
    }

    private void setFontElements(JTextComponent textComponent) {
        Font font = textComponent.getFont();
        fontComboBox.setSelectedItem(font.getFamily());
        fontSizeComboBox.setSelectedItem(Integer.valueOf(font.getSize()));
        boldToggleButton.setSelected(font.isBold());
        italicToggleButton.setSelected(font.isItalic());
        if (textComponent.getComponentOrientation().isLeftToRight()) {
            leftToRightToggleButton.setSelected(true);
        } else {
            rightToLeftToggleButton.setSelected(true);
        }
    }

    private void restoreFocus() {
        if (lastFocusSide == Card.Element.FRONT_SIDE) {
            frontSideTextArea.requestFocusInWindow();
        } else {
            reverseSideTextArea.requestFocusInWindow();
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup alignmentButtonGroup;
    private javax.swing.JButton backgroundButton;
    private javax.swing.JToggleButton boldToggleButton;
    private javax.swing.JMenuItem clearMenuItem;
    private javax.swing.JButton copyButton;
    private javax.swing.JMenuItem copyMenuItem;
    private javax.swing.JButton cutButton;
    private javax.swing.JMenuItem cutMenuItem;
    private javax.swing.JPanel editButtonPanel;
    private javax.swing.JComboBox fontComboBox;
    private javax.swing.JPanel fontPanel;
    private javax.swing.JComboBox fontSizeComboBox;
    private javax.swing.JButton foregroundButton;
    private javax.swing.JLabel frontSideLabel;
    private javax.swing.JPanel frontSidePanel;
    private javax.swing.JScrollPane frontSideScrollPane;
    private javax.swing.JTextArea frontSideTextArea;
    private javax.swing.JToggleButton italicToggleButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JToggleButton leftToRightToggleButton;
    private javax.swing.JLabel memorizingLabel;
    private javax.swing.JButton pasteButton;
    private javax.swing.JMenuItem pasteMenuItem;
    private javax.swing.JList popupList;
    private javax.swing.JPopupMenu popupMenu;
    private javax.swing.JPanel popupPanel;
    private javax.swing.JScrollPane popupScrollPane;
    private javax.swing.JButton redoButton;
    private javax.swing.JPanel redoButtonPanel;
    private javax.swing.JButton redoListButton;
    private javax.swing.JMenuItem redoMenuItem;
    private javax.swing.JComboBox repeatingMethodComboBox;
    private javax.swing.JLabel reverseSideLabel;
    private javax.swing.JPanel reverseSidePanel;
    private javax.swing.JScrollPane reverseSideScrollPane;
    private javax.swing.JTextArea reverseSideTextArea;
    private javax.swing.JToggleButton rightToLeftToggleButton;
    private javax.swing.JMenuItem selectAllMenuItem;
    private javax.swing.JSplitPane splitPane;
    private javax.swing.JButton switchLayoutButton;
    private javax.swing.JButton tabButton;
    private javax.swing.JPopupMenu textAreaPopupMenu;
    private javax.swing.JLabel typingLabel;
    private javax.swing.JButton undoButton;
    private javax.swing.JPanel undoButtonPanel;
    private javax.swing.JLabel undoCounterLabel;
    private javax.swing.JButton undoListButton;
    private javax.swing.JMenuItem undoMenuItem;
    // End of variables declaration//GEN-END:variables
}
