package pauker.program.gui.swing;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.help.JHelpContentViewer;
import javax.help.plaf.basic.BasicContentViewerUI;
import javax.swing.JComponent;
import javax.swing.event.HyperlinkEvent;

/**
 * UI subclass that will open external links with the Desktop class
 * <p/>
 * @author Ronny Standtke <Ronny.Standtke@gmx.net>
 */
public class ExternalLinkContentViewerUI extends BasicContentViewerUI {
	private final static Logger LOGGER =
			Logger.getLogger(ExternalLinkContentViewerUI.class.getName());

	/**
	 * creates a new ExternalLinkContentViewerUI
	 * <p/>
	 * @param viewer the viewer
	 */
	public ExternalLinkContentViewerUI(JHelpContentViewer viewer) {
		super(viewer);
	}

	/**
	 * creates the UI
	 * <p/>
	 * @param component the component
	 * @return the created UI
	 */
	public static javax.swing.plaf.ComponentUI createUI(JComponent component) {
		return new ExternalLinkContentViewerUI((JHelpContentViewer) component);
	}

	/**
	 * called when a hyperlink event happens
	 * <p/>
	 * @param hyperlinkEvent the HyperlinkEvent
	 */
	@Override
	public void hyperlinkUpdate(HyperlinkEvent hyperlinkEvent) {
		if (hyperlinkEvent.getEventType()
				== HyperlinkEvent.EventType.ACTIVATED) {
			URL url = hyperlinkEvent.getURL();
			if (url.getProtocol().equalsIgnoreCase("mailto")
					|| url.getProtocol().equalsIgnoreCase("http")
					|| url.getProtocol().equalsIgnoreCase("ftp")) {
				if (Desktop.isDesktopSupported()) {
					try {
						Desktop.getDesktop().browse(url.toURI());
					} catch (URISyntaxException ex) {
						LOGGER.log(Level.SEVERE, null, ex);
					} catch (IOException ex) {
						LOGGER.log(Level.SEVERE, null, ex);
					}
				} else {
					super.hyperlinkUpdate(hyperlinkEvent);
					LOGGER.warning("Desktop is not supported");
				}
			} else {
				super.hyperlinkUpdate(hyperlinkEvent);
			}
		}
	}
}