/*
 * GraphicalStatisticPanel.java
 *
 * Created on 4. September 2002, 15:37
 */

package pauker.program.gui.swing;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.text.MessageFormat;
import java.util.List;
import java.util.ResourceBundle;
import javax.swing.JLabel;
import javax.swing.JPanel;
import pauker.program.Batch;
import pauker.program.Lesson;
import pauker.program.LongTermBatch;

/**
 *
 * @author Ronny.Standtke@gmx.net
 */
public class GraphicalStatisticPanel extends JPanel {

    private static final int BAR_WIDTH = 30;
    private static final int TOP_INSET = 10;
    private static final int BOTTOM_INSET = 10;
    private final String summaryString;
    private final String notLearnedString;
    private final String ustmString;
    private final String stmString;
    private final String batchString;
    private final Color[] colors = new Color[4];
    private final int[] heights = new int[4];
    private final BasicStroke bigStroke;
    private final GridBagConstraints normalConstraints;
    private final GridBagConstraints lastConstraints;
    private final Font labelFont = new java.awt.Font("Dialog", 0, 10);
    private final GridBagLayout layout;
    private final ResourceBundle strings;
    private int height = 0;
    private int croppedHeight = 0;
    private Lesson lesson;
    private int lastNumberOfLongTermBatches = 0;
    private JLabel[] longTermLabels;

    private enum BatchType {

        NONE, SUMMARY, UNLEARNED, ULTRA_SHORT_TERM, SHORT_TERM, LONG_TERM
    }
    private BatchType selectedBatchType;
    private BatchType highlightedBatchType;
    private int selectedLongTermBatchNumber;
    private int highlightedLongTermBatchNumber;
    private PaukerFrame paukerFrame;
    private boolean mouseSensitive;

    /** Creates new form GraphicalStatisticPanel */
    public GraphicalStatisticPanel() {

        initComponents();

        layout = (GridBagLayout) longTermBatchesPanel.getLayout();

        normalConstraints = new java.awt.GridBagConstraints();
        normalConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        normalConstraints.insets = new java.awt.Insets(0, 15, 0, 0);
        normalConstraints.weighty = 1.0;

        lastConstraints = new java.awt.GridBagConstraints();
        lastConstraints.anchor = java.awt.GridBagConstraints.SOUTHWEST;
        lastConstraints.insets = new java.awt.Insets(0, 15, 0, 15);
        lastConstraints.weightx = 1.0;
        lastConstraints.weighty = 1.0;

        longTermLabels = new JLabel[0];

        strings = ResourceBundle.getBundle("pauker/Strings");

        summaryString = strings.getString("SummaryLabel");
        notLearnedString = strings.getString("NotLearnedLabel");
        ustmString = strings.getString("USTM_Label");
        stmString = strings.getString("STM_Label");
        batchString = strings.getString("BatchLabel");

        bigStroke = new BasicStroke(
                2, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND);

        privateSetLesson(new Lesson());

        mouseSensitive = true;
    }

    /**
     * sets the PaukerFrame to use
     * @param paukerFrame the PaukerFrame to use
     */
    public void setPaukerFrame(PaukerFrame paukerFrame) {
        this.paukerFrame = paukerFrame;
    }

    @Override
    public void paintComponent(Graphics graphics) {

        super.paintComponent(graphics);

        // early return
        int maxCards = lesson.getNumberOfCards();
        if (maxCards == 0) {
            return;
        }

        height = summaryLabel.getLocation().y;
        croppedHeight = height - TOP_INSET - BOTTOM_INSET;

        Graphics2D graphics2D = (Graphics2D) graphics;

        // paint summary
        int numberOfUnlearnedCards =
                lesson.getUnlearnedBatch().getNumberOfCards();
        int numberOfUstmCards = lesson.getUltraShortTermList().size();
        int numberOfStmCards = lesson.getShortTermList().size();
        int numberOfAllStmCards = numberOfUstmCards + numberOfStmCards;
        int numberOfExpiredCards = lesson.getNumberOfExpiredCards();
        int numberOfLearnedCards = maxCards - numberOfUnlearnedCards -
                numberOfAllStmCards - numberOfExpiredCards;
        int numberOfBars = 0;
        int heightSum = 0;
        if (numberOfUnlearnedCards != 0) {
            colors[0] = PaukerFrame.RED;
            heights[0] = croppedHeight * numberOfUnlearnedCards / maxCards;
            heightSum = heights[0];
            numberOfBars++;
        }
        if (numberOfAllStmCards != 0) {
            colors[numberOfBars] = PaukerFrame.YELLOW;
            heights[numberOfBars] =
                    croppedHeight * numberOfAllStmCards / maxCards;
            heightSum += heights[numberOfBars];
            numberOfBars++;
        }
        if (numberOfLearnedCards != 0) {
            colors[numberOfBars] = PaukerFrame.GREEN;
            heights[numberOfBars] =
                    croppedHeight * numberOfLearnedCards / maxCards;
            heightSum += heights[numberOfBars];
            numberOfBars++;
        }
        if (numberOfExpiredCards != 0) {
            colors[numberOfBars] = PaukerFrame.BLUE;
            heights[numberOfBars] = croppedHeight - heightSum;
            numberOfBars++;
        }
        paintBar(graphics2D, summaryLabel, numberOfBars,
                selectedBatchType == BatchType.SUMMARY,
                highlightedBatchType == BatchType.SUMMARY);

        // paint unlearned batch
        if (numberOfUnlearnedCards > 0) {
            colors[0] = PaukerFrame.RED;
            heights[0] = getHeight(maxCards, numberOfUnlearnedCards);
            paintBar(graphics2D, notLearnedLabel, 1,
                    selectedBatchType == BatchType.UNLEARNED,
                    highlightedBatchType == BatchType.UNLEARNED);
        }

        // paint ultra-short-term batch
        if (numberOfUstmCards > 0) {
            colors[0] = PaukerFrame.YELLOW;
            heights[0] = getHeight(maxCards, numberOfUstmCards);
            paintBar(graphics2D, ustmLabel, 1,
                    selectedBatchType == BatchType.ULTRA_SHORT_TERM,
                    highlightedBatchType == BatchType.ULTRA_SHORT_TERM);
        }

        // paint short-term batch
        if (numberOfStmCards > 0) {
            colors[0] = PaukerFrame.YELLOW;
            heights[0] = getHeight(maxCards, numberOfStmCards);
            paintBar(graphics2D, stmLabel, 1,
                    selectedBatchType == BatchType.SHORT_TERM,
                    highlightedBatchType == BatchType.SHORT_TERM);
        }

        // paint all long-term batches
        List<LongTermBatch> longTermBatches = lesson.getLongTermBatches();
        for (int i = 0, size = longTermBatches.size(); i < size; i++) {
            LongTermBatch longTermBatch = longTermBatches.get(i);
            int numberOfCards = longTermBatch.getNumberOfCards();
            if (numberOfCards != 0) {
                int componentCount = longTermBatchesPanel.getComponentCount();
                if (i < componentCount) {
                    JLabel label =
                            (JLabel) longTermBatchesPanel.getComponent(i);
                    numberOfExpiredCards =
                            longTermBatch.getNumberOfExpiredCards();
                    numberOfLearnedCards = numberOfCards - numberOfExpiredCards;
                    heightSum = 0;
                    if (numberOfLearnedCards != 0) {
                        colors[0] = PaukerFrame.GREEN;
                        heights[0] = getHeight(maxCards, numberOfLearnedCards);
                        heightSum++;
                    }
                    if (numberOfExpiredCards != 0) {
                        colors[heightSum] = PaukerFrame.BLUE;
                        heights[heightSum] =
                                getHeight(maxCards, numberOfExpiredCards);
                        heightSum++;
                    }
                    paintBar(graphics2D, label, heightSum,
                            (selectedBatchType == BatchType.LONG_TERM) &&
                            (selectedLongTermBatchNumber == i),
                            (highlightedBatchType == BatchType.LONG_TERM) &&
                            (highlightedLongTermBatchNumber == i));

                } else {
                    System.out.println("WARNING: JLabel for long term batch " +
                            (i + 1) + " not found!");
                }
            }
        }
    }

    private int getHeight(int maxCards, int numberOfCards) {
        int result = (croppedHeight * numberOfCards) / maxCards;
        if (result == 0) {
            result = 1;
        }
        return result;
    }

    private void paintBar(Graphics2D graphics, JLabel label, int numberOfBars,
            boolean selected, boolean highlighted) {

        Point labelLocation = label.getLocation();
        Dimension labelSize = label.getSize();
        int barLocationX = labelLocation.x + (labelSize.width - BAR_WIDTH) / 2;
        if (label.getParent() == longTermBatchesPanel) {
            barLocationX += longTermBatchesPanel.getLocation().x;
        }

        // paint bars
        int partBottom = height - BOTTOM_INSET;

        for (int i = 0; i < numberOfBars; i++) {
            int barHeight = heights[i];
            Color color = colors[i];
            int barLocationY = partBottom - barHeight;
            graphics.setPaint(color);
            graphics.fillRect(barLocationX, barLocationY, BAR_WIDTH, barHeight);
            partBottom = barLocationY;
        }

        // paint transitions
        partBottom = height - BOTTOM_INSET - heights[0];
        for (int i = 1; i < numberOfBars; i++) {
            int barHeight = heights[i];
            int bottomHeight = heights[i - 1];
            Color color = colors[i];
            Color bottomColor = colors[i - 1];
            int transitionOffset = 0;
            if (i == 1) {
                transitionOffset = Math.min(bottomHeight, 5);
            } else {
                transitionOffset = Math.min(bottomHeight / 2, 5);
            }
            if (i == numberOfBars - 1) {
                transitionOffset = Math.min(barHeight, transitionOffset);
            } else {
                transitionOffset = Math.min(barHeight / 2, transitionOffset);
            }
            if (transitionOffset != 0) {
                int top = partBottom - transitionOffset;
                int bottom = partBottom + transitionOffset;
                GradientPaint gradientPaint = new GradientPaint(
                        barLocationX, top, color,
                        barLocationX, bottom, bottomColor);
                graphics.setPaint(gradientPaint);
                graphics.fillRect(barLocationX, top,
                        BAR_WIDTH, bottom - top);
            }
            partBottom -= barHeight;
        }

        // draw border around batch rectangle
        int barHeight = 0;
        for (int i = 0; i < numberOfBars; i++) {
            barHeight += heights[i];
        }
        int barLocationY = height - BOTTOM_INSET - barHeight;
        if (highlighted) {
            // the mouse is hovering over this batch
            graphics.setPaint(Color.yellow);
            graphics.setStroke(bigStroke);
            graphics.drawRect(barLocationX, barLocationY, BAR_WIDTH, barHeight);
        } else if (selected) {
            // this is the selected batch
            graphics.setPaint(Color.red);
            graphics.setStroke(bigStroke);
            graphics.drawRect(barLocationX, barLocationY, BAR_WIDTH, barHeight);
        } else {
            graphics.setPaint(Color.lightGray);
            graphics.draw3DRect(
                    barLocationX, barLocationY, BAR_WIDTH, barHeight, true);
        }
    }

    /**
     * sets the lesson to paint
     * @param lesson the lesson to paint
     */
    public void setLesson(Lesson lesson) {
        privateSetLesson(lesson);
    }

    /**
     * clears the selection
     */
    public void clearSelection() {
        selectedBatchType = BatchType.NONE;
        repaint();
    }

    /**
     * selects the summary batch
     */
    public void selectSummary() {
        selectedBatchType = BatchType.SUMMARY;
        repaint();
    }

    /**
     * selectes the unlearned batch
     */
    public void selectUnlearnedBatch() {
        selectedBatchType = BatchType.UNLEARNED;
        repaint();
    }

    /**
     * selects the ultra short term batch
     */
    public void selectUltraShortTermBatch() {
        selectedBatchType = BatchType.ULTRA_SHORT_TERM;
        repaint();
    }

    /**
     * selectes the short term batch
     */
    public void selectShortTermBatch() {
        selectedBatchType = BatchType.SHORT_TERM;
        repaint();
    }

    /**
     * seletcts a long term batch
     * @param selectedLongTermBatchNumber the number of the long term batch to
     * select
     */
    public void selectLongTermBatch(int selectedLongTermBatchNumber) {
        selectedBatchType = BatchType.LONG_TERM;
        this.selectedLongTermBatchNumber = selectedLongTermBatchNumber;
        repaint();
    }

    /**
     * specifies, if the panel should be sensitive to mouse events
     * @param mouseSensitive if <code>true</code>, the panel is sensitive to
     * mouse events, if <code>false</code> not
     */
    public void setMouseSensitive(boolean mouseSensitive) {
        this.mouseSensitive = mouseSensitive;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Erzeugter Quelltext ">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        descriptionLabel = new javax.swing.JLabel();
        summaryLabel = new javax.swing.JLabel();
        notLearnedLabel = new javax.swing.JLabel();
        ustmLabel = new javax.swing.JLabel();
        stmLabel = new javax.swing.JLabel();
        longTermBatchesPanel = new javax.swing.JPanel();

        setLayout(new java.awt.GridBagLayout());

        setDebugGraphicsOptions(javax.swing.DebugGraphics.NONE_OPTION);
        addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseMoved(java.awt.event.MouseEvent evt) {
                formMouseMoved(evt);
            }
        });
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                formMouseExited(evt);
            }
        });

        descriptionLabel.setFont(new java.awt.Font("Dialog", 0, 10));
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("pauker/Strings"); // NOI18N
        descriptionLabel.setText(bundle.getString("Description_Label")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(50, 5, 0, 0);
        add(descriptionLabel, gridBagConstraints);

        summaryLabel.setFont(new java.awt.Font("Dialog", 0, 10));
        summaryLabel.setText(bundle.getString("SummaryLabel")); // NOI18N
        summaryLabel.setName("summaryLabel");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(50, 15, 0, 0);
        add(summaryLabel, gridBagConstraints);

        notLearnedLabel.setFont(new java.awt.Font("Dialog", 0, 10));
        notLearnedLabel.setText(bundle.getString("NotLearnedLabel")); // NOI18N
        notLearnedLabel.setName("notLearnedLabel");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(50, 15, 0, 0);
        add(notLearnedLabel, gridBagConstraints);

        ustmLabel.setFont(new java.awt.Font("Dialog", 0, 10));
        ustmLabel.setText(bundle.getString("USTM_Label")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(50, 15, 0, 0);
        add(ustmLabel, gridBagConstraints);

        stmLabel.setFont(new java.awt.Font("Dialog", 0, 10));
        stmLabel.setText(bundle.getString("STM_Label")); // NOI18N
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTH;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(50, 15, 0, 0);
        add(stmLabel, gridBagConstraints);

        longTermBatchesPanel.setLayout(new java.awt.GridBagLayout());

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.anchor = java.awt.GridBagConstraints.SOUTHWEST;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(50, 0, 0, 0);
        add(longTermBatchesPanel, gridBagConstraints);

    }// </editor-fold>//GEN-END:initComponents
    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
        if (!mouseSensitive) {
            return;
        }

        Point clickedPoint = evt.getPoint();

        // we intentionally ignore here USTM and STM
        Batch selectedBatch = null;
        if (pointIsOverLabel(clickedPoint, summaryLabel)) {
            if (lesson.getNumberOfCards() == 0) {
                return;
            }
            selectedBatchType = BatchType.SUMMARY;
            selectedBatch = lesson.getSummaryBatch();

        } else if (pointIsOverLabel(clickedPoint, notLearnedLabel)) {
            if (lesson.getUnlearnedBatch().getNumberOfCards() == 0) {
                return;
            }
            selectedBatchType = BatchType.UNLEARNED;
            selectedBatch = lesson.getUnlearnedBatch();

        } else {
            // test all long term batches
            for (int i = 0, length = longTermLabels.length; i < length; i++) {
                JLabel longTermLabel = longTermLabels[i];
                if (pointIsOverLabel(clickedPoint, longTermLabel)) {
                    if (lesson.getLongTermBatch(i).getNumberOfCards() == 0) {
                        return;
                    }
                    selectedBatchType = BatchType.LONG_TERM;
                    selectedLongTermBatchNumber = i;
                    selectedBatch = lesson.getLongTermBatch(i);
                    break;
                }
            }
        }

        if ((selectedBatch != null) && (selectedBatch.getNumberOfCards() > 0)) {
            paukerFrame.loadBatch(selectedBatch);
            repaint();
        }
    }//GEN-LAST:event_formMouseClicked

    private void formMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseExited
        if (!mouseSensitive) {
            return;
        }
        highlightedBatchType = BatchType.NONE;
        repaint();
    }//GEN-LAST:event_formMouseExited

    private void formMouseMoved(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseMoved
        if (!mouseSensitive) {
            return;
        }

        Point clickedPoint = evt.getPoint();

        // detect, if mouse movement lead to a change so that we have to repaint
        BatchType oldHighlightedBatchType = highlightedBatchType;
        int oldHighlightedLongTermBatchNumber = highlightedLongTermBatchNumber;

        highlightedBatchType = BatchType.NONE;
        if (pointIsOverLabel(clickedPoint, summaryLabel)) {
            if (lesson.getNumberOfCards() == 0) {
                return;
            }
            highlightedBatchType = BatchType.SUMMARY;

        } else if (pointIsOverLabel(clickedPoint, notLearnedLabel)) {
            if (lesson.getUnlearnedBatch().getNumberOfCards() == 0) {
                return;
            }
            highlightedBatchType = BatchType.UNLEARNED;

        } else {
            // test all long term batches
            for (int i = 0, length = longTermLabels.length; i < length; i++) {
                JLabel longTermLabel = longTermLabels[i];
                if (pointIsOverLabel(clickedPoint, longTermLabel)) {
                    if (lesson.getLongTermBatch(i).getNumberOfCards() == 0) {
                        return;
                    }
                    highlightedBatchType = BatchType.LONG_TERM;
                    highlightedLongTermBatchNumber = i;
                    break;
                }
            }
        }

        if ((oldHighlightedBatchType != highlightedBatchType) ||
                (highlightedBatchType == BatchType.LONG_TERM &&
                (oldHighlightedLongTermBatchNumber !=
                highlightedLongTermBatchNumber))) {
            repaint();
        }
    }//GEN-LAST:event_formMouseMoved

    private boolean pointIsOverLabel(Point point, JLabel label) {
        Point labelLocation = label.getLocation();
        if (label.getParent() == longTermBatchesPanel) {
            labelLocation.x += longTermBatchesPanel.getLocation().x;
        }
        return (point.x > labelLocation.x) &&
                (point.x < (labelLocation.x + label.getSize().width));
    }
    
    private void privateSetLesson(Lesson lesson) {

        this.lesson = lesson;

        int numberOfCards = lesson.getNumberOfCards();
        int numberOfExpiredCards = lesson.getNumberOfExpiredCards();
        int numberOfUnlearnedCards =
                lesson.getUnlearnedBatch().getNumberOfCards();
        int ustmCards = lesson.getUltraShortTermList().size();
        int stmCards = lesson.getShortTermList().size();
        int learnedCards = numberOfCards - numberOfUnlearnedCards -
                ustmCards - stmCards - numberOfExpiredCards;

        String string = MessageFormat.format(summaryString,
                Integer.valueOf(numberOfExpiredCards),
                Integer.valueOf(learnedCards), Integer.valueOf(numberOfCards));
        PaukerFrame.lazyStringChange(summaryLabel, string);

        string = MessageFormat.format(
                notLearnedString, Integer.valueOf(numberOfUnlearnedCards));
        PaukerFrame.lazyStringChange(notLearnedLabel, string);

        string = MessageFormat.format(ustmString, Integer.valueOf(ustmCards));
        PaukerFrame.lazyStringChange(ustmLabel, string);

        string = MessageFormat.format(stmString, Integer.valueOf(stmCards));
        PaukerFrame.lazyStringChange(stmLabel, string);

        int numberOfLongTermBatches = lesson.getNumberOfLongTermBatches();
        int diff = numberOfLongTermBatches - lastNumberOfLongTermBatches;
        if (diff > 0) {
            // now the lesson has more long term batches than before
            // copy all old labels into new array
            JLabel newlabels[] = new JLabel[numberOfLongTermBatches];
            System.arraycopy(
                    longTermLabels, 0, newlabels, 0, longTermLabels.length);
            longTermLabels = newlabels;

            if (lastNumberOfLongTermBatches > 0) {
                // update constraints on old last long term label
                Component lastLabel = longTermBatchesPanel.getComponent(
                        lastNumberOfLongTermBatches - 1);
                layout.setConstraints(lastLabel, normalConstraints);
            }

            // add new long term batch labels
            for (int i = lastNumberOfLongTermBatches;
                    i < numberOfLongTermBatches; i++) {
                JLabel label = new JLabel();
                label.setFont(labelFont);
                if (i == (numberOfLongTermBatches - 1)) {
                    longTermBatchesPanel.add(label, lastConstraints);
                } else {
                    longTermBatchesPanel.add(label, normalConstraints);
                }
                longTermLabels[i] = label;
            }

            longTermBatchesPanel.validate();

        } else if (diff < 0) {
            // now the lesson has fewer long term batches than before
            // copy as much old labels into new array as possible
            JLabel newlabels[] = new JLabel[numberOfLongTermBatches];
            System.arraycopy(
                    longTermLabels, 0, newlabels, 0, numberOfLongTermBatches);
            longTermLabels = newlabels;

            // remove batch labels that are too much
            for (int i = lastNumberOfLongTermBatches - 1;
                    i >= numberOfLongTermBatches; i--) {
                longTermBatchesPanel.remove(i);
            }

            if (numberOfLongTermBatches > 0) {
                // update constraints on last label
                Component lastLabel = longTermBatchesPanel.getComponent(
                        numberOfLongTermBatches - 1);
                layout.setConstraints(lastLabel, lastConstraints);
            }

            longTermBatchesPanel.validate();
        }
        lastNumberOfLongTermBatches = numberOfLongTermBatches;

        // set batch text
        for (int i = 0; i < numberOfLongTermBatches; i++) {
            JLabel label = (JLabel) longTermBatchesPanel.getComponent(i);
            // simplify junit/jemmy tests by setting a name
            label.setName("longTermBatchLabel" + i);
            LongTermBatch longTermBatch = lesson.getLongTermBatch(i);
            numberOfCards = longTermBatch.getNumberOfCards();
            numberOfExpiredCards = longTermBatch.getNumberOfExpiredCards();
            string = MessageFormat.format(batchString, Integer.valueOf(i + 1),
                    Integer.valueOf(numberOfExpiredCards),
                    Integer.valueOf(numberOfCards - numberOfExpiredCards),
                    Integer.valueOf(numberOfCards));
            PaukerFrame.lazyStringChange(label, string);
        }

        repaint();
    }
    
    // Variablendeklaration - nicht modifizieren//GEN-BEGIN:variables
    private javax.swing.JLabel descriptionLabel;
    private javax.swing.JPanel longTermBatchesPanel;
    private javax.swing.JLabel notLearnedLabel;
    private javax.swing.JLabel stmLabel;
    private javax.swing.JLabel summaryLabel;
    private javax.swing.JLabel ustmLabel;
    // Ende der Variablendeklaration//GEN-END:variables
}
