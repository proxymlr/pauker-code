/*
 * OperatingSystem.java
 *
 * Created on 24.07.2010, 12:50:11
 */
package tools;

/**
 * An enum with known operating systems
 * @author Ronny Standtke <Ronny.Standtke@gmx.net>
 */
public enum OperatingSystem {

    /**
     * the Linux operating system
     */
    Linux,
    /**
     * the Mac OS X operating system
     */
    Mac_OS_X,
    /**
     * the Windows operating system
     */
    Windows,
    /**
     * an unknown operating system
     */
    Unknown
}
