
package tools;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import pauker.program.Card;
import pauker.program.CardSide;
import pauker.program.Lesson;

/**
 * A parser for KVTML files.
 * @author Ronny Standtke <Ronny.Standtke@gmx.net>
 */
public class KvtmlParser {

    private static final Logger logger =
            Logger.getLogger(KvtmlParser.class.getName());
    private static final ResourceBundle STRINGS =
            ResourceBundle.getBundle("pauker/Strings");

    /**
     * parses a KVTKL file
     * @param path the path to the KVTML file
     * @return a Pauker lesson
     * @throws FileNotFoundException if the file could not be found
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws IOException 
     */
    public static Lesson parseKvtmlFile(String path) throws
            FileNotFoundException, ParserConfigurationException, SAXException,
            IOException {

        // load document
        Document document = loadDocument(path);

        // small initial sanity check
        NodeList kvtmlNodelist = document.getElementsByTagName("kvtml");
        if (kvtmlNodelist.getLength() != 1) {
            logger.warning("can not find the kvtml node");
            return null;
        }

        // get kvtml attributes
        Node kvtmlNode = kvtmlNodelist.item(0);
        NamedNodeMap kvtmlAttributes = kvtmlNode.getAttributes();

        // check version
        String kvtmlVersion = getKvtmlVersion(kvtmlAttributes);

        if (kvtmlVersion == null) {
            logger.info(path + " has no kvtml version");
            return parseKvtml_0(kvtmlAttributes, document);

        } else if ("2.0".equals(kvtmlVersion)) {
            logger.info(path + " has kvtml version \"" + kvtmlVersion + '"');
            return parseKvtml_2_0(document);

        } else {
            logger.info(path + " has unknown kvtml version \"" + 
                    kvtmlVersion + "\", trying to parse kvtml 2.0");
            return parseKvtml_2_0(document);
        }
    }

    private static Document loadDocument(String path)
            throws IOException, ParserConfigurationException, SAXException {
        DocumentBuilderFactory documentBuilderFactory =
                DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setIgnoringComments(true);
        documentBuilderFactory.setIgnoringElementContentWhitespace(true);
        DocumentBuilder documentBuilder =
                documentBuilderFactory.newDocumentBuilder();
        documentBuilder.setEntityResolver(new EntityResolver() {

            public InputSource resolveEntity(String publicId, String systemId)
                    throws SAXException, java.io.IOException {
                return new InputSource(new ByteArrayInputStream(
                        "<?xml version='1.0' encoding='UTF-8'?>".getBytes()));
            }
        });

        InputStream inputStream = null;
        Document document = null;
        try {
            inputStream = new FileInputStream(path);
            document = documentBuilder.parse(inputStream);
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return document;
    }

    private static String getKvtmlVersion(NamedNodeMap kvtmlAttributes) {
        Node versionNode = kvtmlAttributes.getNamedItem("version");
        if (versionNode != null) {
            return versionNode.getNodeValue();
        }
        return null;
    }

    private static Lesson parseKvtml_0(
            NamedNodeMap kvtmlAttributes, Document document) {
        Lesson newLesson = new Lesson();

        // set lesson description
        String description = getLessonDescription_0(kvtmlAttributes);
        if (description != null) {
            newLesson.setDescription(description);
        }

        // convert lesson entries
        NodeList entryNodeList = document.getElementsByTagName("e");
        for (int i = 0, length = entryNodeList.getLength(); i < length; i++) {
            CardSide frontSide = null;
            CardSide reverseSide = null;

            Node entryNode = entryNodeList.item(i);
            NodeList entryChildNodes = entryNode.getChildNodes();
            for (int j = 0, length2 = entryChildNodes.getLength();
                    j < length2; j++) {
                Node entryChildNode = entryChildNodes.item(j);
                String entryChildNodeName = entryChildNode.getNodeName();
                if ("o".equals(entryChildNodeName)) {
                    frontSide = parseTextNode(entryChildNode);
                } else if ("t".equals(entryChildNodeName)) {
                    reverseSide = parseTextNode(entryChildNode);
                }
            }

            if ((frontSide != null) && (reverseSide != null)) {
                newLesson.addCard(new Card(frontSide, reverseSide));
            }
        }
        return newLesson;
    }

    private static String getLessonDescription_0(NamedNodeMap kvtmlAttributes) {
        String title = null;
        Node titleNode = kvtmlAttributes.getNamedItem("title");
        if (titleNode == null) {
            logger.info("kvtml attributes do not contain title");
        } else {
            title = titleNode.getNodeValue();
        }

        String generator = null;
        Node generatorNode = kvtmlAttributes.getNamedItem("generator");
        if (generatorNode == null) {
            logger.info("kvtml attributes do not contain generator");
        } else {
            generator = generatorNode.getNodeValue();
        }

        return combineDescription(title, generator);
    }

    private static String getLessonDescription_2_0(Document document) {
        String title = getSingleNodeText(document, "title");
        String generator = getSingleNodeText(document, "generator");
        return combineDescription(title, generator);
    }

    private static String combineDescription(String title, String generator) {
        String description = null;
        if (title != null) {
            description = STRINGS.getString("Title") + ": " + title;
        }
        if (generator != null) {
            if (description == null) {
                description = "";
            } else {
                description += '\n';
            }
            description += STRINGS.getString("Generator") + " " + generator;
        }
        return description;
    }

    private static Lesson parseKvtml_2_0(Document document) {
        Lesson newLesson = new Lesson();

        // set lesson description
        String description = getLessonDescription_2_0(document);
        if (description != null) {
            newLesson.setDescription(description);
        }

        // convert lesson entries
        NodeList entryNodeList = document.getElementsByTagName("entry");
        for (int i = 0, length = entryNodeList.getLength(); i < length; i++) {
            CardSide frontSide = null;
            CardSide reverseSide = null;

            Node entryNode = entryNodeList.item(i);
            NodeList entryChildNodes = entryNode.getChildNodes();
            for (int j = 0, length2 = entryChildNodes.getLength();
                    j < length2; j++) {
                Node entryChildNode = entryChildNodes.item(j);
                String entryChildNodeName = entryChildNode.getNodeName();
                if ("translation".equals(entryChildNodeName)) {
                    NamedNodeMap translationAttributes =
                            entryChildNode.getAttributes();
                    Node idAttribute = translationAttributes.getNamedItem("id");

                    if (idAttribute != null) {
                        String idString = idAttribute.getNodeValue();
                        if ("0".equals(idString)) {
                            frontSide = parseTranslationNode(entryChildNode);
                        } else if ("1".equals(idString)) {
                            reverseSide = parseTranslationNode(entryChildNode);
                        }
                    }
                }
            }

            if ((frontSide != null) && (reverseSide != null)) {
                newLesson.addCard(new Card(frontSide, reverseSide));
            }
        }
        return newLesson;
    }

    private static String getSingleNodeText(Document document, String tagName) {
        NodeList nodelist = document.getElementsByTagName(tagName);
        if (nodelist.getLength() > 0) {
            Node child = nodelist.item(0).getFirstChild();
            return child.getNodeValue();
        }
        return null;
    }

    private static CardSide parseTranslationNode(Node translationNode) {
        NodeList translationNodes = translationNode.getChildNodes();
        for (int i = 0, length = translationNodes.getLength();
                i < length; i++) {
            Node translationChildNode = translationNodes.item(i);
            String translationChildNodeName =
                    translationChildNode.getNodeName();
            if ("text".equals(translationChildNodeName)) {
                return parseTextNode(translationChildNode);
            }
        }
        return null;
    }

    private static CardSide parseTextNode(Node node) {
        Node firstChild = node.getFirstChild();
        if (firstChild != null) {
            String text = firstChild.getNodeValue();
            return new CardSide(text);
        }
        return null;
    }
}
