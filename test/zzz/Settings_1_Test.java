/*
 * Settings_1_Test.java
 *
 * Created on 6. Dezember 2006, 18:50
 *
 */
package zzz;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JCheckBoxOperator;
import org.netbeans.jemmy.operators.JComboBoxOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JSplitPaneOperator;
import org.netbeans.jemmy.operators.JTextAreaOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import pauker.program.gui.swing.PaukerFrame;
import pauker.program.gui.swing.Tools;
import screenshots.Screenshots;

/**
 * tests, if the splitpane position is remembered between Pauker starts
 * @author Ronny.Standtke@gmx.net
 */
public class Settings_1_Test extends TestCase {

    private static final Logger LOGGER =
            Logger.getLogger(Settings_1_Test.class.getName());

    /**
     * runs the test
     * @throws java.lang.Exception
     */
    public void testSettings() throws Exception {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        ResourceBundle strings = ResourceBundle.getBundle("pauker/Strings");

        // start pauker
        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication();
        JFrameOperator frameOperator = new JFrameOperator();
        final PaukerFrame paukerFrame = (PaukerFrame) frameOperator.getSource();

        // enlarge frame here a bit
        // (needed for next splitpane test in Settings_1_Test)
        Dimension size = frameOperator.getSize();
        frameOperator.setSize(
                (int) size.getWidth(), (int) size.getHeight() + 200);

        JSplitPaneOperator splitPaneOperator = new JSplitPaneOperator(
                frameOperator, new NameComponentChooser("splitPane"));
        JButtonOperator addCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("addCardButton"));
        JButtonOperator learnNewCardsButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("learnNewCardsButton"));
        JCheckBoxOperator showTimerCheckBoxOperator = new JCheckBoxOperator(
                frameOperator, new NameComponentChooser("showTimerCheckBox"));
        JButtonOperator cancelLearningButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("cancelLearningButton"));

        // move splitpane
        int minimumDividerLocation =
                splitPaneOperator.getMinimumDividerLocation();
        int maximumDividerLocation =
                splitPaneOperator.getMaximumDividerLocation();

        if (minimumDividerLocation == maximumDividerLocation) {
            fail("can not execute test when "
                    + "minimumDividerLocation == maximumDividerLocation");
        }

        // set divider to maximum location
        splitPaneOperator.setDividerLocation(maximumDividerLocation);

        // set some font properties
        addCardButtonOperator.pushNoBlock();
        JDialogOperator dialogOperator = new JDialogOperator();
        JTextAreaOperator frontSideTextAreaOperator = new JTextAreaOperator(
                dialogOperator, new NameComponentChooser("frontSideTextArea"));
        JTextAreaOperator reverseSideTextAreaOperator =
                new JTextAreaOperator(dialogOperator,
                new NameComponentChooser("reverseSideTextArea"));
        JComboBoxOperator repeatingMethodComboBoxOperator =
                new JComboBoxOperator(dialogOperator,
                new NameComponentChooser("repeatingMethodComboBox"));
        JCheckBoxOperator keepOpenCheckBoxOperator = new JCheckBoxOperator(
                dialogOperator, new NameComponentChooser("keepOpenCheckBox"));
        JButtonOperator okButtonOperator = new JButtonOperator(
                dialogOperator, new NameComponentChooser("okButton"));
        frontSideTextAreaOperator.setFont(
                new Font("DialogInput", Font.BOLD, 20));
        frontSideTextAreaOperator.setText("front");
        frontSideTextAreaOperator.setForeground(Color.BLUE);
        frontSideTextAreaOperator.setBackground(Color.YELLOW);
        reverseSideTextAreaOperator.setText("reverse");
        reverseSideTextAreaOperator.setFont(new Font("Dialog", Font.PLAIN, 12));
        reverseSideTextAreaOperator.setForeground(Color.BLACK);
        reverseSideTextAreaOperator.setBackground(Color.WHITE);
        repeatingMethodComboBoxOperator.setSelectedIndex(0);
        keepOpenCheckBoxOperator.setSelected(false);
        Tools.doClick(okButtonOperator);

        // disable timer (check in next test...)
        Tools.doClick(learnNewCardsButtonOperator);
        showTimerCheckBoxOperator.setSelected(false);
        Tools.doClick(cancelLearningButtonOperator);

        Thread.sleep(1000);
        // exiting via keypress does not work on mac OS X
        // therefore we use reflection here...
        final Method exitMethod =
                PaukerFrame.class.getDeclaredMethod("exitProgram");
        exitMethod.setAccessible(true);
        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                try {
                    exitMethod.invoke(paukerFrame);
                } catch (IllegalAccessException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                } catch (IllegalArgumentException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                } catch (InvocationTargetException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        });
        dialogOperator = new JDialogOperator();
        JButtonOperator dontSaveButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("DontSave"));
        Tools.doClick(dontSaveButtonOperator);

        Thread.sleep(1000);
    }
}
