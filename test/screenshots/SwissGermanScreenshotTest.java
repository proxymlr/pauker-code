/*
 * GermanScreenshotTest.java
 *
 * Created on 29. Oktober 2006, 08:41
 *
 */
package screenshots;

import java.util.Locale;
import junit.framework.TestCase;

/**
 *
 * @author Ronny.Standtke@gmx.net
 */
public class SwissGermanScreenshotTest extends TestCase {

    public void testDoScreenShots() throws Exception {

        assertTrue("Screenshots are not generated!", Screenshots.UPDATE_SCREENSHOTS);

        Screenshots.doScreenShots(new Locale("de", "CH"), "doc/docbook/de_CH/",
                400, 488, null,
                "weniger ist manchmal...", "mehr", "zu wenig", null,
                "test/testLessons/Laender-Hauptstaedte.xml.gz", 487, 489, null,
                "SchriftartDialog",
                null,
                null,
                "Sonstiges",
                "test/testLessons/Laender-Hauptstaedte.xml.gz", null);
    }
}
