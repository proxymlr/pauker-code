/*
 * EnglishScreenshotTest.java
 *
 * Created on 29. Oktober 2006, 08:41
 *
 */
package screenshots;

import java.util.Locale;
import junit.framework.TestCase;

/**
 *
 * @author Ronny.Standtke@gmx.net
 */
public class EnglishScreenshotTest extends TestCase {

    public void testDoScreenShots() throws Exception {

        assertTrue("Screenshots are not generated!", Screenshots.UPDATE_SCREENSHOTS);

        Screenshots.doScreenShots(Locale.ENGLISH, "doc/docbook/en/",
                350, 489, "MainWindow",
                "less is sometimes...", "more", "even less", "NewCardDialog",
                "test/testLessons/Currencies.xml.gz", 494, 489, "Learning",
                "FontDialog",
                "Strategies", "Times", "Misc",
                "test/testLessons/Currencies.xml.gz", "UltraShortermMemory");
    }
}
