/*
 * OpenLearnedUnlearnedTest.java
 *
 * Created on 5. August 2007, 09:55
 *
 */
package pauker.program.gui.swing;

import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JFrameOperator;
import pauker.program.Lesson;
import screenshots.Screenshots;

/**
 * here we test if lessons in old format are correctly opened
 * @author Ronny.Standtke@gmx.net
 */
public class OpenOldFormatTest extends TestCase {

    /**
     * runs the test
     * @throws Exception
     */
    public void testOpenLearnedUnlearned() throws Exception {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        // get references to test lessons
        String internalPath = "/testLessons/Lotti_Latein.xml.gz";
        String testFilePath = getClass().getResource(internalPath).getPath();
        testFilePath = testFilePath.replace("%20", " ");

        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication(new String[]{testFilePath});
        JFrameOperator frameOperator = new JFrameOperator();

        PaukerFrame paukerFrame = (PaukerFrame) frameOperator.getSource();
        Lesson lesson = paukerFrame.getCurrentLesson();
        assertFalse("parsing long term batches failed!",
                lesson.getNumberOfLongTermBatches() == 0);
    }
}
