/*
 * RemoveLongTermTest.java
 *
 * Created on 1. August 2007, 14:16
 *
 */
package pauker.program.gui.swing;

import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import pauker.program.Lesson;
import pauker.program.LongTermBatch;
import screenshots.Screenshots;

/**
 * here we test if removing a card from a longterm batch works like expected
 * @author Ronny.Standtke@gmx.net
 */
public class ForgetLongTermTest extends TestCase {

    public void testForget() throws Exception {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        // get path of test lesson
        String internalPath = "/testLessons/ForgetTest.pau.gz";
        String testFilePath = getClass().getResource(internalPath).getPath();
        testFilePath = testFilePath.replace("%20", " ");

        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication(new String[]{testFilePath});
        JFrameOperator frameOperator = new JFrameOperator();
        PaukerFrame paukerFrame = (PaukerFrame) frameOperator.getSource();
        JButtonOperator repeatCardsButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatCardsButton"));
        JButtonOperator showMeButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("showMeButton"));
        JButtonOperator repeatingNoButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatingNoButton"));

        // simulate that we forgot the card
        repeatCardsButtonOperator.doClick();
        showMeButtonOperator.doClick();
        repeatingNoButtonOperator.doClick();

        // check result
        Lesson currentLesson = paukerFrame.getCurrentLesson();
        int numberOfLongTermBatches =
                currentLesson.getNumberOfLongTermBatches();
        assertEquals(numberOfLongTermBatches, numberOfLongTermBatches);
        LongTermBatch longTermBatch = currentLesson.getLongTermBatch(0);
        int numberOfCards = longTermBatch.getNumberOfCards();
        assertEquals(1, numberOfCards);
        int numberOfExpiredCards = longTermBatch.getNumberOfExpiredCards();
        assertEquals(1, numberOfExpiredCards);
    }
}
