/*
 * BatchListCellRendererTest.java
 * JUnit based test
 *
 * Created on 1. August 2007, 19:11
 */
package pauker.program.gui.swing;

import java.awt.Color;
import java.lang.reflect.Field;
import javax.swing.JList;
import javax.swing.JTextField;
import junit.framework.TestCase;
import pauker.program.Card;
import pauker.program.CardSide;
import screenshots.Screenshots;

/**
 *
 * @author Ronny.Standtke@gmx.net
 */
public class BatchListCellRendererTest extends TestCase {

    /**
     * Test of getListCellRendererComponent, in class
     * pauker.program.gui.swing.BatchListCellRenderer.
     * @throws Exception
     */
    public void testGetListCellRendererComponent() throws Exception {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        final JList testList = new JList();
        final Color listBackground = testList.getBackground();
        final BatchListCellRenderer batchListCellRenderer =
                new BatchListCellRenderer(null, listBackground);
        final Field batchNumberField = 
                BatchListCellRenderer.class.getDeclaredField(
                "batchNumberTextField");
        batchNumberField.setAccessible(true);
        final JTextField batchNumberTextField =
                (JTextField) batchNumberField.get(batchListCellRenderer);
        final Color fieldBackground = batchListCellRenderer.getBackground();

        // test that the renderer paints the batch number color correctly
        final CardSide cardSide = new CardSide();
        final Card card = new Card(cardSide, cardSide);
        batchListCellRenderer.getListCellRendererComponent(
                testList, card, 0, false, false);
        assertEquals(PaukerFrame.RED.getRGB(),
                batchNumberTextField.getBackground().getRGB());
        card.setLearned(true);
        batchListCellRenderer.getListCellRendererComponent(
                testList, card, 0, false, false);
        assertEquals(fieldBackground.getRGB(),
                batchNumberTextField.getBackground().getRGB());
    }
}
