/*
 * OpenKvtml20Test.java
 *
 * Created on 27. Februar 2009, 17:55
 *
 */
package pauker.program.gui.swing;

import java.util.ResourceBundle;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JLabelOperator;
import org.netbeans.jemmy.operators.JListOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import pauker.program.Lesson;
import screenshots.Screenshots;

/**
 * this test verifies that KVTML files can be opened
 * @author Ronny.Standtke@gmx.net
 */
public class OpenKvtml20Test extends TestCase {

    private static final ResourceBundle STRINGS =
            ResourceBundle.getBundle("pauker/Strings");

    /**
     * runs the test
     * @throws java.lang.Exception
     */
    public void testOpenKvtml() throws Exception {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        // get references to test lessons
        String internalPath = "/testLessons/KennzeichenEuropa.kvtml";
        String testFilePath = getClass().getResource(internalPath).getPath();
        testFilePath = testFilePath.replace("%20", " ");

        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication(new String[]{testFilePath});
        JFrameOperator frameOperator = new JFrameOperator();
        JLabelOperator summaryLabelOperator = new JLabelOperator(
                frameOperator, new NameComponentChooser("summaryLabel"));
        JListOperator batchListOperator = new JListOperator(
                frameOperator, new NameComponentChooser("batchList"));

        // load summary
        Tools.clickComponentOperator(summaryLabelOperator);
        batchListOperator.waitComponentShowing(true);
        assertEquals(48, batchListOperator.getModel().getSize());

        // check description
        PaukerFrame paukerFrame = (PaukerFrame) frameOperator.getSource();
        Lesson currentLesson = paukerFrame.getCurrentLesson();
        String expectedDescription =
                STRINGS.getString("Title") + ": Kennzeichen Europa.kvtml\n"
                + STRINGS.getString("Generator") + " kwordquiz 0.8.9";
        assertEquals(expectedDescription, currentLesson.getDescription());
    }
}
