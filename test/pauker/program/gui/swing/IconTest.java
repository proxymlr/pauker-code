/*
 * IconTest.java
 *
 * Created on 8. Oktober 2006, 20:02
 *
 */
package pauker.program.gui.swing;

import java.lang.reflect.Field;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import junit.framework.TestCase;
import pauker.program.Card;
import pauker.program.CardSide;
import screenshots.Screenshots;

/**
 *
 * @author Ronny.Standtke@gmx.net
 */
public class IconTest extends TestCase {

    public void testIcon() throws Exception {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        // this are our icons
        Icon typingIcon = new ImageIcon(
                getClass().getResource("/pauker/icons/key_enter.png"));
        Icon brainIcon = new ImageIcon(
                getClass().getResource("/pauker/icons/smallbrain.png"));

        // start test environment
        PaukerFrame paukerFrame = new PaukerFrame();
        BatchListCellRenderer listCellRenderer = new BatchListCellRenderer(
                paukerFrame, (new JList()).getSelectionBackground());
        Field repeatByTypingLabelField =
                BatchListCellRenderer.class.getDeclaredField(
                "repeatByTypingLabel");
        repeatByTypingLabelField.setAccessible(true);
        JLabel repeatByTypingLabel =
                (JLabel) repeatByTypingLabelField.get(listCellRenderer);

        // check if correct icon is rendered
        Card card = new Card(new CardSide("1"), new CardSide("1"));
        card.setRepeatByTyping(true);
        listCellRenderer.getListCellRendererComponent(
                new JList(), card, 0, true, false);
        Icon icon = repeatByTypingLabel.getIcon();
        assertEquals(typingIcon.toString(), icon.toString());

        card.setRepeatByTyping(false);
        listCellRenderer.getListCellRendererComponent(
                new JList(), card, 0, true, false);
        icon = repeatByTypingLabel.getIcon();
        assertEquals(brainIcon.toString(), icon.toString());
    }
}
