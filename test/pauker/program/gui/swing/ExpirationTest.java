/*
 * OpenTest.java
 *
 * Created on 24. Juni 2006, 11:14
 *
 */
package pauker.program.gui.swing;

import java.lang.reflect.Field;
import java.util.ResourceBundle;
import javax.swing.JList;
import javax.swing.JTextField;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JLabelOperator;
import org.netbeans.jemmy.operators.JListOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import pauker.program.Card;
import pauker.program.Lesson;
import screenshots.Screenshots;

/**
 * tests expiration of cards
 * @author Ronny.Standtke@gmx.net
 */
public class ExpirationTest extends TestCase {

    /**
     * runs the test
     * @throws java.lang.Exception if an exception occurs
     */
    public void testExpiration() throws Exception {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        ResourceBundle strings = ResourceBundle.getBundle("pauker/Strings");

        // get path of test lesson
        String internalPath = "/testLessons/1.pau.gz";
        String testFilePath = getClass().getResource(internalPath).getPath();
        testFilePath = testFilePath.replace("%20", " ");

        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication(new String[]{testFilePath});
        JFrameOperator frameOperator = new JFrameOperator();
        JButtonOperator learnNewCardsButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("learnNewCardsButton"));
        JButtonOperator nextNewCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("nextNewCardButton"));
        JButtonOperator repeatUSTMButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatUSTMButton"));
        JButtonOperator showMeButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("showMeButton"));
        JButtonOperator repeatingYesButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatingYesButton"));
        JButtonOperator repeatSTMButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatSTMButton"));
        JButtonOperator transitionOKButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("transitionOKButton"));
        JListOperator batchListOperator = new JListOperator(
                frameOperator, new NameComponentChooser("batchList"));
        JButtonOperator forgetButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("forgetButton"));
        JLabelOperator summaryLabelOperator = new JLabelOperator(
                frameOperator, new NameComponentChooser("summaryLabel"));
        JButtonOperator instantRepeatButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("instantRepeatButton"));
        JButtonOperator repeatCardsButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatCardsButton"));
        JButtonOperator searchButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("searchButton"));

        // test if the card is correctly rendered
        PaukerFrame paukerFrame = (PaukerFrame) frameOperator.getSource();
        Lesson lesson = paukerFrame.getCurrentLesson();
        BatchListCellRenderer batchListCellRenderer = new BatchListCellRenderer(
                paukerFrame, (new JList()).getSelectionBackground());
        Field batchNumberField = BatchListCellRenderer.class.getDeclaredField(
                "batchNumberTextField");
        batchNumberField.setAccessible(true);
        JTextField batchNumberTextField =
                (JTextField) batchNumberField.get(batchListCellRenderer);
        JList testList = new JList();
        batchListCellRenderer.getListCellRendererComponent(
                testList, lesson.getCards().get(0), 0, true, false);
        assertEquals("batch number is not correctly rendered!",
                strings.getString("Not_learned"),
                batchNumberTextField.getText());

        // "learn" the one and only card
        Tools.doClick(learnNewCardsButtonOperator);
        Tools.doClick(nextNewCardButtonOperator);
        Tools.doClick(repeatUSTMButtonOperator);
        Tools.doClick(showMeButtonOperator);
        Tools.doClick(repeatingYesButtonOperator);
        Tools.doClick(repeatSTMButtonOperator);
        Tools.doClick(transitionOKButtonOperator);
        Tools.doClick(showMeButtonOperator);
        Tools.doClick(repeatingYesButtonOperator);

        // test that expiration formula is correct for first batch
        Card firstCard = lesson.getCards().get(0);
        long learnedTimestamp = firstCard.getLearnedTimestamp();
        long expirationTime = firstCard.getExpirationTime();
        long diff = expirationTime - learnedTimestamp;
        assertEquals(86400000, diff);

        // test manual expiration of card
        JLabelOperator firstLongTermBatchLabelOperator = new JLabelOperator(
                frameOperator, new NameComponentChooser("longTermBatchLabel0"));
        Tools.clickComponentOperator(firstLongTermBatchLabelOperator);
        batchListOperator.waitComponentShowing(true);
        assertTrue(batchListOperator.getModel().getSize() > 0);
        batchListOperator.setSelectedIndex(0);
        forgetButtonOperator.waitComponentEnabled();
        forgetButtonOperator.pushNoBlock();
        JDialogOperator dialogOperator = new JDialogOperator();
        JButtonOperator confirmButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("Move_Card_Back"));
        Tools.doClick(confirmButtonOperator);
        learnNewCardsButtonOperator.waitComponentEnabled();
        assertEquals(0, lesson.getNumberOfLongTermBatches());

        // test instant repeat
        Tools.clickComponentOperator(summaryLabelOperator);
        searchButtonOperator.waitComponentEnabled();
        assertTrue(batchListOperator.getModel().getSize() > 0);
        batchListOperator.setSelectedIndex(0);
        instantRepeatButtonOperator.waitComponentEnabled();
        instantRepeatButtonOperator.pushNoBlock();
        dialogOperator = new JDialogOperator();
        confirmButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("Repeat_Cards_Instantly"));
        Tools.doClick(confirmButtonOperator);
        repeatCardsButtonOperator.waitComponentEnabled();
        lesson = paukerFrame.getCurrentLesson();
        assertEquals(1, lesson.getNumberOfLongTermBatches());
        assertTrue(lesson.getCards().get(0).isLearned());
        assertFalse(learnNewCardsButtonOperator.isEnabled());
        assertTrue("loading the summary failed!",
                batchListOperator.getModel().getSize() > 0);

        // test repetition of expired card
        Tools.doClick(repeatCardsButtonOperator);
        showMeButtonOperator.waitComponentShowing(true);
        showMeButtonOperator.waitComponentEnabled();
        showMeButtonOperator.waitHasFocus();
        showMeButtonOperator.doClick();
        repeatingYesButtonOperator.waitComponentShowing(true);
        repeatingYesButtonOperator.waitComponentEnabled();
        repeatingYesButtonOperator.waitHasFocus();
        repeatingYesButtonOperator.doClick();
        lesson = paukerFrame.getCurrentLesson();
        assertTrue(lesson.getCards().get(0).isLearned());
        Tools.clickComponentOperator(summaryLabelOperator);
        batchListOperator.waitComponentShowing(true);
        assertTrue("loading the summary failed!",
                batchListOperator.getModel().getSize() > 0);

        // test if the batch number is correctly rendered
        batchListCellRenderer.getListCellRendererComponent(
                testList, lesson.getCards().get(0), 0, true, false);
        assertEquals("batch number is not correctly rendered!",
                "2", batchNumberTextField.getText());

        // test that card is now in batch nr.2
        assertEquals("card did not move to batch nr. 2!",
                2, paukerFrame.getCurrentLesson().getNumberOfLongTermBatches());

        // test that expiration formula is correct for second batch
        firstCard = lesson.getCards().get(0);
        learnedTimestamp = firstCard.getLearnedTimestamp();
        expirationTime = firstCard.getExpirationTime();
        diff = expirationTime - learnedTimestamp;
        assertEquals("Expiration formula is incorrect!",
                (long) (Math.E * 86400000), diff);

        // test expiration from summary
        batchListOperator.setSelectedIndex(0);
        forgetButtonOperator.waitComponentEnabled();
        forgetButtonOperator.pushNoBlock();
        dialogOperator = new JDialogOperator();
        confirmButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("Move_Card_Back"));
        Tools.doClick(confirmButtonOperator);
        assertTrue("expiration from summary removed card from summary!",
                batchListOperator.getModel().getSize() > 0);

        // test moving an expired card from the summary to the unlearned batch
        batchListOperator.setSelectedIndex(0);
        instantRepeatButtonOperator.waitComponentEnabled();
        instantRepeatButtonOperator.pushNoBlock();
        dialogOperator = new JDialogOperator();
        confirmButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("Repeat_Cards_Instantly"));
        Tools.doClick(confirmButtonOperator);
        repeatCardsButtonOperator.waitComponentEnabled();
        assertTrue("loading the summary failed!",
                batchListOperator.getModel().getSize() > 0);
        assertEquals("summary list selection was not restored when using "
                + "instant repeat!", 0, batchListOperator.getSelectedIndex());
        batchListOperator.setSelectedIndex(0);
        forgetButtonOperator.waitComponentEnabled();
        forgetButtonOperator.pushNoBlock();
        dialogOperator = new JDialogOperator();
        confirmButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("Move_Card_Back"));
        Tools.doClick(confirmButtonOperator);
        Thread.sleep(500);
        lesson = paukerFrame.getCurrentLesson();
        assertEquals("moving an expired card from the summary to the "
                + "unlearned batch failed!",
                1, lesson.getUnlearnedBatch().getNumberOfCards());
        assertEquals("list selection was not restored when moving cards back "
                + "to the unlearned batch!",
                0, batchListOperator.getSelectedIndex());
    }
}
