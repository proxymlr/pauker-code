/*
 * SortingTest.java
 *
 * Created on 29. Juli 2007, 08:25
 *
 */
package pauker.program.gui.swing;

import javax.swing.JMenuItem;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JLabelOperator;
import org.netbeans.jemmy.operators.JListOperator;
import org.netbeans.jemmy.operators.JMenuItemOperator;
import org.netbeans.jemmy.operators.JMenuOperator;
import org.netbeans.jemmy.operators.JPopupMenuOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import screenshots.Screenshots;

/**
 * there was a bug that the selection was not correctly restored when sorting
 * cards
 * here we test if the behaviour is correct
 *
 * @author Ronny.Standtke@gmx.net
 */
public class SortingTest extends TestCase {

    public void testSorting() throws Exception {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        // get path of test lesson
        String internalPath = "/testLessons/SortingTest.pau.gz";
        String testFilePath = getClass().getResource(internalPath).getPath();
        testFilePath = testFilePath.replace("%20", " ");

        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication(new String[]{testFilePath});
        JFrameOperator frameOperator = new JFrameOperator();
        JLabelOperator summaryLabelOperator = new JLabelOperator(
                frameOperator, new NameComponentChooser("summaryLabel"));
        JListOperator batchListOperator = new JListOperator(
                frameOperator, new NameComponentChooser("batchList"));
        JButtonOperator sortButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("sortButton"));

        // load summary
        Tools.clickComponentOperator(summaryLabelOperator);
        batchListOperator.waitComponentShowing(true);
        assertEquals(7, batchListOperator.getModel().getSize());

        // select first card
        batchListOperator.setSelectedIndex(0);

        // sort batch
        sortButtonOperator.waitComponentShowing(true);
        sortButtonOperator.waitComponentEnabled();
        sortButtonOperator.push();
        JPopupMenuOperator sortingPopupMenuOperator = new JPopupMenuOperator();
        JMenuOperator descendingMenuOperator =
                new JMenuOperator(sortingPopupMenuOperator,
                new NameComponentChooser("descendingMenu"));
        Tools.clickComponentOperator(descendingMenuOperator);
        JMenuItemOperator descendingFrontMenuItemOperator =
                new JMenuItemOperator(
                (JMenuItem) descendingMenuOperator.getMenuComponent(0));
        descendingFrontMenuItemOperator.waitComponentShowing(true);
        Tools.clickComponentOperator(descendingFrontMenuItemOperator);
        Thread.sleep(500);

        // check selection
        int[] selectedIndices = batchListOperator.getSelectedIndices();
        assertEquals("the number of selections changed after sorting the "
                + "batch!", 1, selectedIndices.length);
        final int NEW_CARD_INDEX = 6;
        assertEquals(NEW_CARD_INDEX, selectedIndices[0]);
        assertTrue("scrolling to selected cards after sorting a batch did not "
                + "work!",
                (batchListOperator.getFirstVisibleIndex() <= NEW_CARD_INDEX)
                && (batchListOperator.getLastVisibleIndex() >= NEW_CARD_INDEX));
    }
}
