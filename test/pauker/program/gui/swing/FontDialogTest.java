/*
 * FontDialogTest.java
 *
 * Created on 8. Oktober 2006, 11:03
 *
 */
package pauker.program.gui.swing;

import java.awt.Color;
import java.awt.Font;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JComboBoxOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JLabelOperator;
import org.netbeans.jemmy.operators.JListOperator;
import org.netbeans.jemmy.operators.JSplitPaneOperator;
import org.netbeans.jemmy.operators.JTextAreaOperator;
import org.netbeans.jemmy.operators.JToggleButtonOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import screenshots.Screenshots;

/**
 *
 * @author Ronny.Standtke@gmx.net
 */
public class FontDialogTest extends TestCase {

    private JButtonOperator fontButtonOperator;
    private JDialogOperator fontDialogOperator;

    public void testFontDialog() throws Exception {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        // get reference to test file
        String internalPath = "/testLessons/FontDialogTest.pau.gz";
        String testFilePath = getClass().getResource(internalPath).getPath();
        testFilePath = testFilePath.replace("%20", " ");

        // start pauker with test file
        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication(new String[]{testFilePath});
        JFrameOperator frameOperator = new JFrameOperator();
        JLabelOperator summaryLabelOperator = new JLabelOperator(
                frameOperator, new NameComponentChooser("summaryLabel"));
        JListOperator batchListOperator = new JListOperator(
                frameOperator, new NameComponentChooser("batchList"));
        fontButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("fontButton"));
        JSplitPaneOperator splitPaneOperator = new JSplitPaneOperator(
                frameOperator, new NameComponentChooser("splitPane"));

        if (splitPaneOperator.getDividerLocation() == 1) {
            splitPaneOperator.setDividerLocation(-1);
        }
        assertTrue(summaryLabelOperator.isShowing());
        Tools.clickComponentOperator(summaryLabelOperator);
        Thread.sleep(500);

        // test that loading the summary works
        assertEquals("The summary batch was not correctly loaded!",
                6, batchListOperator.getModel().getSize());

        // test dialog with several selections
        batchListOperator.setSelectedIndex(0);
        checkDialog(new Font("Monospaced", Font.PLAIN, 9),
                Color.BLACK, Color.WHITE);
        fontDialogOperator.close();

        batchListOperator.setSelectedIndices(new int[]{0, 1, 2});
        checkDialog(new Font("Dialog", Font.BOLD | Font.ITALIC, 10),
                Color.RED, Color.GREEN);
        fontDialogOperator.close();

        batchListOperator.setSelectedIndex(4);
        checkDialog(new Font("Dialog", Font.PLAIN, 13),
                Color.BLACK, Color.WHITE);
        fontDialogOperator.close();

        batchListOperator.setSelectedIndices(new int[]{4, 5});
        checkDialog(new Font("Dialog", Font.PLAIN, 13),
                Color.BLACK, Color.WHITE);
        fontDialogOperator.close();
    }

    private void checkDialog(Font expectedFont,
            Color expectedForeground, Color expectedBackground) {

        fontButtonOperator.pushNoBlock();
        fontDialogOperator = new JDialogOperator();
        JComboBoxOperator fontFamilyComboBoxOperator =
                new JComboBoxOperator(fontDialogOperator,
                new NameComponentChooser("fontFamilyComboBox"));
        JComboBoxOperator fontSizeComboBoxOperator =
                new JComboBoxOperator(fontDialogOperator,
                new NameComponentChooser("fontSizeComboBox"));
        JToggleButtonOperator boldToggleButtonOperator =
                new JToggleButtonOperator(fontDialogOperator,
                new NameComponentChooser("boldToggleButton"));
        JToggleButtonOperator italicToggleButtonOperator =
                new JToggleButtonOperator(fontDialogOperator,
                new NameComponentChooser("italicToggleButton"));
        JTextAreaOperator textAreaOperator = new JTextAreaOperator(
                fontDialogOperator, new NameComponentChooser("textArea"));

        // check font family
        String fontFamily =
                (String) fontFamilyComboBoxOperator.getSelectedItem();
        assertEquals(expectedFont.getFamily(), fontFamily);

        // check font size
        String fontSizeString =
                (String) fontSizeComboBoxOperator.getSelectedItem();
        int fontSizeInDialog = Integer.parseInt(fontSizeString);
        assertEquals(expectedFont.getSize(), fontSizeInDialog);

        // check font style
        assertEquals(expectedFont.isBold(),
                boldToggleButtonOperator.isSelected());
        assertEquals(expectedFont.isItalic(),
                italicToggleButtonOperator.isSelected());

        // check colors
        assertEquals(expectedForeground, textAreaOperator.getForeground());
        assertEquals(expectedBackground, textAreaOperator.getBackground());
    }
}
