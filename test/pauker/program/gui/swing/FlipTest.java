/*
 * FlipTest.java
 *
 * Created on 10. Juni 2007, 10:28
 *
 */
package pauker.program.gui.swing;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JComponentOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JListOperator;
import org.netbeans.jemmy.operators.JTextAreaOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import screenshots.Screenshots;

/**
 *
 * Tests that searching for similar cards works after flipping a lesson
 * (there was the following bug: similar cards where searched at the wrong card
 * side after flipping a lesson)
 *
 * @author Ronny.Standtke@gmx.net
 */
public class FlipTest extends TestCase {

    private static final Logger LOGGER =
            Logger.getLogger(FlipTest.class.getName());
    private PaukerFrame paukerFrame;

    /**
     * tests the flip method
     * @throws java.lang.Exception if an exception occurs
     */
    public void testFlip() throws Exception {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        ResourceBundle strings = ResourceBundle.getBundle("pauker/Strings");

        // get path of test lesson
        String internalPath = "/testLessons/Laender-Hauptstaedte.xml.gz";
        String testFilePath = getClass().getResource(internalPath).getPath();
        testFilePath = testFilePath.replace("%20", " ");

        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication(new String[]{testFilePath});
        JFrameOperator frameOperator = new JFrameOperator();
        paukerFrame = (PaukerFrame) frameOperator.getSource();
        JButtonOperator addCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("addCardButton"));
        JComponentOperator aboutPanelOperator = new JComponentOperator(
                frameOperator, new NameComponentChooser("aboutPanel"));

        // flip the lesson
        flip();
        JDialogOperator dialogOperator = new JDialogOperator();
        JButtonOperator flipButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("Flip_Card_Sides"));
        Tools.doClick(flipButtonOperator);

        // test, if the lesson description is still visible after flipping
        // (loading the summary batch after flipping the lesson was a waste
        // and not consistent with loading a lesson)
        Thread.sleep(1000);
        assertTrue("batch was loaded after flipping",
                aboutPanelOperator.isShowing());

        // test if similar card search still works
        addCardButtonOperator.pushNoBlock();
        dialogOperator = new JDialogOperator();
        JTextAreaOperator frontSideTextAreaOperator = new JTextAreaOperator(
                dialogOperator, new NameComponentChooser("frontSideTextArea"));
        JListOperator similarCardsListOperator = new JListOperator(
                dialogOperator, new NameComponentChooser("similarCardsList"));
        frontSideTextAreaOperator.setText("Berlin");
        assertEquals(1, similarCardsListOperator.getModel().getSize());
    }

    private void flip() throws NoSuchMethodException {
        // (pushing menu items does not seem to work on Mac OS X, therefore we
        // use reflection to call the correct method instead of using jemmy)
        final Method flipMethod = PaukerFrame.class.getDeclaredMethod(
                "flipCardSidesMenuItemActionPerformed", ActionEvent.class);
        flipMethod.setAccessible(true);
        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                try {
                    flipMethod.invoke(paukerFrame, (ActionEvent) null);
                } catch (IllegalAccessException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                } catch (IllegalArgumentException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                } catch (InvocationTargetException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        });
    }
}
