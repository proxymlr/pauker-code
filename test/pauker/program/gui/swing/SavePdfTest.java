/*
 * SavePdfTest.java
 *
 * Created on 27.09.2009, 17:51:57
 *
 */

package pauker.program.gui.swing;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.filechooser.FileFilter;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JCheckBoxOperator;
import org.netbeans.jemmy.operators.JComboBoxOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFileChooserOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JTextAreaOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import screenshots.Screenshots;

/**
 * A test that checks that saving a lesson as pdf does not break normal save
 * operations (see Bug#2839046).
 * @author Ronny Standtke <Ronny.Standtke@gmx.net>
 */
public class SavePdfTest extends TestCase {

    private static final Logger LOGGER =
            Logger.getLogger(SavePdfTest.class.getName());
    private JFrameOperator frameOperator;
    private PaukerFrame paukerFrame;
    private final ResourceBundle STRINGS =
            ResourceBundle.getBundle("pauker/Strings");
    private JButtonOperator addCardButtonOperator;

    /**
     * runs the test
     * @throws java.lang.Exception
     */
    public void testSavePdf() throws Exception {
        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        // start pauker
        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication();
        frameOperator = new JFrameOperator();
        paukerFrame = (PaukerFrame) frameOperator.getSource();
        addCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("addCardButton"));
        JButtonOperator saveButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("saveButton"));

        // add a card
        addCard("1f", "1r");

        // try saving as temporary file
        File tmpFile = File.createTempFile("pauker_save_test", ".pau.gz");
        tmpFile.delete();
        saveFile(tmpFile);

        // now save as PDF
        savePdfFile();

        // add a second card
        addCard("2f", "2r");

        // save lesson again via toolbar button
        Tools.doClick(saveButtonOperator);

        // save again as PDF
        savePdfFile();

        // add a third card
        addCard("3f", "3r");

        // save lesson via menu item
        // because pushing menu items via jemmy on Mac OS X does not work
        // we use reflection here
        final Method saveMethod = PaukerFrame.class.getDeclaredMethod(
                "saveMenuItemActionPerformed", ActionEvent.class);
        saveMethod.setAccessible(true);
        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                try {
                    saveMethod.invoke(paukerFrame, (ActionEvent) null);
                } catch (IllegalAccessException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                } catch (IllegalArgumentException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                } catch (InvocationTargetException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        });
        Thread.sleep(1000);
        assertFalse("saving after PDF export failed",
                saveButtonOperator.isEnabled());
    }

    private void addCard(String frontSideText, String reverseSideText)
            throws InterruptedException {
        addCardButtonOperator.pushNoBlock();
        JDialogOperator newDialogOperator = new JDialogOperator();
        JTextAreaOperator newFrontSideTextAreaOperator =
                new JTextAreaOperator(newDialogOperator,
                new NameComponentChooser("frontSideTextArea"));
        JTextAreaOperator reverseSideTextAreaOperator =
                new JTextAreaOperator(newDialogOperator,
                new NameComponentChooser("reverseSideTextArea"));
        JComboBoxOperator repeatingMethodComboBoxOperator =
                new JComboBoxOperator(newDialogOperator,
                new NameComponentChooser("repeatingMethodComboBox"));
        JCheckBoxOperator keepOpenCheckBoxOperator =
                new JCheckBoxOperator(newDialogOperator,
                new NameComponentChooser("keepOpenCheckBox"));
        JButtonOperator okButtonOperator = new JButtonOperator(
                newDialogOperator, new NameComponentChooser("okButton"));
        newFrontSideTextAreaOperator.setText(frontSideText);
        reverseSideTextAreaOperator.setText(reverseSideText);
        repeatingMethodComboBoxOperator.setSelectedIndex(1);
        keepOpenCheckBoxOperator.setSelected(false);
        Tools.doClick(okButtonOperator);
    }

    private void saveFile(File file) throws IOException {
        JFileChooserOperator fileChooserOperator = saveAs();
        fileChooserOperator.setSelectedFile(file);
        fileChooserOperator.approveSelection();
    }

    private void savePdfFile() throws IOException {
        JFileChooserOperator fileChooserOperator = saveAs();
        FileFilter[] fileFilters =
                fileChooserOperator.getChoosableFileFilters();
        fileChooserOperator.setFileFilter(fileFilters[3]);
        File tmpPdfFile = File.createTempFile("pauker_save_test", ".pdf");
        fileChooserOperator.setSelectedFile(tmpPdfFile);
        overwrite(fileChooserOperator);
        JDialogOperator saveToPdfDialogOperator = new JDialogOperator();
        JButtonOperator okButtonOperator = new JButtonOperator(
                saveToPdfDialogOperator, STRINGS.getString("OK"));
        okButtonOperator.push();
        saveToPdfDialogOperator.waitComponentVisible(false);
    }

    private JFileChooserOperator saveAs() {
        try {
            // because pushing menu items via jemmy does not work on Mac OS X
            // we use reflection here
            final Method saveMethod = PaukerFrame.class.getDeclaredMethod(
                    "saveFileAs");
            saveMethod.setAccessible(true);
            EventQueue.invokeLater(new Runnable() {

                @Override
                public void run() {
                    try {
                        saveMethod.invoke(paukerFrame);
                    } catch (IllegalAccessException ex) {
                        LOGGER.log(Level.SEVERE, null, ex);
                    } catch (IllegalArgumentException ex) {
                        LOGGER.log(Level.SEVERE, null, ex);
                    } catch (InvocationTargetException ex) {
                        LOGGER.log(Level.SEVERE, null, ex);
                    }
                }
            });
        } catch (NoSuchMethodException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            LOGGER.log(Level.SEVERE, null, ex);
        }
        return new JFileChooserOperator();
    }

    private void overwrite(JFileChooserOperator fileChooserOperator) {
        fileChooserOperator.approveSelection();
        JDialogOperator overwriteDialogOperator = new JDialogOperator();
        JButtonOperator overwriteButtonOperator = new JButtonOperator(
                overwriteDialogOperator, STRINGS.getString("Overwrite"));
        overwriteButtonOperator.push();
        overwriteDialogOperator.waitComponentVisible(false);
    }
}
