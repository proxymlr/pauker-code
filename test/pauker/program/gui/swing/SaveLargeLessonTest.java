/*
 * SaveLargeLessonTest.java
 *
 * Created on 15.11.2008, 14:56:47
 *
 */
package pauker.program.gui.swing;

import java.io.File;
import java.util.ResourceBundle;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JCheckBoxOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFileChooserOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JMenuItemOperator;
import org.netbeans.jemmy.operators.JMenuOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import screenshots.Screenshots;

/**
 * A test that checks that saving large lessons is possible without throwing
 * an OOM exception.
 * (This was once the case...)
 * @author Ronny Standtke <Ronny.Standtke@gmx.net>
 */
public class SaveLargeLessonTest extends TestCase {

    private JFrameOperator frameOperator;
    private JMenuOperator fileMenuOperator;
    private final ResourceBundle STRINGS =
            ResourceBundle.getBundle("pauker/Strings");

    /**
     * runs the test
     * @throws java.lang.Exception
     */
    public void testSaveLargeLesson() throws Exception {

        // this test does no longer work with newer JVMs
        // but we keep it for reference reasons...
        if (true) {
            return;
        }

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        // start pauker
        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication();
        frameOperator = new JFrameOperator();
        fileMenuOperator = new JMenuOperator(
                frameOperator, new NameComponentChooser("fileMenu"));

        // open first lesson
        String internalPath = "/testLessons/English.pau.gz";
        String testFilePath = getClass().getResource(internalPath).getPath();
        testFilePath = testFilePath.replace("%20", " ");
        File testFile = new File(testFilePath);
        openFile(testFile, false);

        // check, how many cards are in the lesson
        PaukerFrame paukerFrame = (PaukerFrame) frameOperator.getSource();
        int cardsInFirstLesson =
                paukerFrame.getCurrentLesson().getNumberOfCards();

        // try saving as temporary file
        File tmpFile = File.createTempFile("oom_pauker_test", ".pau.gz");
        saveFile(tmpFile);

        // open second lesson
        internalPath = "/testLessons/Lingua.pau.gz";
        testFilePath = getClass().getResource(internalPath).getPath();
        testFilePath = testFilePath.replace("%20", " ");
        testFile = new File(testFilePath);
        openFile(testFile, true);

        // try saving again
        saveFile(tmpFile);

        // a dialog regarding OutOfMemoryError must be shown
        JDialogOperator hintDialogOperator = new JDialogOperator();
        JButtonOperator okButtonOperator = new JButtonOperator(
                hintDialogOperator, STRINGS.getString("OK"));
        okButtonOperator.push();

        // press don't save button
        JButtonOperator newButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("newButton"));
        newButtonOperator.pushNoBlock();
        JDialogOperator questionDialogOperator = new JDialogOperator();
        JButtonOperator dontSaveButtonOperator = new JButtonOperator(
                questionDialogOperator, STRINGS.getString("DontSave"));
        dontSaveButtonOperator.push();

        // try opening the tmp file
        openFile(tmpFile, false);

        // check number of cards in loaded file
        assertEquals("OOM error while saving destroyed lesson file!",
                cardsInFirstLesson,
                paukerFrame.getCurrentLesson().getNumberOfCards());
    }

    private void openFile(File file, boolean merge) {
        fileMenuOperator.doClick();
        JMenuItemOperator openMenuItemOperator = new JMenuItemOperator(
                frameOperator, new NameComponentChooser("openMenuItem"));
        openMenuItemOperator.push();
        JDialogOperator openFileDialogOperator = new JDialogOperator();
        JFileChooserOperator fileChooserOperator = new JFileChooserOperator();
        fileChooserOperator.setSelectedFile(file);
        JButtonOperator openButtonOperator = new JButtonOperator(
                openFileDialogOperator, new NameComponentChooser("openButton"));
        JCheckBoxOperator mergingCheckBoxOperator = new JCheckBoxOperator(
                openFileDialogOperator,
                new NameComponentChooser("mergingCheckBox"));
        mergingCheckBoxOperator.setSelected(merge);
        openButtonOperator.doClick();
    }

    private void saveFile(File file) {
        fileMenuOperator.doClick();
        JMenuItemOperator saveAsMenuItemOperator = new JMenuItemOperator(
                frameOperator, new NameComponentChooser("saveAsMenuItem"));
        saveAsMenuItemOperator.push();
        JFileChooserOperator fileChooserOperator = new JFileChooserOperator();
        fileChooserOperator.setSelectedFile(file);
        fileChooserOperator.approveSelection();
        JDialogOperator overwriteDialogOperator = new JDialogOperator();
        JButtonOperator overwriteButtonOperator = new JButtonOperator(
                overwriteDialogOperator, STRINGS.getString("Overwrite"));
        overwriteButtonOperator.push();
        overwriteDialogOperator.waitComponentVisible(false);
    }
}
