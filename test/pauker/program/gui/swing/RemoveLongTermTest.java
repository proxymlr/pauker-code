/*
 * RemoveLongTermTest.java
 *
 * Created on 1. August 2007, 14:16
 *
 */
package pauker.program.gui.swing;

import java.util.ResourceBundle;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JLabelOperator;
import org.netbeans.jemmy.operators.JListOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import pauker.program.Card;
import screenshots.Screenshots;

/**
 * here we test if removing a card from a longterm batch works like expected
 * @author Ronny.Standtke@gmx.net
 */
public class RemoveLongTermTest extends TestCase {

    public void testRemove() throws Exception {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        ResourceBundle strings = ResourceBundle.getBundle("pauker/Strings");

        // get path of test lesson
        String internalPath = "/testLessons/RemoveTest.xml.gz";
        String testFilePath = getClass().getResource(internalPath).getPath();
        testFilePath = testFilePath.replace("%20", " ");

        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication(new String[]{testFilePath});
        JFrameOperator frameOperator = new JFrameOperator();
        JListOperator batchListOperator = new JListOperator(
                frameOperator, new NameComponentChooser("batchList"));
        JButtonOperator removeCardButton = new JButtonOperator(
                frameOperator, new NameComponentChooser("removeCardButton"));
        JLabelOperator firstLongTermBatchLabelOperator = new JLabelOperator(
                frameOperator, new NameComponentChooser("longTermBatchLabel0"));
        JLabelOperator summaryLabelOperator = new JLabelOperator(
                frameOperator, new NameComponentChooser("summaryLabel"));

        BatchListModel batchListModel =
                (BatchListModel) batchListOperator.getModel();

        // load first longterm batch
        Tools.clickComponentOperator(firstLongTermBatchLabelOperator);
        Thread.sleep(500);
        assertTrue(batchListModel.getSize() > 0);
        batchListOperator.setSelectedIndex(0);

        // remove first card
        Card firstCard = (Card) batchListOperator.getModel().getElementAt(0);
        removeCardButton.waitComponentShowing(true);
        removeCardButton.waitComponentEnabled();
        removeCardButton.pushNoBlock();
        JDialogOperator dialogOperator = new JDialogOperator();
        JButtonOperator confirmButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("Remove_Card"));
        Tools.doClick(confirmButtonOperator);

        // check, if card is still in summary batch
        Tools.clickComponentOperator(summaryLabelOperator);
        Thread.sleep(500);
        boolean cardInSummary = false;
        for (int i = 0, size = batchListModel.getSize(); i < size; i++) {
            if (batchListModel.get(i) == firstCard) {
                cardInSummary = true;
                break;
            }
        }
        assertFalse("removed card is still in summary!",
                cardInSummary);

        // test if the grammar is correct when removing several cards
        batchListOperator.setSelectedIndices(new int[]{0, 1});
        removeCardButton.waitComponentShowing(true);
        removeCardButton.waitComponentEnabled();
        removeCardButton.pushNoBlock();
        dialogOperator = new JDialogOperator();
        new JButtonOperator(dialogOperator, strings.getString("Remove_Cards"));
        // if the grammar is false, Jemmy will throw an exception because
        // it can't fine the button...
    }
}
