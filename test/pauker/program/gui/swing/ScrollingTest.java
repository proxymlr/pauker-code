/*
 * ScrollingTest.java
 *
 * Created on 08.03.2008, 14:16:18
 *
 */

package pauker.program.gui.swing;

import javax.swing.JScrollBar;
import javax.swing.JSplitPane;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JCheckBoxOperator;
import org.netbeans.jemmy.operators.JComboBoxOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JScrollPaneOperator;
import org.netbeans.jemmy.operators.JSplitPaneOperator;
import org.netbeans.jemmy.operators.JTextAreaOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import screenshots.Screenshots;

/**
 * Test against Bug#1901578: Scrolling when Split Horizontally Unintuitive
 * @author Ronny Standtke <Ronny.Standtke@gmx.net>
 */
public class ScrollingTest extends TestCase {

    /**
     * tests against Bug#1901578
     * @throws java.lang.Exception
     */
    public void testScrolling() throws Exception {
        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);
        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication();
        JFrameOperator frameOperator = new JFrameOperator();
        JButtonOperator addCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("addCardButton"));
        JButtonOperator learnNewCardsButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("learnNewCardsButton"));
        JScrollPaneOperator ustmFrontSideScrollPaneOperator =
                new JScrollPaneOperator(frameOperator,
                new NameComponentChooser("ustmFrontSideScrollPane"));
        JScrollPaneOperator ustmReverseSideScrollPaneOperator =
                new JScrollPaneOperator(frameOperator,
                new NameComponentChooser("ustmReverseSideScrollPane"));
        JButtonOperator nextNewCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("nextNewCardButton"));
        JButtonOperator repeatUSTMButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatUSTMButton"));
        JScrollPaneOperator repeatingFrontSideScrollPaneOperator =
                new JScrollPaneOperator(frameOperator,
                new NameComponentChooser("repeatingFrontSideScrollPane"));
        JButtonOperator showMeButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("showMeButton"));
        JScrollPaneOperator repeatingReverseSideScrollPaneOperator =
                new JScrollPaneOperator(frameOperator,
                new NameComponentChooser("repeatingReverseSideScrollPane"));

        // add a single card
        addCardButtonOperator.pushNoBlock();
        JDialogOperator dialogOperator = new JDialogOperator();
        JSplitPaneOperator splitPaneOperator = new JSplitPaneOperator(
                dialogOperator, new NameComponentChooser("splitPane"));
        JButtonOperator switchLayoutButtonOperator = new JButtonOperator(
                dialogOperator, new NameComponentChooser("switchLayoutButton"));
        JTextAreaOperator frontSideTextAreaOperator = new JTextAreaOperator(
                dialogOperator, new NameComponentChooser("frontSideTextArea"));
        JTextAreaOperator reverseSideTextAreaOperator = new JTextAreaOperator(
                dialogOperator,
                new NameComponentChooser("reverseSideTextArea"));
        JComboBoxOperator repeatingMethodComboBoxOperator = 
                new JComboBoxOperator(dialogOperator,
                new NameComponentChooser("repeatingMethodComboBox"));
        JCheckBoxOperator keepOpenCheckBoxOperator = new JCheckBoxOperator(
                dialogOperator, new NameComponentChooser("keepOpenCheckBox"));
        JButtonOperator okButtonOperator = new JButtonOperator(
                dialogOperator, new NameComponentChooser("okButton"));

        if (splitPaneOperator.getOrientation() == JSplitPane.HORIZONTAL_SPLIT) {
            switchLayoutButtonOperator.doClick();
        }
        frontSideTextAreaOperator.setText("1\n2\n3\n4\n5");
        reverseSideTextAreaOperator.setText("5\n4\n3\n2\n1");
        repeatingMethodComboBoxOperator.setSelectedIndex(1);
        keepOpenCheckBoxOperator.setSelected(false);
        okButtonOperator.doClick();

        // start learning this card
        learnNewCardsButtonOperator.doClick();

        // where is the scrollbar?
        JScrollBar scrollBar =
                ustmFrontSideScrollPaneOperator.getVerticalScrollBar();
        assertEquals("the text in the USTM front side text field was not " +
                "scrolled to top!", 0, scrollBar.getValue());
        scrollBar = ustmReverseSideScrollPaneOperator.getVerticalScrollBar();
        assertEquals("the text in the USTM reverse side text field was not " +
                "scrolled to top!", 0, scrollBar.getValue());

        Tools.doClick(nextNewCardButtonOperator);
        Tools.doClick(repeatUSTMButtonOperator);

        scrollBar = repeatingFrontSideScrollPaneOperator.getVerticalScrollBar();
        assertEquals("the text in the STM front side text field was not " +
                "scrolled to top!", 0, scrollBar.getValue());

        Tools.doClick(showMeButtonOperator);

        scrollBar =
                repeatingReverseSideScrollPaneOperator.getVerticalScrollBar();
        assertEquals("the text in the STM reverse side text field was not " +
                "scrolled to top!", 0, scrollBar.getValue());
    }
}
