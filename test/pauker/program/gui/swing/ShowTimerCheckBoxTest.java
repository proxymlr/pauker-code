/*
 * ShowTimerCheckBoxTest.java
 *
 * Created on 27.09.2009, 13:55:15
 *
 */
package pauker.program.gui.swing;

import java.io.File;
import java.util.prefs.Preferences;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JComponentOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import screenshots.Screenshots;

/**
 * Test against Bug#2826311: Strange stopwatch and play/stop behavior
 *
 * @author Ronny Standtke <Ronny.Standtke@gmx.net>
 */
public class ShowTimerCheckBoxTest extends TestCase {

    /**
     *
     * @throws Exception
     */
    public void testScrolling() throws Exception {
        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        // get path of test lesson
        String internalPath = "/testLessons/1.pau.gz";
        String testFilePath = getClass().getResource(internalPath).getPath();
        testFilePath = testFilePath.replace("%20", " ");

        // set preferences to not show timer
        Preferences preferences =
                Preferences.userRoot().node("/org/pauker");
        preferences.putBoolean("SHOW_TIMER", false);

        // start program
        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication(new String[]{testFilePath});
        JFrameOperator frameOperator = new JFrameOperator();
        JButtonOperator learnNewCardsButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("learnNewCardsButton"));

        // start learning
        Tools.doClick(learnNewCardsButtonOperator);

        // check if the noTimerPanel is showing
        JComponentOperator noTimerPanelOperator = new JComponentOperator(
                frameOperator, new NameComponentChooser("noTimerPanel"));
        noTimerPanelOperator.waitComponentShowing(true);
    }
}
