/*
 * OpenTest.java
 *
 * Created on 24. Juni 2006, 11:14
 *
 */
package pauker.program.gui.swing;

import java.awt.EventQueue;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.MessageFormat;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JMenuItem;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.ContainerOperator;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JCheckBoxOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFileChooserOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JLabelOperator;
import org.netbeans.jemmy.operators.JMenuItemOperator;
import org.netbeans.jemmy.operators.JMenuOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import screenshots.Screenshots;

/**
 *
 * @author Ronny.Standtke@gmx.net
 */
public class OpenXmlTest extends TestCase {

    private static final Logger LOGGER =
            Logger.getLogger(OpenXmlTest.class.getName());
    private PaukerFrame paukerFrame;

    /**
     * runs the test
     * @throws Exception
     */
    public void testOpen() throws Exception {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        ResourceBundle strings = ResourceBundle.getBundle("pauker/Strings");

        // get references to test lessons
        String internalPath = "/testLessons/1.xml.gz";
        String testFilePath = getClass().getResource(internalPath).getPath();
        testFilePath = testFilePath.replace("%20", " ");
        final File testFile1 = new File(testFilePath);
        internalPath = "/testLessons/2.pau.gz";
        testFilePath = getClass().getResource(internalPath).getPath();
        testFilePath = testFilePath.replace("%20", " ");
        final File testFile2 = new File(testFilePath);

        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication(new String[]{testFilePath});
        JFrameOperator frameOperator = new JFrameOperator();
        paukerFrame = (PaukerFrame) frameOperator.getSource();
        JButtonOperator newButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("newButton"));
        JLabelOperator summaryLabelOperator = new JLabelOperator(
                frameOperator, new NameComponentChooser("summaryLabel"));
        JMenuOperator fileMenuOperator = new JMenuOperator(
                frameOperator, new NameComponentChooser("fileMenu"));

        // check, if command line open was successfull
        String summaryString = strings.getString("SummaryLabel");
        String formattedSummary = MessageFormat.format(summaryString,
                Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(1));
        assertEquals(formattedSummary, summaryLabelOperator.getText());


        Tools.doClick(newButtonOperator);


        // test opening via menu item
        open();
        JDialogOperator openFileDialogOperator = new JDialogOperator();
        JFileChooserOperator fileChooserOperator = new JFileChooserOperator();
        fileChooserOperator.setSelectedFile(testFile1);
        JButtonOperator openButtonOperator = new JButtonOperator(
                openFileDialogOperator, new NameComponentChooser("openButton"));
        Tools.doClick(openButtonOperator);
        assertEquals(formattedSummary, summaryLabelOperator.getText());

        // test merging of lessons
        open();
        openFileDialogOperator = new JDialogOperator();
        final JFileChooserOperator fileChooserOperator2 =
                new JFileChooserOperator(openFileDialogOperator);
        JCheckBoxOperator mergingCheckBoxOperator = new JCheckBoxOperator(
                openFileDialogOperator,
                new NameComponentChooser("mergingCheckBox"));
        mergingCheckBoxOperator.setSelected(true);
        final File[] selectedFiles = new File[]{testFile1, testFile2};
        while (fileChooserOperator2.getSelectedFiles().length
                != selectedFiles.length) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                LOGGER.log(Level.SEVERE, null, ex);
            }
            EventQueue.invokeLater(new Runnable() {

                @Override
                public void run() {
                    fileChooserOperator2.setSelectedFiles(selectedFiles);
                }
            });
        }
        openButtonOperator = new JButtonOperator(
                openFileDialogOperator, new NameComponentChooser("openButton"));
        Tools.doClick(openButtonOperator);
        formattedSummary = MessageFormat.format(summaryString,
                Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(3));
        assertEquals(formattedSummary, summaryLabelOperator.getText());


        // test recent file menu
        fileMenuOperator.doClick();
        JMenuOperator recentFilesMenuOperator = new JMenuOperator(
                new ContainerOperator(fileMenuOperator.getPopupMenu()),
                new NameComponentChooser("recentFilesMenu"));
        recentFilesMenuOperator.doClick();
        JMenuItemOperator recentFilesMenuItem1Operator = new JMenuItemOperator(
                (JMenuItem) recentFilesMenuOperator.getMenuComponent(0));

        recentFilesMenuItem1Operator.pushNoBlock();
        JDialogOperator dialogOperator = new JDialogOperator();
        JButtonOperator dontSaveButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("DontSave"));
        Tools.doClick(dontSaveButtonOperator);

        formattedSummary = MessageFormat.format(summaryString,
                Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(1));
        assertEquals(formattedSummary, summaryLabelOperator.getText());
    }

    private void open() throws Exception {
        // simulating menu item operations does not work on Mac OS X
        // therefore we use reflection instead of jemmy to bring up the
        // file open dialog
        final Method openMethod =
                PaukerFrame.class.getDeclaredMethod("openAction");
        openMethod.setAccessible(true);
        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                try {
                    openMethod.invoke(paukerFrame);
                } catch (IllegalAccessException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                } catch (IllegalArgumentException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                } catch (InvocationTargetException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        });
    }
}
