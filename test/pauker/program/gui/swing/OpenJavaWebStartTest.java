/*
 * OpenJavaWebStartTest.java
 *
 * Created on 1. August 2007, 22:22
 *
 */
package pauker.program.gui.swing;

import java.lang.reflect.InvocationTargetException;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JFrameOperator;
import pauker.program.Lesson;
import screenshots.Screenshots;

/**
 * here we test if Pauker can handle the command line argument "-open" that is
 * used in Java Web Start
 * @author Ronny.Standtke@gmx.net
 */
public class OpenJavaWebStartTest extends TestCase {

    public void testOpen() throws Exception {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        Thread thread = new Thread() {
            // must put this into a thread because the error dialog (if any) is
            // shown in this thread while running startApplication()

            @Override
            public void run() {
                String internalPath = "/testLessons/1.pau.gz";
                String testFilePath =
                        getClass().getResource(internalPath).getPath();
                testFilePath = testFilePath.replace("%20", " ");
                try {
                    ClassReference classReference = new ClassReference(
                            "pauker.program.gui.swing.PaukerFrame");
                    classReference.startApplication(
                            new String[]{"-open", testFilePath});
                } catch (ClassNotFoundException ex) {
                    ex.printStackTrace();
                } catch (InvocationTargetException ex) {
                    ex.printStackTrace();
                } catch (NoSuchMethodException ex) {
                    ex.printStackTrace();
                }
            }
        };
        thread.start();

        JFrameOperator frameOperator = new JFrameOperator();
        PaukerFrame paukerFrame = (PaukerFrame) frameOperator.getSource();

        Thread.sleep(500);

        Lesson currentLesson = paukerFrame.getCurrentLesson();
        assertEquals(1, currentLesson.getNumberOfCards());
    }
}
