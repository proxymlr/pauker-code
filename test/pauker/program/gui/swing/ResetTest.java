/*
 * ResetTest.java
 *
 * Created on 27. November 2007, 20:28
 *
 */
package pauker.program.gui.swing;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JCheckBoxOperator;
import org.netbeans.jemmy.operators.JComboBoxOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JTextAreaOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import pauker.program.Lesson;
import screenshots.Screenshots;

/**
 * a test that checks that resetting cards works like expected
 * @author Ronny.Standtke@gmx.net
 */
public class ResetTest extends TestCase {

    private static final Logger LOGGER =
            Logger.getLogger(ResetTest.class.getName());

    /**
     * runs the test
     * @throws java.lang.Exception
     */
    public void testReset() throws Exception {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        ResourceBundle strings = ResourceBundle.getBundle("pauker/Strings");

        // start pauker
        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication();
        JFrameOperator frameOperator = new JFrameOperator();
        final PaukerFrame paukerFrame = (PaukerFrame) frameOperator.getSource();
        final JButtonOperator addCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("addCardButton"));
        JButtonOperator learnNewCardsButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("learnNewCardsButton"));
        JButtonOperator nextNewCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("nextNewCardButton"));
        JButtonOperator repeatUSTMButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatUSTMButton"));
        JButtonOperator showMeButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("showMeButton"));
        JButtonOperator repeatingYesButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatingYesButton"));
        JButtonOperator repeatSTMButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatSTMButton"));
        JButtonOperator transitionOKButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("transitionOKButton"));


        // insert a card
        addCardButtonOperator.pushNoBlock();
        JDialogOperator newDialogOperator = new JDialogOperator();
        JTextAreaOperator newFrontSideTextAreaOperator = new JTextAreaOperator(
                newDialogOperator, new NameComponentChooser("frontSideTextArea"));
        JTextAreaOperator reverseSideTextAreaOperator = new JTextAreaOperator(
                newDialogOperator, new NameComponentChooser("reverseSideTextArea"));
        JComboBoxOperator repeatingMethodComboBoxOperator = new JComboBoxOperator(
                newDialogOperator, new NameComponentChooser("repeatingMethodComboBox"));
        JCheckBoxOperator keepOpenCheckBoxOperator = new JCheckBoxOperator(
                newDialogOperator, new NameComponentChooser("keepOpenCheckBox"));
        JButtonOperator okButtonOperator = new JButtonOperator(
                newDialogOperator, new NameComponentChooser("okButton"));
        newFrontSideTextAreaOperator.setText("1");
        reverseSideTextAreaOperator.setText("2");
        repeatingMethodComboBoxOperator.setSelectedIndex(1);
        keepOpenCheckBoxOperator.setSelected(false);
        Tools.doClick(okButtonOperator);

        // "learn" the one and only card
        learnNewCardsButtonOperator.waitComponentEnabled();
        learnNewCardsButtonOperator.doClick();
        nextNewCardButtonOperator.waitComponentShowing(true);
        nextNewCardButtonOperator.waitComponentEnabled();
        nextNewCardButtonOperator.doClick();
        repeatUSTMButtonOperator.waitComponentShowing(true);
        repeatUSTMButtonOperator.doClick();
        showMeButtonOperator.waitComponentShowing(true);
        showMeButtonOperator.doClick();
        repeatingYesButtonOperator.waitComponentShowing(true);
        repeatingYesButtonOperator.doClick();
        repeatSTMButtonOperator.waitComponentShowing(true);
        repeatSTMButtonOperator.doClick();
        transitionOKButtonOperator.waitComponentShowing(true);
        transitionOKButtonOperator.doClick();
        showMeButtonOperator.waitComponentShowing(true);
        showMeButtonOperator.doClick();
        repeatingYesButtonOperator.waitComponentShowing(true);
        repeatingYesButtonOperator.doClick();

        // try resetting the lesson
        // (because pushing menu items does not work on Mac OS X we use
        // reflection instead of jemmy here)
        final Method resetMethod = PaukerFrame.class.getDeclaredMethod(
                "resetMenuItemActionPerformed", ActionEvent.class);
        resetMethod.setAccessible(true);
        EventQueue.invokeLater(new Runnable() {

            @Override
            public void run() {
                try {
                    resetMethod.invoke(paukerFrame, (ActionEvent) null);
                } catch (IllegalAccessException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                } catch (IllegalArgumentException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                } catch (InvocationTargetException ex) {
                    LOGGER.log(Level.SEVERE, null, ex);
                }
            }
        });
        JDialogOperator dialogOperator = new JDialogOperator();
        JButtonOperator resetButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("Reset_Lesson"));
        Tools.doClick(resetButtonOperator);

        // check that all cards are in the unlearned batch
        Thread.sleep(500);
        Lesson lesson = paukerFrame.getCurrentLesson();
        assertEquals(0, lesson.getNumberOfLongTermBatches());
    }
}
