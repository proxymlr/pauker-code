/*
 * CancelLearningTest.java
 *
 * Created on 12.10.2008, 13:22:37
 *
 */
package pauker.program.gui.swing;

import java.util.ResourceBundle;
import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JCheckBoxOperator;
import org.netbeans.jemmy.operators.JComboBoxOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JLabelOperator;
import org.netbeans.jemmy.operators.JListOperator;
import org.netbeans.jemmy.operators.JTextAreaOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import pauker.program.Lesson;
import pauker.program.LongTermBatch;
import screenshots.Screenshots;

/**
 * Test against bug#2858292:
 * When editing cards while repeating, the lesson gets messed up
 * @author Ronny Standtke <Ronny.Standtke@gmx.net>
 */
public class EditWhileRepeatingTest extends TestCase {

    /**
     * runs the test
     * @throws Exception if an exception occurs
     */
    public void testEditWhileRepeating() throws Exception {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        ResourceBundle strings = ResourceBundle.getBundle("pauker/Strings");

        // start pauker
        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication();
        JFrameOperator frameOperator = new JFrameOperator();
        PaukerFrame paukerFrame = (PaukerFrame) frameOperator.getSource();
        JButtonOperator addCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("addCardButton"));
        JButtonOperator learnNewCardsButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("learnNewCardsButton"));

        // add a cards
        addCardButtonOperator.pushNoBlock();
        JDialogOperator newDialogOperator = new JDialogOperator();
        JTextAreaOperator newFrontSideTextAreaOperator = new JTextAreaOperator(
                newDialogOperator, new NameComponentChooser("frontSideTextArea"));
        JTextAreaOperator reverseSideTextAreaOperator = new JTextAreaOperator(
                newDialogOperator, new NameComponentChooser("reverseSideTextArea"));
        JComboBoxOperator repeatingMethodComboBoxOperator = new JComboBoxOperator(
                newDialogOperator, new NameComponentChooser("repeatingMethodComboBox"));
        JCheckBoxOperator keepOpenCheckBoxOperator = new JCheckBoxOperator(
                newDialogOperator, new NameComponentChooser("keepOpenCheckBox"));
        JButtonOperator okButtonOperator = new JButtonOperator(
                newDialogOperator, new NameComponentChooser("okButton"));
        newFrontSideTextAreaOperator.setText("1");
        reverseSideTextAreaOperator.setText("1");
        repeatingMethodComboBoxOperator.setSelectedIndex(1);
        keepOpenCheckBoxOperator.setSelected(false);
        Tools.doClick(okButtonOperator);

        // instant repeat the card
        JLabelOperator summaryLabelOperator = new JLabelOperator(
                frameOperator, new NameComponentChooser("summaryLabel"));
        JListOperator batchListOperator = new JListOperator(
                frameOperator, new NameComponentChooser("batchList"));
        JButtonOperator instantRepeatButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("instantRepeatButton"));
        JButtonOperator repeatCardsButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatCardsButton"));
        JButtonOperator showMeButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("showMeButton"));
        Tools.clickComponentOperator(summaryLabelOperator);
        batchListOperator.waitComponentShowing(true);
        assertTrue(batchListOperator.getModel().getSize() > 0);
        batchListOperator.setSelectedIndex(0);
        instantRepeatButtonOperator.waitComponentEnabled();
        instantRepeatButtonOperator.pushNoBlock();
        JDialogOperator dialogOperator = new JDialogOperator();
        JButtonOperator confirmButtonOperator = new JButtonOperator(
                dialogOperator, strings.getString("Repeat_Cards_Instantly"));
        Tools.doClick(confirmButtonOperator);
        repeatCardsButtonOperator.waitComponentEnabled();
        Lesson lesson = paukerFrame.getCurrentLesson();
        assertEquals(1, lesson.getNumberOfLongTermBatches());
        assertTrue(lesson.getCards().get(0).isLearned());
        assertFalse(learnNewCardsButtonOperator.isEnabled());
        assertTrue("loading the summary failed!",
                batchListOperator.getModel().getSize() > 0);
        Tools.doClick(repeatCardsButtonOperator);
        showMeButtonOperator.waitComponentShowing(true);
        showMeButtonOperator.waitComponentEnabled();
        showMeButtonOperator.waitHasFocus();

        // edit the card
        JButtonOperator editWhileRepeatingButtonOperator =
                new JButtonOperator(frameOperator,
                new NameComponentChooser("editWhileRepeatingButton"));
        editWhileRepeatingButtonOperator.pushNoBlock();
        JDialogOperator editDialogOperator =
                new JDialogOperator(strings.getString("Edit_Card"));
        JTextAreaOperator editFrontSideTextAreaOperator =
                new JTextAreaOperator(editDialogOperator,
                new NameComponentChooser("frontSideTextArea"));
        JButtonOperator editOKButtonOperator = new JButtonOperator(
                editDialogOperator, new NameComponentChooser("editOKButton"));
        editFrontSideTextAreaOperator.setText("2");
        Tools.doClick(editOKButtonOperator);

        // continue repeating
        showMeButtonOperator.doClick();
        JButtonOperator repeatingYesButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("repeatingYesButton"));
        repeatingYesButtonOperator.waitComponentShowing(true);
        repeatingYesButtonOperator.waitComponentEnabled();
        repeatingYesButtonOperator.waitHasFocus();
        repeatingYesButtonOperator.doClick();

        lesson = paukerFrame.getCurrentLesson();
        LongTermBatch longTermBatch0 = lesson.getLongTermBatch(0);
        assertEquals(0, longTermBatch0.getNumberOfExpiredCards());
        assertEquals(0, longTermBatch0.getLearnedCards().size());
        assertEquals(1, lesson.getLongTermBatch(1).getNumberOfCards());
    }
}
