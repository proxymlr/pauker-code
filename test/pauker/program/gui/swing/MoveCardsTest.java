/*
 * MoveCardsTest.java
 *
 * Created on 28.02.2009, 17:49:37
 *
 */

package pauker.program.gui.swing;

import junit.framework.TestCase;
import org.netbeans.jemmy.ClassReference;
import org.netbeans.jemmy.operators.JButtonOperator;
import org.netbeans.jemmy.operators.JCheckBoxOperator;
import org.netbeans.jemmy.operators.JDialogOperator;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JLabelOperator;
import org.netbeans.jemmy.operators.JListOperator;
import org.netbeans.jemmy.operators.JTextAreaOperator;
import org.netbeans.jemmy.util.NameComponentChooser;
import pauker.program.Card;
import screenshots.Screenshots;

/**
 * A test that checks that moving cards works as expected
 * @author Ronny Standtke <Ronny.Standtke@gmx.net>
 */
public class MoveCardsTest extends TestCase {

    /**
     * runs the test
     * @throws Exception if an exception occurs
     */
    public void testCancelling() throws Exception {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        // start pauker
        ClassReference classReference = new ClassReference(
                "pauker.program.gui.swing.PaukerFrame");
        classReference.startApplication();
        JFrameOperator frameOperator = new JFrameOperator();
        JLabelOperator summaryLabelOperator = new JLabelOperator(
                frameOperator, new NameComponentChooser("summaryLabel"));
        JListOperator batchListOperator = new JListOperator(
                frameOperator, new NameComponentChooser("batchList"));
        JButtonOperator addCardButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("addCardButton"));
        JButtonOperator topButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("topButton"));
        JButtonOperator bottomButtonOperator = new JButtonOperator(
                frameOperator, new NameComponentChooser("bottomButton"));

        // add some cards
        addCardButtonOperator.pushNoBlock();
        JDialogOperator newDialogOperator = new JDialogOperator();
        JTextAreaOperator newFrontSideTextAreaOperator = new JTextAreaOperator(
                newDialogOperator, new NameComponentChooser("frontSideTextArea"));
        JTextAreaOperator reverseSideTextAreaOperator = new JTextAreaOperator(
                newDialogOperator, new NameComponentChooser("reverseSideTextArea"));
        JCheckBoxOperator keepOpenCheckBoxOperator = new JCheckBoxOperator(
                newDialogOperator, new NameComponentChooser("keepOpenCheckBox"));
        JButtonOperator okButtonOperator = new JButtonOperator(
                newDialogOperator, new NameComponentChooser("okButton"));
        JButtonOperator cancelButtonOperator = new JButtonOperator(
                newDialogOperator, new NameComponentChooser("cancelButton"));
        keepOpenCheckBoxOperator.setSelected(true);
        for (int i = 0; i < 5; i++) {
            String text = String.valueOf(i);
            newFrontSideTextAreaOperator.setText(text);
            reverseSideTextAreaOperator.setText(text);
            Tools.doClick(okButtonOperator);
        }
        Tools.doClick(cancelButtonOperator);

        // load the summary
        Thread.sleep(1000);
        Tools.clickComponentOperator(summaryLabelOperator);
        batchListOperator.waitComponentShowing(true);

        // test button (de)activation
        batchListOperator.setSelectedIndex(-1);
        assertFalse(topButtonOperator.isEnabled());
        assertFalse(bottomButtonOperator.isEnabled());
        batchListOperator.setSelectedIndex(0);
        assertFalse(topButtonOperator.isEnabled());
        assertTrue(bottomButtonOperator.isEnabled());
        batchListOperator.setSelectedIndex(1);
        assertTrue(topButtonOperator.isEnabled());
        assertTrue(bottomButtonOperator.isEnabled());
        batchListOperator.setSelectedIndex(4);
        assertTrue(topButtonOperator.isEnabled());
        assertFalse(bottomButtonOperator.isEnabled());

        // test card moving
        batchListOperator.setSelectedIndex(2);
        Tools.doClick(topButtonOperator);
        Card card = (Card) batchListOperator.getModel().getElementAt(0);
        assertEquals("2", card.getFrontSide().getText());

        Tools.doClick(bottomButtonOperator);
        card = (Card) batchListOperator.getModel().getElementAt(0);
        assertEquals("0", card.getFrontSide().getText());
    }
}
