/*
 * PaukerTest.java
 * JUnit based test
 *
 * Created on 13. April 2006, 16:13
 */

package pauker.program;

import junit.framework.TestCase;
import screenshots.Screenshots;

/**
 *
 * @author Ronny.Standtke@gmx.net
 */
public class PaukerTest extends TestCase {

    /**
     * Test of lessonToCSV method, of class pauker.program.Pauker.
     */
    public void testLessonToCSV() {

        assertFalse("Only screenshots are generated!",
                Screenshots.UPDATE_SCREENSHOTS);

        String lineSeparator = System.getProperty("line.separator");

        // simple test
        Lesson lesson = new Lesson();
        addCard(lesson, "1", "2");
        String expResult = "1,2";
        String result = Pauker.lessonToCSV(lesson);
        assertEquals(expResult, result);

        // test newline insertion
        lesson = new Lesson();
        addCard(lesson, "1", "2");
        addCard(lesson, "3", "4");
        expResult =
                "1,2" + lineSeparator +
                "3,4";
        result = Pauker.lessonToCSV(lesson);
        assertEquals(expResult, result);

        // test all four escape situations
        lesson = new Lesson();
        addCard(lesson, "kom,ma", " leer zeichen ");
        addCard(lesson, "hoch\"komma", "Zeilen\numbruch");
        expResult =
                "\"kom,ma\",\" leer zeichen \"" + lineSeparator +
                "\"hoch\"\"komma\",\"Zeilen\numbruch\"";
        result = Pauker.lessonToCSV(lesson);
        assertEquals(expResult, result);

        // test lesson with unlearned and long term batches
        lesson = new Lesson();
        LongTermBatch longTermBatch = lesson.addLongTermBatch();
        addCard(lesson, "1", "1");
        CardSide frontSide = new CardSide("2");
        CardSide reverseSide = new CardSide("2");
        Card card = new Card(frontSide, reverseSide);
        longTermBatch.addCard(card);
        expResult =
                "1,1" + lineSeparator +
                "2,2";
        result = Pauker.lessonToCSV(lesson);
        assertEquals("export to CSV with several batches failed\n" +
                "expected result:\n" + expResult +
                "\nresult: " + result, expResult, result);

        // test lesson with only long term batches
        lesson = new Lesson();
        longTermBatch = lesson.addLongTermBatch();
        frontSide = new CardSide("1");
        reverseSide = new CardSide("1");
        card = new Card(frontSide, reverseSide);
        longTermBatch.addCard(card);
        expResult = "1,1";
        result = Pauker.lessonToCSV(lesson);
        assertEquals("export to CSV with only long term batches failed\n" +
                "expected result:\n" + expResult +
                "\nresult: " + result, expResult, result);
    }

    private void addCard(Lesson lesson, String frontText, String reverseText) {
        CardSide frontSide = new CardSide(frontText);
        CardSide reverseSide = new CardSide(reverseText);
        Card card = new Card(frontSide, reverseSide);
        lesson.addCard(card);
    }
}
